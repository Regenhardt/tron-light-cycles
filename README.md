# Light Cycles
Game using view-library used in BAI5-VSP lab.

## Prerequisites
- Java 19
- Maven

## Build and Run

```bash
# build executable jar
mvn clean package

# run java application
java -jar target/tron-0.2.jar
# alternatively you can use the tron script
./tron
```

# Dokumentation

Das Projekt wird aufbauend auf arc42 dokumentiert. 

[Game](arc42/tron/AppDoc.md)  
[Middleware](arc42/middleware/LibDoc.md)
