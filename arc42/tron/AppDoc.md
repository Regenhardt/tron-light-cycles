VSP WS22 – Beta 1 – Nina Godenrath, Jessica Moll, Marlon Regenhardt – 8. Januar 2023

Das aktuelle Dokument ist auch unter folgendem Link zu finden: [https://gitlab.com/Regenhardt/tron-light-cycles/-/blob/master/arc42/tron/AppDoc.md](https://gitlab.com/Regenhardt/tron-light-cycles/-/blob/master/arc42/tron/AppDoc.md)

# Einführung und Ziele

Tron ist ein Arcade-Spiel für mehrere Spieler. Jeder Spieler steuert ein Fahrzeug, 
dass eine Spur hinter sich zieht. Ein Spieler hat das Spiel verloren, 
sobald er mit einem Hindernis kollidiert. 
Dies kann entweder eine Spur, ein anderer Spieler oder die Spielfeldgrenze sein.

Ziel ist es Tron letztlich als verteiltes System zu implementieren, um dadurch im Rahmen der Veranstaltung *Verteilte Systeme* Erfahrung und Wissen durch die praktische Anwendung zu erlangen.

## Aufgabenstellung 

Es gibt eine Konfigurationsdatei, die beim Starten des Spiels geladen wird. Diese Einstellungen sind nur bei einem Neustart des Spiels änderbar. 

### Use Cases

![Use Case Diagramm](images/use_case_diagramm.drawio.png)

### UC-1
- **Titel:** Spiel starten
- **Akteur:** Spieler*in
- **Ziel:** Ein neues Spiel starten
- **Auslöser:** Spieler*in hat auf den Start-Button geklickt
- **Vorbedingungen:**
    1. Der Startbildschirm wird angezeigt
- **Nachbedingungen:**
    1. Das Spiel läuft mit der angegebenen Anzahl an Spielenden
    2. Fahrzeuge bewegen sich in einem konfigurierten Takt vorwärts
- **Erfolgsszenario:**
    1. Das System zeigt den Startbildschirm mit vorausgefülltem Feld für die Anzahl der Spielenden und einen Start-Button an 
    2. Spieler*in klickt auf den Start-Button
    3. Es wird ein Wartescreen angezeigt
    4. Es wird ein Countdown von 3sek angezeigt und darunter die relevanten festgelegten Farben inklusive Steuerung für die Spielenden

### UC-1.1
- **Titel:** Anzahl Spielende eingeben
- **Akteur:** Spieler*in
- **Ziel:** Die Anzahl der Spielenden für das Spiel einstellen
- **Auslöser:**
    1. Spieler*in drückt auf + oder - neben dem Feld für die Anzahl der Spielenden
- **Vorbedingungen:**
    1. Der Startbildschirm wird angezeigt sein
- **Nachbedingungen:**
    1. Die angezeigte Zahl für die Anzahl der Spielenden wird um 1 erhöht oder verringert
- **Erfolgsszenario:**
    1. Spieler*in klickt auf einen der Buttons (+, -) neben der Anzeige für die Anzahl der Spielenden
    2. Die Anzeige ändert sich dementsprechend
- **Erweiterungsfall:**
    1. a. Die Anzahl der Spielenden würde mit der entsprechenden Operation ein Limit über-/unterschreiten
       1. Der auslösende Button wird deaktiviert

### UC-2
- **Titel:** Fahrzeug steuern
- **Akteur:** Spieler*in
- **Ziel:** Das Fahrzeug steuern, um Kollisionen zu vermeiden.
- **Auslöser:**
    1. Der/die Spieler*in drückt eine Taste zur Richtungsänderung.
- **Vorbedingungen:**
    1. Das Spiel ist gestartet und läuft.
- **Nachbedingungen:**
    1. Die Fahrtrichtung des Fahrzeugs ist die durch die Taste abgebildete Richtung.
- **Erfolgsszenario:**
    1. Der/die Spieler*in drückt eine Taste, die eine Richtung abbildet.
    2. Die Ausrichtung des Fahrzeugs ändert sich entsprechend.
    3. Beim nächsten Takt bewegt sich das Fahrzeug in die neue Richtung.
- **Fehlerfälle:**
    1. Der/die Spieler*in drückt eine Taste, die keine Richtung abbildet.
      1.a Die Fahrtrichtung des Fahrzeugs ändert sich nicht.
    
### UC-3
- **Titel:** Spiel gewinnen
- **Akteur:** Spieler*in
- **Ziel:** Der Allerbeste sein, wie keiner vor ihm war.
- **Auslöser:**
    1. Alle anderen Spieler*innen haben verloren.
- **Vorbedingungen:**
    1. Das Spiel läuft.
- **Nachbedingungen:**
    1. Das Spiel ist beendet.
- **Erfolgsszenario:**
    1. Alle anderen Fahrzeuge sind vom Spielfeld entfernt worden.
    2. Das System merkt sich, dass diese\*r Spieler*in gewonnen hat.
    2. Das Spiel wird beendet.

### UC-4
- **Titel:** Spiel verlieren
- **Akteur:** Spieler*in
- **Ziel:** Trotzdem Spaß haben.
- **Auslöser:**
    1. Das eigene Fahrzeug ist gegen die Spielfeldbegrenzung, ein anderes Fahrzeug oder die Spur eines anderen Fahrzeugs gefahren.
- **Vorbedingungen:**
    1. Das Spiel läuft.
- **Nachbedingungen:**
    1. Das Fahrzeug des/der Spieler*in ist samt Spur vom Spielfeld entfernt.
- **Erfolgsszenario:**
    1. Der/die Spieler*in verliert das Spiel.
    2. Das System merkt sich, dass diese\*r Spieler*in verloren hat.
    3. Für die übrigen Spieler läuft das Spiel weiter.
    3.a Die übrigen noch im Spiel befindlichen Spieler*innen verlieren im selben Takt.
       3.a.1 Das System beendet das Spiel.

### UC-5
- **Titel:** Ergebnis ansehen
- **Akteur:** Spieler*in
- **Ziel:** Sehen, welche Spieler jeweils gewonnen oder verloren haben.
- **Auslöser:**
    1. Das Spiel ist beendet.
- **Vorbedingungen:**
    1. Das Spiel ist beendet.
- **Nachbedingungen:**
    1. Ein Neustart ist möglich.
- **Erfolgsszenario:**
    1. Das Spiel ist beendet.
    2. Das System zeigt eine Ansicht, die die Spieler und ihren jeweiligen Spielausgang auflistet.

Konfigurierbare Einstellungen:
- Spieleranzahl
- Maximale Spieleranzahl
- Spielfeldgröße, dabei können Breite und Höhe unabhängig sein
- Rastergröße
- Wartezeit
- Steuerungstasten für die Spieler
- Geschwindigkeit Felder/sek (wobei 100% nicht spielbar sein sollte)

### Storyboard

| | |
|-|-|
|![Screen 1](images/storyboard_1.png)| 1️⃣ <ul><li>Feld mit Default vorausgefüllt (parametrierbar)</li> <li>Mögliche Spieleranzahl 2-6</li> </ul>|
|![Screen 2](images/storyboard_2.png)| 2️⃣ <ul><li>Wartescreen vor dem Starten des Spiels</li> <li>Maximale Wartezeit (parametrierbar)</li> <li>Auch mit weniger Spieler möglich das Spiel zu starten, falls Spieler ausfallen</li> <li>Abbruch, wenn alleine ➡️ 1️⃣ </li></ul> |
|![Screen 3](images/storyboard_3.png)| 3️⃣ <ul><li>Countdown vor Starten des Spiels als Overlay (3,2,1)</li> <li>Spielfeld als sichtbares Raster</li> <li>Faire Startposition</li> <li>alle Spielenden ziehen eine Wand als Schatten hinter ihrem Fahrzeug</li> <li>Verlieren durch Kollision mit Wand / Schatten (auch eigener)</li> <li>Gewonnen hat, wer als letztes übrig bliebt</li> <li>Zusammenstoß zweier Fahrzeuge lässt beide Spielenden verlieren. Sonderfall: sind diese die letzten verbliebenen ist das Spiel unentscheiden</li> <li>nach Ausscheiden verschwindet der Schatten des eigenen Fahrzeugs vom Spielfeld</li> <li>geeignete Darstellung, welches Fahrzeug das eigene ist</li> </ul> Steuerung: <ul><li>Konfigurierbare Tastenkombinationen (parametrisierbar)</li> <li>automatische Fortbewegung (wie in Snake)</li> <li>Es kann nach rechts oder links im 90 Grad Winkel gesteuert werden</li> </ul> ||
|![Screen 4](images/storyboard_4.png)| 4️⃣ <ul><li>Wird nach dem Spiel angezeigt</li> <li>Geeignet darstellen wer wer ist</li> <li>Durch einen Button kommt man wieder zum Startbildschirm ➡️ 1️⃣</li> </ul>|

## User Stories

### TRON-1.1
- **Titel:** Spiel konfigurieren
- **Beschreibung:** Als Spieler*in kann ich ein Spiel konfigurieren
- **Akzeptanzkriterien:**
    1. Man kann die Spieleranzahl einstellen

### TRON-1.2
- **Titel:** Spiel starten
- **Beschreibung:** Als Spieler*in kann ich ein Spiel starten
- **Akzeptanzkriterien**
    1. Das Spiel startet mit der gegebenen Anzahl von Fahrzeugen
    2. Zwischen dem Starten und dem eigentlichen Spiel gibt es einen Ladebildschirm

### TRON-1.3
- **Titel:** Farbe und Tastenbelegung
- **Beschreibung:** Als Spieler*in kann ich sehen, welche Tastenbelegung meine Steuerung und welche Farbe mein Fahrzeug hat
- **Akzeptanzkriterien:**
    1. Die Information wird für alle im Startscreen angezeigt

### TRON-2.1
- **Titel:** Countdown
- **Beschreibung:** Als Spieler*in wird mir (bzw. allen Spielenden) ein Countdown von 3 Sekunden als Overlay angezeigt, bevor das Spiel beginnt
- **Akzeptanzkriterien:**
    1. Ist das Spiel bereit zum Starten, wird vor dem Start ein Countdown von 3 Sekunden angezeigt
    1. Während des Countdown sieht man das Spielfeld mit den Fahrzeugen an deren Startpositionen
    1. Nach dem Countdown verschwindet das Overlay und das Spiel beginnt 
    
### TRON-2.2
- **Titel:** Fahrzeug steuern 
- **Beschreibung:** Als Spieler*in kann ich mein Fahrzeug steuern
- **Akzeptanzkriterien:**
    1. Man kann bei seinem fahrenden Fahrzeug die Richtung (absolut) wählen, in die es dann fährt

### TRON-2.3
- **Titel:** Gewinnbedingung
- **Beschreibung:** Als letzte Person mit intaktem Fahrzeug habe ich das Spiel gewonnen
- **Akzeptanzkriterien:**
    1. Ist nur noch ein Fahrzeug auf dem Spielfeld, ist dies der/die Sieger*in des Spiels
    1. Ist kein Fahrzeug mehr auf dem Spielfeld, endet das Spiel in einem Unentschieden zwischen den beiden zuletzt ausgeschiedenen Fahrzeugen
    1. Berührt ein Fahrzeug eine Leuchtspur oder den Bildschirmrand, scheidet es aus dem Spiel aus und seine Leuchtspur verschwindet vom Spielfeld

### TRON-3
- **Titel:** Ergebnis
- **Beschreibung:** Als Spieler*in kann ich nach Spielende das Ergebnis sehen
- **Akzeptanzkriterien:**
    1. Nach Spielende wird eine Übersicht über das Ergebnis angezeigt

### TRON-4
- **Titel:** Rückkehr zum Startbildschirm
- **Beschreibung:** Als Spieler*in kann ich vom Ergebnisbildschirm zum Startbildschirm wechseln
- **Akzeptanzkriterien:**
    1. Durch einen Buttonklick, kommt man wieder zum Startbildschirm

## Qualitätsziele

| Qualitätsziel                      | Motivation und Erläuterung
|------------------------------------|---------------------------------------------------------------------|
|Ein Spiel am Stück spielbar (Zuverlässigkeit)|Es reicht aus, ist aber auch Mindestanforderung, dass ein Spiel ohne Abstürze am Stück durchspielbar ist.|
|Möglichkeit, dass mindestens 2 Applikationen miteinander spielen können (Kompatibilität)|Es soll die Möglichkeit geschaffen werden, von verschiedenen Geräten aus miteinander spielen zu können.|
|Läuft immer gleich schnell (keine Jitter-Abhängigkeit) (Effizienz) |Auch bei Spielen mit großem Spielfeld darf das Spiel bei keinem Teilnehmer 'laggen', da es für die Fairness wichtig ist, dass die Spieler keinen Nachteil durch Verzögerungen haben. Es darf keine Verzögerung von mehr als 200ms zwischen einer Aktion des Spielers und der Reaktion des Spiels geben.|
|Stabilität bei Absturz anderer Teilnehmer (Zuverlässigkeit)|Auch wenn ein Spieler die Verbindung verliert, sollen die anderen weiterspielen können. Der betroffene Spieler wird dann als 'ausgeschieden' behandelt (d.h. sein Fahrzeug und Fahrspur verschwinden).|


## Stakeholder

| Rolle                      | Kontakt                          | Erwartungshaltung                                                                                                                |
|----------------------------|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| Dozent                     | Martin Becke                     | Ordentliche Doku, Planung, Implementierung                                                                                       |
| Entwickler                 | Nina, Jessica & Marlon              | Lernen der Planung & Implementierung verteilter Systeme                                                                          |
| Spieler                    | Entwickler + ggf. andere         | Lauffähiges Tron-Spiel                                                                                                           |
| Entwickler anderer Clients | Alle im MS Team "VSP WS2022 Tron" | Gemeinsames Protokoll zur Kommunikation, ggf. Absprache bzgl. Architektur, damit die Clients in gemeinsamer Lobby spielen können |

# Randbedingungen

**Technische Randbedingungen**

|Randbedingung|Erläuterung|
|-------------|-----------|
|Implementierung in Java|Wir implementieren die Anwendung in Java 19.|
|Verwendung der view-library für die GUI|Für die GUI verwenden wir das JavaFX-basierte GUI-Grundgerüst, das in den Projekten view-library und view-library-example für das Praktikum bereitgestellt wurde.|  
| Extra Entrypoint | Da die genutzte view-library javafx in der jar mitliefert, benötigen wir eine eigene Entrypoint-Klasse, statt die Application direkt zu starten. |
|Maven als Build-Tool|Als Build-Tool verwenden wir Maven.|
|Protokoll für Netzwerk-Mehrspieler|Die verwendete Middleware unterstützt als Funktions-ID nur ein einzelnes Byte und als Payload nur primitive Datentypen und Strings, daher verwenden wir ein Protokoll, das diese Einschränkungen berücksichtigt.|
|Java kann keine Generics|Eine Klasse mit selbem Namen aber unterschiedlichen generischen Parametern wird von Java als diesselbe Klasse interpretiert. Deswegen mussten wir unsere drei Action Klassen durchnummerieren.|

**Organisatorische Randbedingungen**

|Randbedingung|Erläuterung|
|-------------|-----------|
|Vorgegebener Zeitplan im Rahmen des Praktikums|Es gibt 3 Milestones, die jeweils zum Ende der KW 42, 45 und 48 erreicht werden sollen.|
|Entwicklungswerkzeuge|Für die Erstellung von Grafiken und Diagrammen verwenden wir draw.io und PlantUML. |
|Versionsverwaltung über GitLab|Alle Unterlagen und der Code zu diesem Projekt werden stets aktuell gehalten und unter https://git.haw-hamburg.de/gruppe-1-4ever/verteilte-systeme/wi-se-22-23-vs-2543198-2579777-2582094 abgelegt.|
|Versionsverwaltung nach Cyberangriff auf IT-Infrastruktur der HAW|Da nach dem Angriff kein Zugriff mehr auf die GitLab-Instanz der HAW möglich ist, wurde das Projekt auf gitlab.com (https://gitlab.com/Regenhardt/tron-light-cycles) übertragen.|


**Konventionen**

|Konvention|Erläuterung|
|----------|-----------|
|Architekturdokumentation|Terminologie und Gliederung nach dem deutschen arc42-Template.|
|Sprache|Wir verwenden englische Begriffe im Code (Klassen-, Methoden-, Variablennamen etc.) und deutsche Sprache in den Kommentaren, der GUI und allen anderen Dokumenten.|

# Kontextabgrenzung

Es kann zwischen fachlichem und technischem Kontext unterschieden werden. Im Fall von unserem Tron-Spiel gibt es nicht allzu viel abzugrenzen, da es sich um eine sogenannte Standalone-Applikation handelt, die unabhängig von anderen Systemen funktioniert.

## Fachlicher Kontext

An einem Spiel können 2 bis 6 Spielende teilnehmen. 

![Fachlicher Kontext von Tron als Standalone-Applikation](images/tron_fachlicher_kontext.png)


## Technischer Kontext

Der technische Kontext wird hier nur mit der Applikation als Teil eines verteilten Tron-Systems dargestellt, da es beim lokalen Spiel-Modus keinen hier relevanten technischen Kontext nach außen hin gibt, der nicht auch im folgenden Diagramm ersichtlich wäre.

![Technischer Kontext von Tron als Standalone-Applikation](images/technischer_kontext.drawio.png)

# Lösungsstrategie

Die folgende Tabelle stellt die Qualitätsziele des Tron-Spiels passenden Lösungsstrategien gegenüber.

|Qualitätsziel|Lösungsstrategie|
|-------------|-----------------|
|Ein Spiel am Stück spielbar (Zuverlässigkeit)|Überprüfung aller Parameter zur Konfiguration, Abfangen und angemessener Umgang mit Fehlern.|
|Läuft immer gleich schnell (keine Jitter-Abhängigkeit) (Effizienz)|Die Aktualisierungsgeschwindigkeit (Bewegungen pro Zeiteinheit) ist durchgehend für alle Spieler konstant und Steuerbewegungen werden ebenfalls nur in diesem Takt verarbeitet.|
|Stabilität bei Absturz anderer Teilnehmer (Zuverlässigkeit)|Wenn ein Spieler ausfällt, wird sein Avatar samt Spur aus dem Spiel entfernt, sodass keine Vorgänge blockiert werden können.|

Die Anwendung ist nach dem MVC-Pattern aufgebaut (näheres unter Querschnittliche Konzepte). 
Wie wir dies konkret realisieren, steht im folgenden Abschnitt.

In die folgenden Funktionsliste, sind Methoden, die keiner weiteren Erklärung bedürfen, wie getter und setter, nicht mit aufgenommen. Außerdem wird in der Fehlersemantik nicht auf unerwartete Eingaben eingegangen, da diese einfach ignoriert werden und maximal für die Nachvollziehbarkeit geloggt werden, aber keinen Einfluss auf den Ablauf haben. 
Die Methoden für das Senden von Events sind in den entsprechendne Ablaufsemantiken erwähnt und in einer anschließenden eigenen Tabelle aufgeführt. 

`TronGame` ist der Einstiegspunkt für das Spiel. Hier werden die drei Hauptkomponenten initialisiert und zusammengesteckt. Außerdem werden benötigte config-Datei in Properties gewandelt und den Komponenten mitgegeben. 

Der `TronViewWrapper` ist dafür zuständig auf die Events aus dem `ITronModel` zu reagieren und der `TronController` ist dafür zuständig die Eingaben aus dem `ITronViewWrapper` zu verarbeiten und an das `ITronModel` weiterzugeben.

| Use Case | Objekt | Methode | Ablaufsemantik | Fehlersemantik | Vorbedingung | Nachbedingung
|----------|--------|---------|----------------|----------------|--------------|---------------|
|0.1|TronController|`new TronController(properties: Properties, TronViewWrapper, ITronModel)`|Erstellt einen Controller, der einen TronViewWrapper und ein TronModel hält|-|TronViewWrapper und TronModel sind initialisiert|Controller ist initialisiert|
|0.2|ITronModel|`static buildModel(rows: int, columns: int, tickrate: double, remote: bool): ITronModel`|Erstellt ein Model mit den gegebenen Zeilen, Spalten und Tickrate. Mit `remote=true` wird statt des vollen Models ein ApplicationStub namens `RemoteModel` erstellt|Anzahl an rows/columns ist kleiner als 10|-|Model ist erzeugt und im StartState|
|1.1+5.1|StartState|`new StartState(tronModel: TronModel)`|Erstellt einen StartState|-|-|Model ist im StartState|
|1.2+5.2|StartState|`activate()`|ruft `resetGame()`auf dem Model auf|-|-|-|
|1.3+5.3|TronModel|`resetGame()`|Setzt das Model in den Initialzustand zurück, ruft `resetPlayers()` auf|-|-||
|1.4+5.4|TronModel|`resetPlayers()`|Leert die LightCycle-Liste und setzt den PlayerCount auf 0|-|-|LightCycle-Liste ist leer und PlayerCount ist 0|
|0.3|TronViewWrapper|`new TronViewWrapper(configFile: String, ITronModel)`|Erstellt eine View, registriert Overlays als GUI und verknüpft Events vom ITronModel|ConfigFile existiert nicht|Model ist initialisiert und im StartState|View ist initialisiert|
|1.5|StartScreen|`setOnStartClicked(Action<Integer>)`|Registriert den Eventhandler|EventHandler ist null|Button ist initialisiert|EventHandler ist registriert|
|1.6|StartScreen|`onStartButtonClicked()`|Ruft den OnStartClicked EventHandler mit dem PlayerCount aus dem Inputfeld auf|-|-|EventHandler wurde aufgerufen|
|0.5|Keymap| `new Keymap(props: Properties)`|Erstellt eine Keymap, die die Steuerung für die verschiedenen LightCycle enthält. Die zu einem Button gehörigen Befehle in der Keymap werden jeweils auf eine Funktion gemappt.|-|Config-Datei ist vollständig|Keymap ist initialisiert|
|0.6|TronViewWrapper|`setOnKeyPressed(EventHandler<KeyEvent>)`|Registriert den Eventhandler für Tastendrücke an der Szene aus der TronView|EventHandler ist null|View ist initialisiert|EventHandler ist registriert|
|1.7|TronGame|`startGame(playerCount: int)`|Startet das Spiel|-|View ist initialisiert|Spiel läuft|
|1.8|TronModel|`startGame(playerCount: int)`|Gibt das TronCommand zum Starten des Spiels mit der Anzahl der Spielenden an den aktuellen Zustand|Ist der PlayerCount ungültig oder das Model nicht im StartState geschieht nichts|Zustand ist initialisiert|Spiel läuft|
|1.9|StartState|`execute(command: TronCommand, count: Integer)`|Wenn der TronCommand aus aus dem TronCommand START und einer gültigen Anzahl von Spielern besteht, wird der State auf den WaitingState gesetzt und die Anzahl unter `PlayerCount` im Model gespeichert. Außerdem wird der View über ein Event bescheid gegeben die Anzeige zu updaten|sollte die übergebene Spieleranzahl ungültig sein, wird das TronCommand ignoriert|Der aktuelle State im Model ist der StartState|Der aktuelle State im Model ist der WaitingState|
|1.10|TronViewWrapper|`showWaitingScreen(playerCount: int)`|Zeigt den Ladebildschirm mit der übergebenen Anzahl der Player als Overlay|-|-|Der Ladebildschirm wird angezeigt|
|1.11|WaitingState|`new WaitingState(tronModel: TronModel)`|Der WaitingState wird erzeugt und startet `initLightCycles()` asynchron auf dem Model. |Wenn nach Ablauf einer bestimmten Zeit nicht alle Spieler verfügbar sind, wird zum StartState gewechselt.|-|LightCycles sind initialisiert|
|1.12|WaitingState|`activate()`|Startet einen neuen `LoadingThread(model.initLightCycles())`|-|-|-|
|1.13|TronModel|`initLightCycles()`|Initialisiert die in PlayerCount gespiecherte angegebene Anzahl an LightCycles mit Id, einer einzigartigen Farbe, einer fairen Startposition und einer Direction und aktualisiert dann einmal die View. Anschließend wartet es bis alle Spieler verfügbar sind (bei lokalem Spielen sofort). Dann lässt er das Model in den CountdownState wechseln |-|Die Liste mit LightCycles muss initialisiert sein muss gesetzt sein|Die initialisierten LightCycles sind in der Liste des TronModels gespeichert|
|1.14|TronModel|`initStartingPositions()`|Initialisiert die Position-Liste aller LightCycles mit einer für die Anzahl an Spielenden fairen Startposition und Direction|-|Die LightCycles sind initialisiert| Alle LightCycles haben eine faire Startposition und Direction|
|0.7|TronModel|`changeState(State: state)`|Der State des Models wird auf den übergebenen State gesetzt und auf diesem wird `activate()` aufgerufen|-|-|Das Model ist im übergebenen State|
|❌1.15|~~WaitingState~~|~~`execute(command: TronCommand, count: Integer)`~~|~~Wenn der TronCommand aus aus dem TronCommand ALLREADY und null besteht, wird der State auf den CountdownState gesetzt. Außerdem wird der View über ein Event bescheid gegeben die Anzeige zu updaten~~|-|~~Der aktuelle State im Model ist der WaitingState|Der aktuelle State im Model ist der CountdownState~~|
|1.16|CountdownState|`new CountdownState(tronModel: TronModel)`|Der CountdownState wird erzeugt und ruft im TronModel `startCountdown()` auf |-|-|-|
|1.17|CountdownState|`activate()`|startet den Countdown im Model mit `model.startCountdown()`|-|-|-|
|1.18|TronModel|`startCountdown()`|Gibt der View in einem Abstand von 1 Sekunde per Event Bescheid, runterzuzählen, wenn dieser bei 0 angekommen ist, wird in den GameState gewechselt|-|-|Der Countdown in der View ist beendet.|
|1.19|TronViewWrapper|`showCountdownNumber(countdownNumber: int)`|Zeigt den Countdownscreen mit der übergebenen Zahl als Overlay|-|-|Der Countdownscreen wird angezeigt|
|1.20|GameState|`new GameState(tronModel: TronModel)`|Der GameState wird erzeugt und ruft `startGameLoop()` auf. Ruft `onGameStarted()` auf dem Model auf, was die View benachrichtigt. |-|-|Der Loop läuft mit der übergebenen Frame-Rate.| 
|1.21|GameState|`startGameLoop()`|Startet einen neuen Thread mit der gespeicherten Frame-Rate der immer wieder `update()` des Models aufruft und anschließend bis zum nächsten Tick schläft|Wenn `update()` eine Exception wirft, wird mit einer entsprechenden Fehlermessage in den GameOverState gewechselt|Der GameLoop-Thread läuft noch nicht|Es läuft ein einziger Thread für die GameLoop|
|1.22|TronViewWrapper|`showGame()`|Verbirgt alle Overlays|-|-|Das Spiel wird ohne Overlays angezeigt|
|1.23|TronViewWrapper|`showOwnColor(id: int)`|zeigt auf dem CountdownScreen die Color der übergebenen Id|-|-|-|
|2.1|TronModel|`udpate()`|Ruft auf dem Grid `update()` und danach `getCrashed()` auf. Die daraus erhaltenen gestorbenen Player werden den Observer über ein Event mitgeteilt. Danach wird `getAlive()` auf dem Grid aufgerufen, um anschließend die aktuellen Spielerzustände an Observer zu kommunizieren. Außerdem wird überprüft, ob sich nach einer Kollision noch Spieler*innen auf dem Feld befinden oder ob das Spiel beendet ist. Sollte dies der Fall sein, wird auf dem akutellen State `execute(GAMEOVER, null` aufgerufen und das Event `onGameOver(lightCycles: List<LightCycle>, message: String)` mit der Liste aller Spieler und einer Nachricht aus `getGameOverMessage()` ausgelöst|-|-|-|
|2.2|Grid|`update()`|Berechnet und setzt alle Fahrzeug-Koordinaten (samt ihrer Spuren) für den neuen Loop.|-|Alle Spieler sind mit mindestens einer Coordinate in ihrer Positionsliste und einer Direction initialisiert.|Alle Fahrzeug-Koordinaten sind aktualisiert|
|2.3|Grid|`List<LightCycle> getAlive()`|Gibt alle Player zurück, die noch am Leben sind, also deren `IsAlive` auf `true` steht|-|-|-|
|2.4|ILightCycle|`getCoordinates()`|gibt die Liste der Coordinates des LightCycles zurück|-|-|-|
|2.5|ILightCycle|`getId()`|gibt die ID des LightCycles zurück|-|-|-|
|2.6|TronController|`onKeyPressed(KeyCode)`|Holt sich das passende InputCommand für den KeyCode aus dem Event und führt es auf dem ITronModel aus|Wenn das KeyEvent keinem TronCommand zugewiesen ist, geschieht nichts|Die Keymap muss initialisiert worden sein|-|
|2.7|Keymap|`Action getAction(Keycode)`|Übersetzt den übergebenen Keycode in einen Aufruf der am Model aufgerufen werden kann|Ist für den Keycode keine Übersetzung angelegt, wird null zurückgegeben|Die HashMap mit den Übersetzungen muss initialisiert worden sein|Der Keycode wurde in das passende TronCommand übersetzt und zurückgegeben|
|2.8|TronModel|`setLightCycleTurn(turn: Turn, id: int)`|Ruft `setLightCycleTurn(turn: Turn, id: int)` auf dem Grid auf.|-|-|-|
|2.9|GameState|`execute(LEFT: TronCommand, id: int)`|Ruft setLightCycleTurn(LEFT,id) auf dem Model auf (analog mit RIGHT)|-|-|-|
|2.10|Grid|`setLightCycleTurn(turn: Turn, id: int)`|Setzt den Turn des LightCycle mit der angegebenen Farbe auf den übergebenen Turn|Wenn das LightCycle nicht in der Liste der LightCycles vorhanden ist, geschieht nichts|Die Playerliste des Grids muss intialisiert worden sein|Im LightCycle mit der übergebenen Farbe ist der Turn aktualisiert|
|2.11|LightCycle|`move()`|Aktualisiert die Direction des LightCycle abhängig vom Turn und danach die Position des LightCycle, d.h. es wird eine neue Coordinate an das Ende der Position-Liste angehängt. Anschließend wird die Direction wieder auf STRAIGHT gesetzt.|-|-|Die neue Position steht am Ende der Position-Liste des LightCycle|
|3.1+4.1|Grid|`Set<Coordinate> getCrashed()`|Überprüft bei allen Fahrzeugen, ob sie mit ihren neuen Koordinaten mit anderen Fahreugen, deren Spuren oder der Spielfeldbegrenzung kollidieren. Setzt ggf. das Attribut isAlive bei betreffenden LightCycles. Die Liste mit den gecrashten eindeutigen Koordinaten wird zurückgegeben. |-|siehe `update()`|Attribute isAlive und rank sind bei allen LightCycles aktualisiert|
|3.2+4.2|TronModel|`String getGameOverMessage()`|Gibt je nach Ranking der Spielenden eine Nachricht über das Endergebnis zurück. Hier wird zwischen einem klaren Sieg und einem Unentschieden unterschieden|Alle Player haben ein Ranking|Sollte ein Ranking nicht gesetzt sein, wird eine entsprechende Fehlernachricht zurückgegeben|-|
|4.3|TronViewWrapper|`onDeath(Coordinate)`|Zeigt vorrübergehend eine rote Zelle an der Position des Spielers, der gestorben ist|wenn die Koordinate nicht auf dem sichtbaren Spielfeld ist, passiert nichts|Das Spielfeld wird angezeigt|Das Feld der übergebenen Coordinate ist rot eingefärbt|
|❌5.4|~~GameState~~|~~`execute(GAMEOVER, null)`~~|~~Wechselt zum GameOverState und ruft onGameOver() auf dem Model auf~~|-|-|~~Model ist im GameOverState und View zeigt GameOverScreen~~|
|5.5|IGrid|`List<LightCycle> getResults()`|Erstellt eine Kopie sämtlicher LightCycle-Objekte zur Erstellung einer Ergebnistabelle|-|-|-|
|5.6|TronViewWrapper|`showGameOver(lightCycles: List<LightCycle>, message: String)`|Durch die zuvor im Rank der LightCycles gespeicherte Informationen, wird eine Übersicht zur Platzierung der Player angezeigt|Ist ein Spieler nicht ausreichend initialisiert, wird dieser automatisch letzter|-|Das Overlay für mit den Endergebnissen wird angezeigt|
|5.7|ITronModel|`setOnResetClicked(Action)`|Registriert den Eventhandler|EventHandler ist null|Button ist initialisiert|EventHandler ist registriert|
|5.8|TronMdoel|`onResetClicked()`|Ruft den OnResetClicked EventHandler auf|-|-|EventHandler wurde aufgerufen|
|5.9|TronModel|`resetGame()`|Gibt den Befehl `RESET` an den aktuellen Zustand weiter|-|-|Model ist wieder im Initialzustand|
|5.10|GameOverState|`execute(RESET, null)`|Wechselt zum StartState und ruft onReset() auf dem Model auf|-|-|Model ist wieder im StartState und View zeigt StartScreen|
|5.11|TronViewWrapper|`showStartScreen()`|Zeigt den StartScreen an|-|-|Den StartScreen wird angezeigt|

Dies sind die vom Model ausgelösten Events, die von der View abgefangen werden können:  

| Event | Beschreibung |
|-------|--------------|
|`onReset()`|Wird ausgelöst, wenn das Spiel geresettet wird|
|`onWaiting(playerCount: int)`|Wird ausgelöst, wenn das Spiel geladen wird.|
|`onCountdown(countdownNumber: int)`|Wird ausgelöst, wenn der Countdown zählt.|
|`onGameStarted()`|Wird ausgelöst, wenn das Spiel gestartet wird.|
|`onGameOver(ightCycles: List<LightCycle>, message: String)`|Wird ausgelöst, wenn das Spiel vorbei ist.|
|`onUpdate(lightCycles: List<LightCycle>)`|Wird ausgelöst, wenn die Spieler aktualisiert werden.|
|`onDeath(coordinate: Coordinate)`|Wird ausgelöst, wenn ein Spieler stirbt.|
|`onOwnColorSet(id: int)`|Wird ausgelöst, wenn die Id im RemoteModel bekannt ist|

# Bausteinsicht

### TronController (Controller)
Der Controller dient als zentraler Einstiegspunkt zum Starten der Applikation. Hier werden auch Model und View initialisiert. 

### TronViewWrapper (View)
Ein Wrapper, damit wir die verschiednen Overlays nicht im Controller einzeln erstellen müssen und die Logik für die View gesammelt ist.
Note: Hier könnte später auch per Factory Pattern ein Bot Wrapper eingesetzt werden, um das Spiel gegen einen Computer zu spielen. 

### TronModel (Model)
Im Model findet die eigentliche Spiellogik statt. Hier werden das Spielfeld, die LightCycles, die verschiedenen States und Aktualisierungen verwaltet. Das Model selber schickt nur Events zur Kommunikation und nimmt Befehle über Interfaces entgegen.

![](images/bausteinsicht.png)

# Laufzeitsicht

## Initialisierung
![Initialisierung](images/laufzeitsichten/out/Init.png)

## UC-1 – Spiel starten

### MVC
![MVC](images/laufzeitsichten/out/UC1-MVC/MVC.png)

### TronModel
![Auslöser](images/laufzeitsichten/out/UC1-Model/Ausl%C3%B6ser.png)
![Laden](images/laufzeitsichten/out/UC1-Model/Laden.png.png)
![Countdown](images/laufzeitsichten/out/UC1-Model/Countdown.png)

## UC-2 – Fahrzeug steuern

### MVC
![MVC](images/laufzeitsichten/out/UC2-MVC/MVC.png)

### TronModel
![TronModel](images/laufzeitsichten/out/UC2-Model/Model.png)

### TronViewWrapper
![TronViewWrapper](images/laufzeitsichten/out/UC2-View/View.png)

## UC-3 + UC-4 – Gewinnen / Verlieren

### MVC
![MVC](images/laufzeitsichten/out/UC3-UC4-MVC/MVC.png)

### TronModel
![TronModel](images/laufzeitsichten/out/UC3-UC4-Model/Model.png)

### TronViewWrapper
![TronViewWrapper](images/laufzeitsichten/out/UC3-UC4-View/View.png)

## UC-5 – Ergebnisse anzeigen

### MVC
![Ergebnis](images/laufzeitsichten/out/UC5-MVC/Ergebnis.png)
![Startbildschirm](images/laufzeitsichten/out/UC5-MVC/Startbildschirm.png)

### TronModel
![Ergebnis](images/laufzeitsichten/out/UC5-Model/Ergebnis.png)
![Startbildschirm](images/laufzeitsichten/out/UC5-Model/Model.png)

# Verteilungssicht

![](images/verteilungssicht.png)

Es gibt einen NameService, der für die Synchronisierung der Spiele im REST Mode zur initialen Koordinierung verwendet wird. 

# Querschnittliche Konzepte

### Architekturmuster

Wir haben uns für MVC als Architekturmuster entschieden, da es sich gut für interaktive Anwendung mit GUI eignet. Hierbei kennt der Controller die View und das Model und greift auf ihre Schnittstellen zu. Die View kennt außerdem das Model und greift über seine Schnittstelle darauf zu. Kommunikation in die entgegengesetzte Richtung läuft über das Versenden von Events. 
Die View leitet Events an den Controller und das Model leitet Events an die View. Der Controller ruft anhand der Events die er von der View bekommt das Model auf.

### Protokoll

**Übersetzung von Methoden und Parametern zur Kommunikation über die Middleware**

Da unsere Middleware nur primitive Datentypen und Strings unterstützt, werden vor dem Aufruf auf der Middleware die Methodennamen auf IDs (1 Byte) abgebildet. Diese Übersetzung findet bei uns im `RemoteModel` statt, das die Enum-Klasse `Protocol` verwendet und das Protokoll implementiert. Für die Methoden mit den IDs 3-6 müssen die Parameter in simplere Strukturen übersetzt werden. Dieses Vorgehen wird in der Spalte 'Parameterübersetzung' beschrieben.

|Methode|ID|Parameterübersetzung|
|-------|--|--------------------|
|`onCountdownListener.invoke(int)`|1|Muss nicht übersetzt werden.|
|`onGameStartedListener.invoke()`|2|Kein Parameter zum Übersetzen vorhanden.|
|`onUpdateListener.invoke(Iterable<? extends ILightCycle>)`|3|Nachrichten mit der Methoden-ID 3 übermitteln als Parameter ein int[], das die neuesten Koordinaten für jedes LightCycle enthält. Die Methode `addCoordinatesToLightCycles(int[])` fügt die empfangenen neuesten Koordinaten zu den Koordinatenlisten der jeweiligen lokalen LightCycles hinzu. (Es werden also nicht die ganzen LightCycles gesendet sondern nur das Koordinaten-Update.) Der Update-Listener wird dann mit den aktualisierten LightCycles aufgerufen.|
|`onDeathListener.invoke(Coordinate)`|4|Nachrichten mit der Methoden-ID 4 übermitteln als Parameter ein int[], wobei die ersten beiden Zahlen die Koordinaten darstellen, an der eine Kollision stattfand, also ein Tod ausgelöst wurde. Der Rest des Arrays enthält die IDs der noch lebenden LightCycles, um in `updateLightCyclesOnDeath(Object... args)` das Attribut `isAlive` bei den kollidierten LightCycles aktualisieren zu können.|
|`onGameOverListener.invoke(Iterable<? extends ILightCycle>, String)`|5|Nachrichten mit der Methoden-ID 5 übermitteln als Parameter einen String mit der Gameover-Message. Zum Invoken des Listeners wird dieser String und die lokalen aktuell gehaltenen LightCycles verwendet.|
|`tronModel.setLightCycleTurn(Turn, int)`|6|Nachrichten mit der Methoden-ID 6 übermitteln als Parameter ein int[] der Länge 2, wobei die erste Zahl den Wert des Turn-Enums abbildet (LEFT = 1, RIGHT = 2, STRAIGHT = 3) und der zweite die ID des LightCycles.|
|`onAssignedIdListener.invoke(int)`|7|Nachrichten mit der Methoden-ID 7 übermitteln als Parameter einen Integer, wobei diese Zahl den Offset zur ursprünglichen Id beschreibt.|

### REST

siehe Anhang

# Architekturentscheidungen

## Keymap zweimal übersetzen
Die Keymap, die die individuelle Steuerung der LightCycles zuordnet, wird in der Anwendung zweimal übersetzt:
- Ein Set an Befehlen für die Zuordnung von Richtungen und Spielern, das über die Keymap.properties Datei vom Benutzer geändert werden kann
- Ein davon disjunktes Set an internen Befehlen für die Weiterverarbeitung im Model  

Dadurch bleibt auch bei Änderung der internen Logik die externe API stabil.

# Risiken und technische Schulden

## Java vs Kotlin
Es wird weiterhin Java benutzt und nicht Kotlin, da das Risiko, sich jetzt noch Kotlin aneignen zu müssen, größer ist, als das Risiko, ab und zu eine NullReferenceException zu bekommen.

## config der verteilten Spiele muss gleich sein
Es gibt keine explizite Überprüfung der verschiedenen config Datein. Sollte also die Höhe und Breite des Spielfelds unterschiedlich sein, gibt es keine Garantie, dass das Spiel für alle Peers einwandfrei spielbar ist. 

# Anhang

## REST

Die RESTful API für die Tron Applikation wird mit dem Decorator-Pattern realisiert. Dabei dekorieren wir das TronModel mit einem RestModel. Die Methoden in der folgenden Tabelle sind alle im RestModel zu finden.  
Grundsätzlich implementiert das RestModel auch das `ITronModel` Interface, sodass es zunächst alle dort definierten Methoden implementiert werden. Da nur bestimmte Dinge verändert werden sollen, werden zunächst alle Methoden bis auf die hier aufgeführten direkt deligiert.
Beim Starten der Applikation im REST Mode, kann durch die Auswahl im StartScreen angegeben werden, wie viele Peers über die hiesige Middleware verknüpft werden. Im Anschluss wird bis zur maximalen Anzahl an Spielenden gesucht (6) bzw. maximal 30 Sekunden. 

### Lösungsstrategie
Wie in den Randbedingungen erwähnt, gibt es zur Zeit der Erstellung keinen Zurgriff auf das HAW-Netzwerk. Deswegen schreiben wir die uns exklusiv von ZeroTier zugewiesene IP-Adresse bei einer Registrierung in den NameService. Dadurch ist unserer Spielinstanz bekannt, ob sie Koordinator ist oder nicht. Aus diesem Grund läuft der REST Mode nur mit bestehender und bekannter ZeroTier Verbindung. 

Für die Umsetzung eines Thin-REST-Servers wird die `org.glassfish.grizzly` Bibliothek verwendet. 

|Methode|Ablaufsemantik|
|-|-|
|`startGame(int playerCount)`|Der `playerCount` wird als Anzahl, der bisher gefundenen Spieler*innen gesehen. Über REST werden weitere Spielinstanzen verknüpft|
|`register()`|An den in der config festgelegten NameService wird ein PUT/registration mit dem verwendeten Port `port` und der vorhandenen Spieleranzahl `playerCount` gesendet. Dadurch versucht sich die Instanz selbst als Koordinator beim NameService einzutragen.|
|`registrationsAccepted()`|prüft ob noch Platz für die angefragte Anzahl an Spielenden ist|
|`lookupCoordinator()`|Geht an den NameService, um sich die Adresse eines eventuellen Koordinators zu holen|
|`handleRegistrationRequest()`|Prüft, ob Registrierungen angenommen werden. Dies ist der Fall, wenn man selbst Koordinator ist und noch kein Spiel läuft. Wenn dem so ist, wird geprüft, ob noch genügend Platz im Spiel vorhanden ist. Bei erfolgreicher Prüfung wird der Sender des Requests in einer Liste gespeichert|
|`sendGameConfiguration()`|Sendet die für den Spielstart und zum Identifizieren der anderen Spieler benötigten Informationen an alle Supernodes, die sich registriert haben.|
|`sendSteering(int id, Turn turn)`|Sendet Turn-Befehl mit ID des entsprechenden LightCycles per REST raus.|
|`buildIdMap(List configData)`|Baut eine Map, wodurch die Ids des lokalen RemoteModel auf die zugewiesenen Ids übersetzt wird. Diese werden anschließend an die anderen REST-Peers gesendet und an das lokale ITronModel|

### Protokoll

Auf folgendes Protokoll wurde sich mit zwei weiteren Gruppen zur Kommunikation über REST geeinigt:

Das aktuelle Protokoll ist unter folgender Adresse zu finden: [https://gitlab.com/jessyvere/rest-protocol](https://gitlab.com/jessyvere/rest-protocol)

Wir bauen ein Netzwerkprotokoll auf REST-Basis für das Spiel Light Cycles. Das Protokoll soll es ermöglichen, dass mehrere Clients miteinander spielen können. Die Kommunikation zwischen den Clients soll über ein lokales Netzwerk stattfinden.  

#### Anforderungen

Das Protokoll wird nicht verwendet, um ein Spiel komplett als Server-Client-Anwendung laufen zu lassen wo die gesamte Logik an einem Server läuft. Das Protokoll soll lediglich die Eingaben der Clients an alle anderen Supernode weitergeben, sodass diese dort verarbeitet werden können. Jeder Supernode übernimmt die Verantwortung die Logik für seine Clients und sich selbst zu deligieren oder auszuführen. Somit werden die Spiele in ihren Zuständen synchronisiert.

Es wird davon ausgegangen, dass ein Koordinator gewählt wird bevor die hier definierten Nachrichten verschickt werden, und dass die jeweilige Implementierung selbst bestimmt, wie der Koordinator gefunden wird (z. B. hardcoded oder über einen Nameservice). 

Festgelegt ist, dass die Ressource "tron.coordinator" heißt wie folgt hinterlegt wird:

**HTTP-Methode**: `PUT`  
**Route**: `/entry`  
Beispiel-Payload:  

```json
{
    "name": "tron.coordinator",
    "address":"http://ip:port"
}
``` 

Auch ist selbst zu entscheiden, wie mit einer zu geringen Anzahl an Spielern umgegangen wird (Timer im Koordinator oder weiter warten)

#### Charakterisierung

- Application-Layer
- nicht verbindungsorientiert
- HTTP

#### Schlüsselbegriffe

- "Client": Ein Client ist ein am Spiel beteiligter Prozess der ein LightCycle steuert. 
- Zustände des Spiels: In welche Richtung jedes LightCycle im jeweils nächsten Tick abbiegen möchte und ob das Spiel losgeht.
- "Koordinator": Ein Prozess der ein neues Spiel organisiert. Hier können sich Client zum Spielen registrieren. Er bestimmt, wann das Spiel startet und die Startpositionen und teilt den Clients die Adressen der anderen Clients mit.
- "Supernode": Ein Prozess, der ein Gateway im REST-Netzwerk darstellt. Er ist dafür zuständig, die Nachrichten der per RPC an ihn angeschlossenen Clients per REST-Protokoll (definiert in diesem Dokument) an die anderen Supernodes weiterzuleiten und Nachrichten anderer Supernodes zu verarbeiten, um die Spiele miteinander zu synchronisieren.

#### Ressourcen

- **REGISTRATION**: Teilt dem Koordinator mit, dass man spielen möchte, und enthält die zu registrierende Spieleranzahl. Erwartet eine Antwort, die mitteilt, ob die Registrierung angenommen oder abgelehnt wurde.
- **GAME**: Wenn der Koordinator feststellt, dass die gewünschte Spieleranzahl erreicht ist, wird das Spiel gestartet. Als Nutzdaten werden die Adressen der Supernodes, die IDs der dort angeschlossenen Spieler, die Startpositionen und Startrichtungen aller Spieler übertragen. Diese Nachricht ist auch das Signal zum Starten des Countdowns. 
- **STEERING**: Wird während des Spiels von einem Supernode zu allen anderen Supernodes gesendet, wenn dort ein Spieler steuern möchte. Enthält die Spieler-ID und die Richtung, in die gelenkt werden soll. 

#### Ablaufsemantik

##### PUT REGISTRATION

###### Vorbedingungen
- Es existiert ein Koordinator.
- Supernode kennt die Adresse des Koordinators.
###### Nachbedingungen
- Supernode ist beim Koordinator registriert oder selbst Koordinator
###### Ablauf
1. Supernode sendet REGISTER an den Koordinator
2. Der Koordinator prüft, ob noch Platz vorhanden ist
3. Der Koordinator nimmt die erhaltenen Clients mit der ID/Adresse des Supernodes in eine Liste auf
4. Der Koordinator antwortet dem Supernode mit dem HTTP-Code `200`
5. Supernode bekommt den HTTP-Code `200` zurück
###### Erweiterungsfall 3a) Es ist kein Platz mehr vorhanden
1. Der Koordinator antwortet dem Supernode mit dem HTTP-Code `406` 
2. Supernode bekommt den HTTP-Code `406` zurück
3. Der Supernode wird nicht registriert und startet sein Spiel lokal
###### Erweiterungsfall 3b) Das Spiel ist schon voll bzw. es läuft schon
1. Der Koordinator antwortet dem Supernode mit dem HTTP-Code `410`
2. Supernode bekommt den HTTP-Code `410` zurück
3. Der Supernode trägt sich selbst als Koordinator beim NameServer ein
###### Erweiterungsfall 3c) Der Supernode bekommt keine Antwort
1. Der Supernode hat nach einer Sekunde noch keine Antwort vom Koordinator bekommen
2. Der Supernode trägt sich selbst als Koordinator beim NameServer ein


##### PUT GAME

###### Vorbedingungen
- Nach Verarbeitung eines REGISTERs sind genügend Clients zum Starten eines Spiels vorhanden
- Alle Light Cycles haben definierte Startpositionen, -richtungen und IDs
###### Nachbedingungen
- Alle Supernodes kennen die Startpositionen, Startrichtungen, IDs aller Clients und Adressen aller Supernodes
- Bei allen Clients läuft der Countdown
- Koordinator ignoriert weitere REGISTERs
###### Ablauf
1. Der Koordinator sendet die Startpositionen, -richtungen und IDs an die registrierten Supernodes
2. Die Supernodes setzen die gesendeten Daten in ihrem Spiel
3. Alle Supernodes starten den Countdown

##### PUT STEERING

###### Vorbedingungen
- Das Spiel ist gestartet
###### Nachbedingungen
- Die Richtung des jeweiligen Light Cycles ist dem Inhalt der STEERING-Nachricht entsprechend gesetzt
###### Ablauf
1. Supernode sendet STEERING an alle bekannten Supernodes
2. Alle Supernodes setzen die Richtung des betreffenden Light Cycles neu

#### Fehlersemantik

Wenn eine eingehene Nachricht nicht den hier beschriebenen Formaten entspricht, wird sie ignoriert. 

#### Kodierung

Alle Payloads werden als JSON kodiert mitgesendet.
Die entsprechenden Objekte sind wie folgt aufgebaut.

##### REGISTRATION

**HTTP-Methode**: `PUT`  
**Route**: `/registration`  
Beispiel-Payload:  

```json
{
    "port": 80,
    "playerCount": 3
}
``` 

##### GAME

**HTTP-Methode**: `PUT`  
**Route**: `/game`  
**Anmerkung**: Die LightCycles innerhalb eines Supernodes, besitzen nur direkt aufeinander folgende IDs, welche in aufsteigender Reihenfolge sortiert sind *(bsp. 0,1,2 und nicht 2,0,1)*. Über die enthaltenen IDs werden auch die Supernodes in der Liste aufsteigend sortiert. *(bsp. [{Supernode mit 0,1,2}, {Supernode mit 3,4,5}])*  
Beispiel-Payload:

```json
[
    { // Supernode 1
        "address": "http://localhost:8080",
        "lightCycles": [
            { // LightCycle
                "id": 0,
                "position": {
                    "x": 0,
                    "y": 0
                },
                "direction": "RIGHT"
            },
            {
                "id": 1,
                "position": {
                    "x": 0,
                    "y": 0
                },
                "direction": "RIGHT"
            }
        ]
    },
    { // Supernode 2
        "address": "http://192.168.3.20:8081",
        "lightCycles": [
            {
                "id": 2,
                "position": {
                    "x": 0,
                    "y": 0
                },
                "direction": "RIGHT"
            }
        ]
    }
]
```

##### STEERING

**HTTP-Methode**: `PUT`  
**Route**: `/steering`  
Beispiel-Payload:

```json

{
    "id": 1,
    "turn": "RIGHT"
}
```

### Ablaufdiagramm
**Registration**  
![](images/Registration.png)

### Laufzeitsichten

**startGame**  
![](images/laufzeitsichten/out/REST/startGame.png)  

**setLightCycleTurn intern**  
![](images/laufzeitsichten/out/REST/setLightCycleTurn%20intern.png)  

**setLightCycleTurn extern**  
![](images/laufzeitsichten/out/REST/setLightCycleTurn%20extern.png)  
