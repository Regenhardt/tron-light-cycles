VSP WS22 – Beta 1 – Nina Godenrath, Jessica Moll, Marlon Regenhardt – 8. Januar 2023

Das aktuelle Dokument ist auch unter folgendem Link zu finden: [https://gitlab.com/Regenhardt/tron-light-cycles/-/blob/master/arc42/middleware/LibDoc.md](https://gitlab.com/Regenhardt/tron-light-cycles/-/blob/master/arc42/middleware/LibDoc.md)

# Einführung und Ziele

Um in einem verteilten System mit mehreren Spielern das von uns entwickelte Tron-Spiel zu spielen, ist eine Middleware zur Kommunikation notwendig. Diese ist in diesem Dokument beschrieben.  
Da unser Anwendungsfall keine synchronen Aufrufe erfordert, werden lediglich asynchrone Aufrufe beschrieben, die keine direkte Antwort erwarten.

## Aufgabenstellung

### Use Cases

Es wird angenommen, dass eine Verbindung zwischen drei Instanzen hergestellt werden soll.

![](images/use_case_diagram.png)

### UC-1
- **Titel:** Verbindung aufbauen 
- **Akteur:** Middleware (MW)
- **Ziel:** Die Peers können organisiert Nachrichten austauschen
- **Auslöser:** MW A wird instanziiert
- **Vorbedingungen:** -
- **Nachbedingungen:**
    1. Jede Instanz hat einen Multicast-Socket auf Port 15000
    2. Der (aktuelle) Superpeer hat jeweils einen TCP-Socket an alle Peers
    3. Die Peers haben jeweils einen TCP-Socket zum (aktuellen) Superpeer
- **Erfolgsszenario**
    1. C wird instanziiert
       1. C öffnet einen TCP-Unicast-Empfänger-Socket
       2. C öffnet einen UDP-Socket auf der Multicast-Adresse und hört auf Nachrichten
       3. C schickt an eine Multicast-Adresse ihren Timestamp
    2. A öffnet einen TCP-Unicast-Empfänger-Socket
       1. A schickt an diesselbe Multicast-Adresse ihren Timestamp
    3. B wird instanziiert
       1. B öffnet einen TCP-Unicast-Empfänger-Socket
       2. B schickt an diesselbe Multicast-Adresse ihren Timestamp
    4. A bekommt Nachricht von B mit Timestamp
       1. A hat den älteren Timestamp und ist damit der Superpeer
       2. A schickt an B die Info, dass A der Superpeer ist
    5. B bekommt Nachricht von A mit der Superpeerinfo 
        1. merkt sich A als Superpeer
    6. B bekommt Nachricht von C mit Timestamp und 
        1. ignoriert diese
    7. A bekommt Nachricht von C mit Timestamp
       1. A merkt C hat den älteren Timestamp
       2. A schickt ein Handover mit der aktuellen Peerliste (inklusive A) an C
    8. C bekommt den Handover von A
        1. C schickt an alle Peers aus der Liste die Info, dass C jetzt Superpeer ist
    9. A erhält die Nachricht von C, dass C jetzt Superpeer ist 
       1. merkt sich C als Superpeer
    10. B erhält die Nachricht von C, dass C jetzt Superpeer ist 
        1. merkt sich C als Superpeer
- **Erweiterungsfall 4a): Niemand antwortet (evtl ging die Nachricht verloren)**
  1. A bekommt nach einer vorgegebenen Zeit keine Antwort über Multicast
  2. A schickt an diesselbe Multicast-Adresse den vorher gespeicherten Timestamp
- **Erweiterungsfall 4b): Multicast geht verloren**
  1. A und C bekommen den Timestamp von B
  2. A und C entscheiden aufgrund ihrer ältere Timestamps, dass sie Superpeer sein müssen
  3. B bekommt Nachricht von A mit der Superpeerinfo
  4. B merkt sich A als Superpeer
  5. B bekommt Nachricht von C mit der Superpeerinfo
  6. B bricht die Verbindung zu A ab
  7. B merkt sich C als Superpeer
  8. A merkt, dass die Verbindung zu B geschlossen wurde
  9. A schickt seinen Timestamp an die Multicast Adresse
  10. C erhält den Timestamp von A und entscheidet, dass C der Superpeer ist
  11. weiter mit 9
- **Erweiterungsfall 5a): Aktuelle Anzahl der Peers mitteilen**
  1. A schickt die aktuelle Anzahl der Peers (2) an alle Peers (B)
  2. alle Peers (B) erhalten die Info

### UC-1.1
- **Titel:** Superpeerwahl
- **Akteur:** Middleware
- **Ziel:** Für das Spiel ist ein Superpeer festgelegt
- **Auslöser:** Verbindung zwischen der angestrebten Anzahl an spielwilligen Peers besteht
- **Vorbedingungen:**
    1. Jede Instanz hat einen Multicast-Socket auf Port 15000
    2. Der Superpeer hat jeweils einen TCP-Socket an alle Peers
    3. Die Peers haben jeweils einen TCP-Socket zum Superpeer
- **Nachbedingungen:**
    1. Ein Superpeer steht fest
    2. Alle Peers kennen den Superpeer
    3. Alle Peers sind über UDP-Unicast mit dem Superpeer verbunden
    4. Alle Peers sind über UDP-Multicast mit dem Superpeer verbunden
- **Erfolgsszenario:**
    1. Superpeer erkennt, dass die angestrebte Spieleranzahl erreicht ist
    2. Superpeer würfelt eine neue zufällige Multicast-Domäne aus, die nicht die alte ist
    3. Superpeer sendet durch die vorhandenen TCP-Sockets die neue Multicast-Socket-Adresse an alle Peers, die sich auf dieser registrieren
    4. Sagt der Applikation Bescheid, dass sie als Superpeer nun ein fertiges Netzwerk zur Verfügung hat

### UC-2
- **Titel:** Services registrieren
- **Akteur:** Middleware
- **Ziel:** Benötigte RemoteModel in der Middleware registrieren
- **Auslöser:** Applikation weiß, ob sie Superpeer oder Peer ist
- **Vorbedingungen:**
    1. Alle zu registrierenden Services sind bekannt (je nach Rolle)
- **Nachbedingungen:**
    1. Alle nötigen Services sind registriert
- **Erfolgsszenario:**
    1. Applikation bekommt über ihre Rolle Bescheid
    2. Für alle für die Rolle benötigten Services wird ein Application Stub erzeugt und registriert   

### UC-3
- **Titel:** Funktionsaufrufe empfangen
- **Akteur:** Server-Stub
- **Ziel:** Nachricht empfangen und verarbeiten
- **Auslöser:** Client ruft Methode auf Client-Stub auf
- **Vorbedingungen:**
    1. Es besteht eine Verbindung zu anderen Teilnehmenden über Sockets
    2. Entsprechende ApplicationStubs sind registriert
- **Nachbedingungen:**
    1. Nachricht ist angekommen und verarbeitet
- **Erfolgsszenario:**
    1. Nachricht geht beim Server-Stub ein
    5. Stub entpackt Parameter (Unmarshalling)
    6. Stub ruft entsprechende Methode auf Superpeer auf
    7. Superpeer führt Methode aus

### UC-3.1
- **Titel:** Unmarshalling
- **Akteur:** Server-Stub
- **Ziel:** Eine RPC-Nachricht ist in einen Methodenaufruf übersetzt
- **Auslöser:** Eine RPC-Nachricht ist am Server-Stub eingegangen
- **Vorbedingungen:** - 
- **Nachbedingungen:**
    1. Die RPC-Nachricht ist verarbeitet
- **Erfolgsszenario:**
    1. RPC-Nachricht geht beim Server-Stub ein
    2. Wird über passende Deserialisierung der Parameter in einen Methodenaufruf übersetzt
    3. Die Methode wird am passenden Remote Object des Server-Stubs aufgerufen 
- **Erweiterungsfall 3a**
    1. Der entpackte Methodenaufruf entspricht keinem validen Methodenaufruf
    2. Die Nachricht wird verworfen

### UC-4
- **Titel:** Funktionen aufrufen
- **Akteur:** Application-Stub
- **Ziel:** Nachricht ist beim Empfänger eingegangen
- **Auslöser:** Application-Stub ruft Funktion auf
- **Vorbedingungen:**
    1. Es besteht eine Verbindung zu anderen Teilnehmenden über Sockets
    2. Ein Remote-Object ist im Netzwerk gehostet
- **Nachbedingungen:**
    1. Nachricht ist korrekt verpackt versendet
- **Erfolgsszenario:**
    1. Applikation ruft Remote-Funktion auf
    2. Middleware verpackt den Methodenaufruf samt Parametern (Marshalling)
    3. Middleware sendet Nachricht

### UC-4.1
- **Titel:** Marshalling
- **Akteur:** Client-Stub
- **Ziel:** Methodenaufruf als RPC-Nachricht verpacken
- **Auslöser:** Client-Prozess ruft Methode auf Client-Stub auf
- **Vorbedingungen:** - 
- **Nachbedingungen:**
    1. Methodenaufruf ist als RPC-Nachricht verpackt
- **Erfolgsszenario:**
    1. Methodenaufruf auf Client-Stub
    2. Client-Stub verpackt Methodenaufruf als RPC-Nachricht



## Qualitätsziele

| Qualitätsziel                      | Motivation und Erläuterung
|------------------------------------|---------------------------------------------------------------------|
|Kommunikation mit mehreren Prozessen, sodass 2-6 Spieler an verschiedenen Geräten teilnehmen können (Skalierbarkeit)|Die Middleware soll es ermöglichen, von verschiedenen Geräten aus miteinander spielen zu können, sodass das Spiel als verteiltes System läuft.|
|Schnelle Übertragung (Zuverlässgikeit)|Die Middleware muss die zeitnahe Übertragung von relevanten Nachrichten gewährleisten, damit es einen fairen, synchronen Spielfluss gibt.|
|Verbindungsaufbau zu anderen Gruppen (Kompatibilität, Offenheit)|Zumindest mit den dafür ausgewählten Gruppen aus dem Praktikum muss ein Verbindungsaufbau über eine offene Schnittstelle der  Middleware möglich sein.|
|Zuschnitt auf Tron-Applikation (Kompatibilität)|Die Middleware muss auf die Applikation zugeschnitten sein, da sie ja für ihre Kommunikation da ist.|
|Verteilungstransparenz|Die Spielenden sollten möglichst nicht merken, dass das Spiel als verteiltes System läuft und es überhaupt eine Middleware gibt.|
|Transparency|<ul><li>Access: Die Spielenden wissen nicht, wie die beteiligten Prozesse aufeinander zugreifen (wenn überhaupt).</li><li>Location: Die Applikationen wissen nicht, wo die anderen sich befinden.</li><li>Relocation: Hier nicht gegeben, da kein Ortswechsel stattfindet.</li><li>Migration: Hier nicht gegeben, da keine Migration während des Spiels vorgesehen ist.</li><li>Replication: Hier nicht gegeben, da keine Replikation stattfindet.</li><li>Concurrency: Hier nicht gefordert.</li><li>Failure: Die Applikationen bekommen (kleinere) Fehler nicht mit.</li></li></ul>|
|Openess|Die Middleware verfügt über wohldefinierte Interfaces, ist kompatibel zur Tron-Applikation und erweiterbar.|
|Scaling|<ul><li>Size: Die Anzahl teilnehmender Prozesse ist erweiterbar.</li><li>Geographical: Das Spiel muss in einem LAN laufen.</li><li>Administrative: Administrative Erweiterbarkeit ist nicht vorgesehen.</li></ul>|

# Randbedingungen

**Randbedingungen für die Middleware**

|Randbedingung|Erläuterung|
|-------------|-----------|
|Implementierung in Java|Wir implementieren die Middleware in Java 19.|
|RPC|Die Middleware soll Kommunikation übers RPCs gewährleisten. Javas RMI zu verwenden ist nicht erlaubt.|  
|Lauffähigkeit auf den Rechnern im Informatik-Labor| Letztendlich muss es möglich sein, auf den PCs im Informatik-Labor miteinander zu spielen. |
|Berechnung der Anzahl möglicher Multicast-Gruppen| Da die IPv6 Adressen zu groß für ein Int64 sind, arbeiten wir mit BigInteger|

- auf den Laborrechnern läuft Java 19 nach vorherigem Herunterladen und starten über das Terminal
- es ist Multicast über IPv6 möglich

**Konventionen**

|Konvention|Erläuterung|
|----------|-----------|
|Architekturdokumentation|Terminologie und Gliederung nach dem deutschen arc42-Template.|
|Sprache|Wir verwenden englische Begriffe im Code (Klassen-, Methoden-, Variablennamen etc.) und deutsche Sprache in den Kommentaren, der GUI und allen anderen Dokumenten.|


# Kontextabgrenzung

## Technischer Kontext

![](images/kontextsicht.png)

# Bausteinsicht

![](images/bausteinsicht.png)  

Durch das Nutzen des ByteArrayStreams in Kombination mit dem ObjectStream haben wir uns gegen eigene Klassen für das Un-/Marshalling und Sending/Receiving entschieden. 
siehe [Marshaller/Unmarshaller](##Marshaller/Unmarshaller)

# Lösungsstrategie

## Adressraum
FF05:0:0:0:0:0:0:500-FF05:0:0:0:0:0:0:3485, konfigurierbar  
Die niedrigste Adresse ist die Basisadresse zum Aufbau eines Peer Networks, in diesem Fall also FF05::500.

## Verbindungen zwischen Superpeer und Peers
in config:
- Multicast Basis Adresse: FF05::500
- TCP Port: 15000

### Bei Suche / Wahl
Jeder: Hört auf TCP(15000)  
Jeder: Hört auf Multicast(FF05::40, 15000+{gewünschte Spieleranzahl})  
Neuer Peer: Sendet Timestamp über Multicast  
Superpeer: Sendet über Unicast TCP "I'm Superpeer" oder "You're Superpeer! + peers"  

### Nach Spielstart
Superpeer: Sendet Multicast zu allen Peers (update())  
Superpeer: Sendet einzelne TCP Verbindung zu allen Peers (gameStart(), gameOver()...)  
Superpeer: Empfängt UDP Unicast der einzelnen Peers (setLightCycleTurn())  
Peer: Empfängt auf Multicast (update())  
Peer: Empfängt über TCP vom Superpeer (gameStart(), gameOver()...)  
Peer: Sendet UDP Unicast zum Superpeer (setLightCycleTurn())  

## Klassen
|Klasse|Beschreibung|
|-|-|
|IMiddleware|Bietet an, ein Objekt in einem Netzwerk zu hosten und alle Teilnehmer mit diesem Objekt zu verbinden.|
|ClientStub|Repräsentiert sämtliche verknüpften Remote-Objekte. Aufrufe werden an alle Remote-Objekte übertragen.|
|ServerStub|Repräsentiert den Empfänger für sämtliche verknüpften Peers. Aufrufe an die Applikation können parallel auftreten.|
|PeerFinder|Kümmert sich um den Verbindungsaufbau und Wahl des Teilnehmers, der die Spiellogik ausführt und an alle anderen Peers sendet.|

## Methoden
|Use Case|Objekt|Methode|Ablaufsemantik|
|-|-|-|-|
|1|IMiddleware|`create()`|Kreiert ein Middleware-Objekt mit den Properties aus NETWORK_CONFIG_FILE.|
|1|MiddlewareFacade|`initPeerNetwork(count: Int, configureMiddleware Consumer<IMiddleware,Integer>, registrationFactory*: Consumer<IMiddleware>)`|Findet die übergebene Anzahl an Peers-1 und ruft dann _zuerst_ `configureMiddleware` und anschließend die `registrationFactory` auf lässt vom gewählten Host die registrationFactory aufrufen, um dort das für alle Peers erreichbare RemoteObject zu erstellen. Dabei bekommt `configureMiddleware` zusätzlich eine eindeutige Id, um sich beim gehosteten Objekt zu identifizieren|
|1 + (3.1)|ServerStub|`listenTcp()`|Akzeptiere Verbindungen auf dem vorher festgelegten Port, eingehende Nachrichten werden automatisch unmarshallt|
|1 + (3.1)|ServerStub|`listenUdp()`|Empfange Datagrams auf dem vorher festgelegten Port, eingehende Nachrichten werden automatisch unmarshallt|
|1|PeerFinder|`setConfig()`|Speichert die in der config-Datei festgelegten Adressen (Multicast und TCP-Port) in eigenen Variablen|
|1|PeerFinder|`findPeers(x-1)`|Startet den Wahlalgorithmus, um ein Netzwerk aus der übergebenen Anzahl an Peers zu erstellen.|
|1|PeerFinder|`connectToPeer(senderAddress: InetAddress, port: Int)`|Öffnet eine TCP Verbindung zur `senderAddress` auf Port `port`, sendet die "Ich bin Superpeer"-Nachricht an den resultierenden Socket und merkt sich diesen in einer Liste|
|1|PeerFinder|`targetCountReached()`|Vergleicht die zuvor angefragte Anzahl an Peers mit den schon vorhandenen und gibt zurück, ob die angefrage Anzahl vorhanden ist|
|1|PeerFinder|`handOverSuperpeer(senderAddress: InetAddress )`|Sendet Liste mit bisherigen Peers (inklusive selbst) an neuen Superpeer per TCP. Hierbei wird das Byte, das für die Nachricht "Your're the Superpeer now!" + der bisherigen Anzahl an Peers (bisherige Superpeer + gefundene Mitspieler) + der tatsächlichen Liste an Peer-IP-Adressen|
|1|PeerFinder|`setSuperpeer()`|Geht durch Peer-Socket-Liste und schließt alle vorhandenen Sockets. Fügt Superpeer-Socket zur Liste hinzu.|
|1|PeerFinder|`notifyPeers(count: Short)`|Sendet über TCP die aktuelle Anzahl an Mitspielenden an alle Peers.|
|1.1|PeerFinder|`finishPeerNetwork()`|Würfelt eine neue Muticast-Adresse aus dem Muticast-Adressraum (Dynamic Allocation), die nicht die Startadresse ist. Diese wird als zukünftiger Event-Bus für das Netzwerk genutzt. Setzt `hostObjectLocally` auf `true`.|
|1.1|MiddlewareFacade|`peersFound(PeerNetwork)`|Superpeer baut sich aus den enthaltenen TCP Sockets jeweils einen `Peer`|
|1.1 + 2|MiddlewareFacade|`hostObject()`|Startet Thread mit Socket auf 15.000 der wartet bis die Verbindung zum Host aufgebaut ist.|
|3|ServerStub|`register(Callback)`|Registriert das übergebene Objekt für Aufrufe aus dem PeerNetwork|
|4 + (4.1)|MiddlewareFacade|`invokeReliable(target: PeerId, id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über TCP unicast an den durch `target` identifizierten Peer|
|4 + (4.1)|MiddlewareFacade|`invokeQuick(target: PeerId, id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über UDP unicast an den durch `target` identifizierten Peer|
|4 + (4.1)|MiddlewareFacade|`publishReliable(id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über TCP unicast an alle Peers einzeln|
|4 + (4.1)|MiddlewareFacade|`publishQuick(id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über UDP multicast|
|4 + (4.1)|ClientStub|`sendUdpUnicast(Id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über UDP unicast an das RemoteObject|
|4 + (4.1)|ClientStub|`sendUdpMulticast(Id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten an die Multicast Gruppe|
|4 + (4.1)|ClientStub|`sendTCP(id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über TCP unicast an alle Peers einzeln|
|4 + (4.1)|ClientStub|`sendTCP(target: InetAddress, id: Byte, data: string\|int\|byte[])`|Marshallt die Daten und schickt die übergebene ID und die Daten über TCP unicast an den durch `target` identifizierten Peer|

## NameService
Wir haben uns gegen einen expliziten NameService entschieden, da sich unsere Spieler*innen durch Multicast direkt finden können, solange wir im selben Netzwerk sind. Am ehesten ist also unser PeerFinder mit einem NameService vergleichbar.

## PeerFinder

Nutzt auch ClientStub/ServerStub, um Nachrichten auszutauschen. Es gibt außerdme eine eigene Klasse `MiddlewareProtocol`, in der Nachrichten mit ihrer jeweils zugeordneten ID stehen, um magic numbers zu vermeiden.

## Marshaller/Unmarshaller

Für das Packen und Entpacken der Nachrichten verwenden wir einen ObjectOutStream/ObjectInputStream, bei UDP auch mit ByteArrayStream. Es hat sich gezeigt, dass der ObjectOutputStream trotz der Methode `writeByte`, immer 4 Byte sendet. Dies liegt daran, dass Java das übergebene Byte in einen Integer konvertiert. Daher wird in der aktuellen Implementierung das Byte für die Id der Methode direkt in den ByteArrayOutputStream geschrieben. Als zusätzliche Sicherheit dürfen Ids nur bis 127 vergeben werden, damit keine vorzeichenbehafteten Fehlinterpretationen der Ids auftreten können.

# Aktivitätsdiagramm

![](images/laufzeitsichten/out/anfang.png)

# Laufzeitsichten

**Initialisierung**  
![](images/laufzeitsichten/out/Initialisierung%20Middleware.png)

**Anfangssequenz**  
![](images/laufzeitsichten/out/Anfangssequenz.png)

**Peeranzahl gefunden**  
![](images/laufzeitsichten/out/Peeranzahl%20gefunden.png)

# Querschnittliche Konzepte

Marshaller und Unmarshaller können nur primitive Datentypen, Strings und Listen/Arrays dieser Typen in Bytearrays umwandeln bzw. andersherum. Wenn eine Applikation komplexere Datentypen übertragen will, kann dies durch eine Kodierung mittels XML, JSON, etc. in String geschehen. Dabei muss sich der ApplicationStub um die Übersetzung kümmern.

## Protokolle

### PeerFinder

Jede Nachricht wird mit einem Byte als Methoden-ID kodiert. Dies wird durch den Enum MiddlewareProtocol dargestellt.

|Nachricht|Encoding|Transportprotokoll|Daten|
|-|-|-|-|
|Lookup|127|Multicast(Port 15000+PlayerCount)|Timestamp|
|Handover|126|TCP|ein Byte für Id der Nachricht, Byte für die Anzahl der Adressen, Byte für IP-Version je Adresse, die Adressen|
|Ich bin Superpeer|-|TCP-Verbindungsaufbau|Durch den Verbindungsaufbau per TCP, weiß der Peer, dass dies der Superpeer ist|
|PeerNetworkFinished|125|TCP|ein Integer, der die PeerId darstellt und eine IPv6 Multicast-Adresse|
|Notify|124|TCP|2 Byte mit der Anzahl der aktuell gefundenen Peers|

### Middleware

**Header**  
|||
|-|-|
|ApplicationStub|ein Byte für die Id der Methode|
|Middleware|ein Integer für die Anzahl der Parameter|
|~~Sender~~|~~optional falls UPD: Reihenfolge Bye~~ * zu vernachlässigen, da unsere Nachrichten nicht so groß werden und das Netzwerk gut ist| 

# Risiken und technische Schulden

### Superpeerwahl
- wir benutzen Timestamps zur Bestimmung des Superpeer, was unter Umständen dazu führen kann, dass nicht der tatsächlich erste anfragende Spieler das Spiel hostet. Dies liegt daran, das Timestamps nicht zuverlässig sind. Da die Wahrscheinlichkeit des Eintretens einer so als unfair wahrgenommenen Situation sehr gering ist, nehmen wir dieses Risiko in Kauf. Außerdem hat es im Zweifel keinen Vorteil Superpeer oder Peer zu sein. 

- die Multicast-Nachricht zum Suchen der Spielpartner-Peers kann verloren gehen. Hier könnte man in einem eigenen Thread des PeerFinders den Multicast periodisch wiederholen

### Reelection
Man könnte bei der Superpeerwahl bei verzögerten Nachrichten älterer Teilnehmer eine Reelection starten. Dadurch könnte man Komplikationen der Handshake-Prozesse und eine ungewollte Übernahme Dritter verhindern.  

### Datagram (UDP)
Nachricht könnten zu groß ist für ein Datagram (update mit Liste) werden.
Ideen:
- Datagram braucht Indices, damit wir richtige Reihenfolge einhalten können
- in Array speichern
- müssen auf erste Nachricht warten, um zu erkennen wie groß die Nachricht ist  
🔥 Timeout falls 1. Nachricht zu lange braucht, Array wegschmeissen

### Logische Uhr für Steuerung
Wir gehen davon aus, dass unsere User nicht so schnell Tastenanschläge senden, dass diese sich im Netzwerk überholen können.

### Überprüfung des Absenders
Auf dem derzeitigen Stand ist es den Peers möglich, `setLightCycleTurn` für eine eine ID aufzurufen, die nicht ihre eigene ist. Dies wird im Moment (noch) nicht verhindert, da wir unseren Mitspielern vertrauen!