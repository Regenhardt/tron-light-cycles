package middleware;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

import org.junit.jupiter.api.Test;

class PeerFinderTest {

    @Test
    void testGenerateMulticastInRange() throws IOException {
        var props = new Properties();
        props.setProperty("multicast-start", "FF05::500");
        props.setProperty("multicast-end", "FF05::3485");
        props.setProperty("port", "15000");

        var peerFinder = new PeerFinder(null, props);
        InetAddress newAddress;
        try {
            newAddress = peerFinder.generateMulticastAddress();
            assertTrue(newAddress.isMulticastAddress());
        } catch (Exception e) {
            fail();
        }
    }
}
