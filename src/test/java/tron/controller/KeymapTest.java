package tron.controller;

import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;
import tron.model.ITronModel;
import tron.model.Turn;
import tron.util.Action1;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class KeymapTest {
    // From Keymap
    static final int P1_ID = 0;


    @NotNull
    private static Properties createProperties() {
        Properties props = new Properties();
        props.setProperty(Keymap.P1_LEFT, "Q");
        props.setProperty(Keymap.P1_RIGHT, "W");
        props.setProperty(Keymap.P2_LEFT, "O");
        props.setProperty(Keymap.P2_RIGHT, "P");
        props.setProperty(Keymap.P3_LEFT, "R");
        props.setProperty(Keymap.P3_RIGHT, "T");
        props.setProperty(Keymap.P4_LEFT, "Z");
        props.setProperty(Keymap.P4_RIGHT, "U");
        props.setProperty(Keymap.P5_LEFT, "C");
        props.setProperty(Keymap.P5_RIGHT, "V");
        props.setProperty(Keymap.P6_LEFT, "N");
        props.setProperty(Keymap.P6_RIGHT, "M");
        return props;
    }

    @org.junit.jupiter.api.Test
    void keymap_canBeCreated() {
        Properties props = createProperties();
        assertDoesNotThrow(() -> new Keymap(props));
    }

    @org.junit.jupiter.api.Test
    void getCommand_returnCorrectCommand() {
        // Build keymap
        Properties props = createProperties();
        Keymap keymap = new Keymap(props);
        // ITronModel mock to invoke the command on
        ITronModel model = mock(ITronModel.class);

        // Get command for P1 left
        Action1<ITronModel> command = keymap.getCommand(KeyCode.Q);
        assertNotNull(command);

        // Apply command to ITronModel
        command.invoke(model);
        verify(model, times(1)).setLightCycleTurn(Turn.LEFT, P1_ID);
    }
}