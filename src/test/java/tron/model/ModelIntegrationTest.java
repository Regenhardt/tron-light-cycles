package tron.model;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import javafx.scene.paint.Color;
import tron.model.states.GameState;
import tron.util.Action1;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Erstmal mit Fake States, um nicht auf Countdown warten zu müssen oder die
 * Game Loop zu starten.
 */
public class ModelIntegrationTest {
    // From Keymap
    static final int P1_ID = 0;
    static final int P2_ID = 1;
    static final int P3_ID = 2;

    @org.junit.jupiter.api.Test
    public void setLightCycleTurn_shouldSetTurn() {
        // Arrange
        var grid = new Grid(10, 10);
        var bike = new LightCycle(P1_ID);
        bike.turn = Turn.STRAIGHT;
        grid.setLightCycles(List.of(bike));
        var model = new TronModel(grid, 1);
        model.changeState(new FakeGameState(model));

        // Act
        model.setLightCycleTurn(Turn.LEFT, P1_ID);

        // Assert
        assertEquals(Turn.LEFT, bike.turn);
    }

    @org.junit.jupiter.api.Test
    public void update_shouldMoveBike_andNotifyObserver() {
        // Arrange
        var grid = new Grid(10, 10);
        var bike = new LightCycle(P1_ID);
        var start = new Coordinate(0, 0);
        bike.direction = Direction.EAST;
        bike.turn = Turn.STRAIGHT;
        bike.coordinates.add(start);
        grid.setLightCycles(List.of(bike));
        var model = new TronModel(grid, 1);
        model.changeState(new FakeGameState(model));
        AtomicBoolean triggered = new AtomicBoolean(false);
        Action1<Iterable<? extends ILightCycle>> observer = (cycles) -> triggered.set(true);
        model.setOnUpdate(observer);

        // Act
        model.update();

        // Assert
        assertEquals(start, bike.coordinates.get(0));
        assertEquals(new Coordinate(1, 0), bike.coordinates.get(1));
        assertTrue(triggered.get());
    }

    @org.junit.jupiter.api.Test
    public void update_shouldMoveBikeWithTurn() {
        // Arrange
        var grid = new Grid(10, 10);
        var bike = new LightCycle(P1_ID);
        var start = new Coordinate(0, 0);
        bike.direction = Direction.EAST;
        bike.turn = Turn.RIGHT;
        bike.coordinates.add(start);
        grid.setLightCycles(List.of(bike));
        var model = new TronModel(grid, 1);
        model.changeState(new FakeGameState(model));

        // Act
        model.update();

        // Assert
        assertEquals(start, bike.coordinates.get(0));
        assertEquals(new Coordinate(0, 1), bike.coordinates.get(1));
    }

    @org.junit.jupiter.api.Test
    public void onGameOver_shouldSendRightStats() {
        // Arrange
        var grid = new Grid(5, 5);
        var bike1 = new LightCycle(P1_ID);
        var bike2 = new LightCycle(P2_ID);
        var bike3 = new LightCycle(P3_ID);
        grid.setLightCycles(List.of(bike1, bike2, bike3));
        grid.initStartingPositions();
        LightCycle[] bikeList = { bike1, bike2, bike3 };
        grid.setLightCycles(Arrays.asList(bikeList));
        var model = new TronModel(grid, 1);
        model.changeState(new FakeGameState(model));

        // Assert
        model.setOnGameOver((lightcycles, message) -> {
            assertEquals("We have a winner!", message);

            for (int i = 0; i < bikeList.length; i++) {
                AtomicInteger j = new AtomicInteger(i);
                var result = StreamSupport.stream(lightcycles.spliterator(), false)
                        .filter(lc -> lc.getRank() == j.get() + 1)
                        .findFirst()
                        .orElse(null);

                assertNotNull(result);
                assertEquals(bikeList[bikeList.length - i + 1].getId(), result.getId());
                assertEquals(bikeList[bikeList.length - i + 1].getRank(), result.getRank());
            }
        });

        // Act
        for (int i = 0; i < bikeList.length - 1; i++) {
            bikeList[i].isAlive = false;
            model.update();
        }
    }

    @org.junit.jupiter.api.Test
    public void update_ifOnePlayerDiesAndAnotherDiesOfDiedPlayerRibbon_bothGetDeleted() {
        // Arrange
        var size = 5;
        var grid = new Grid(size, size);
        var bike1 = new LightCycle(P1_ID);
        var bike2 = new LightCycle(P2_ID);
        // bike1 takes up whole first row + crashes into wall next move
        for (int i = 0; i < size; i++) {
            bike1.coordinates.add(new Coordinate(i, 0));
        }
        bike1.direction = Direction.EAST;
        // bike2 crashes into ribbon of bike1 next move
        bike2.coordinates.add(new Coordinate(0, 1));
        bike2.direction = Direction.NORTH;
        grid.setLightCycles(List.of(bike1, bike2));
        var model = new TronModel(grid, 1);
        model.changeState(new FakeGameState(model));

        // Act
        model.update();

        // Assert
        assertFalse(bike1.isAlive);
        assertFalse(bike2.isAlive);
    }

    @org.junit.jupiter.api.Test
    public void update_ifLastTwoPlayersDieAtSameTime_GameOverWithoutWinner() {
        // Arrange
        var size = 5;
        var grid = new Grid(size, size);
        var bike1 = new LightCycle(P1_ID);
        var bike2 = new LightCycle(P2_ID);
        bike1.coordinates.add(new Coordinate(size / 2 - 1, 0));
        bike2.coordinates.add(new Coordinate(size / 2 + 1, 0));
        bike1.direction = Direction.EAST;
        bike2.direction = Direction.WEST;
        var model = new TronModel(grid, 1);
        model.changeState(new FakeGameState(model));

        // Assert
        model.setOnGameOver((lightcycles, message) -> {
            assertEquals("No one wins...", message);

            lightcycles.forEach(lc -> assertTrue(lc.getRank() != 1));
        });

        // Act
        model.update();
    }

    @org.junit.jupiter.api.Test
    public void update_ifMultiplePlayerDieAtSimeTime_shouldGetSameRanking() {
    }
}

class FakeGameState extends GameState {
    public FakeGameState(TronModel model) {
        super(model);
    }

    @Override
    public void activate() {
        // Do not start game loop
    }
}