package tron.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DirectionTest {

    private Direction north, south, west, east;

    @BeforeEach
    void setup() {
        north = Direction.NORTH;
        south = Direction.SOUTH;
        west = Direction.WEST;
        east = Direction.EAST;
    }

    @Test
    void turn_shouldTurnInCorrectDirection() {
        // arrange in setup

        // act & assert
        assertEquals(Direction.WEST, north.turn(Turn.LEFT));
        assertEquals(Direction.EAST, north.turn(Turn.RIGHT));
        assertEquals(Direction.EAST, south.turn(Turn.LEFT));
        assertEquals(Direction.WEST, south.turn(Turn.RIGHT));
        assertEquals(Direction.SOUTH, west.turn(Turn.LEFT));
        assertEquals(Direction.NORTH, west.turn(Turn.RIGHT));
        assertEquals(Direction.NORTH, east.turn(Turn.LEFT));
        assertEquals(Direction.SOUTH, east.turn(Turn.RIGHT));
    }

    @Test
    void applyTo_shouldReturnCorrectNewCoordinate() {
        // arrange
        Coordinate currentCoordinate = new Coordinate(10, 10);

        // act and assert
        assertEquals(new Coordinate(10, 9), north.applyTo(currentCoordinate));
        assertEquals(new Coordinate(10, 11), south.applyTo(currentCoordinate));
        assertEquals(new Coordinate(9, 10), west.applyTo(currentCoordinate));
        assertEquals(new Coordinate(11, 10), east.applyTo(currentCoordinate));
    }
}