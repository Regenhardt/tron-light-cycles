package tron.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LightCycleTest {

    private LightCycle lightCycle;
    private ArrayList<Coordinate> coordinatesFromLightCycle;

    @BeforeEach
    void setup() {
        // initial direction is EAST
        lightCycle = new LightCycle(0);
        lightCycle.coordinates.add(new Coordinate(1, 10));
    }

    @Test
    void getId_shouldReturnCorrectId() {
        // arrange in setup

        // act and assert
        assertEquals(0, lightCycle.getId());
    }

    @Test
    void getCoordinates_shouldReturnCorrectCoordinates() {
        // arrange
        coordinatesFromLightCycle = (ArrayList<Coordinate>) lightCycle.getCoordinates();

        // act and assert
        assertTrue(coordinatesFromLightCycle.contains(new Coordinate(1, 10)));
        assertEquals(1, coordinatesFromLightCycle.size());
    }

    @Test
    void move_shouldUpdateDirectionIfTurnIsSet_andAddCorrectCoordinate() {
        // arrange (direction is EAST)
        lightCycle.turn = Turn.RIGHT;

        // act
        lightCycle.move();
        coordinatesFromLightCycle = (ArrayList<Coordinate>) lightCycle.getCoordinates();

        // assert
        assertEquals(Turn.STRAIGHT, lightCycle.turn);
        assertEquals(Direction.SOUTH, lightCycle.direction);
        assertTrue(coordinatesFromLightCycle.contains(new Coordinate(1, 10)));
        assertTrue(coordinatesFromLightCycle.contains(new Coordinate(1, 11)));
        assertEquals(2, coordinatesFromLightCycle.size());
    }

    @Test
    void move_shouldUpdateWithoutDirectionChangeIfTurnIsNotSet_andAddCorrectCoordinate() {
        // arrange in setup (direction is EAST and turn is not set)

        // act
        lightCycle.move();
        coordinatesFromLightCycle = (ArrayList<Coordinate>) lightCycle.getCoordinates();

        // assert
        assertEquals(Turn.STRAIGHT, lightCycle.turn);
        assertEquals(Direction.EAST, lightCycle.direction);
        assertTrue(coordinatesFromLightCycle.contains(new Coordinate(1, 10)));
        assertTrue(coordinatesFromLightCycle.contains(new Coordinate(2, 10)));
        assertEquals(2, coordinatesFromLightCycle.size());
    }
}