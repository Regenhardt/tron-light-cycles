package tron.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GridTest {

    private LightCycle lc1, lc2, lc3, lc4, lc5, lc6;
    private Grid grid;

    @BeforeEach
    void setup() {
        lc1 = new LightCycle(0);
        lc2 = new LightCycle(1);
        lc3 = new LightCycle(2);
        lc4 = new LightCycle(3);
        lc5 = new LightCycle(4);
        lc6 = new LightCycle(5);

        grid = new Grid(80, 100);
    }

    @Test
    void initStartingPositions_shouldInitStartingPositionsForTwoPlayers() {
        // arrange
        ArrayList<LightCycle> lightCycles = new ArrayList<>(Arrays.asList(lc1, lc2));
        grid.setLightCycles(lightCycles);

        // act
        grid.initStartingPositions();

        // assert
        ArrayList<LightCycle> lightCyclesFromGrid = (ArrayList<LightCycle>) grid.getLightCycles();
        assertIterableEquals(lightCycles, lightCyclesFromGrid);
        assertIterableEquals(lightCycles, grid.getAlive());
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 40) }), lightCyclesFromGrid.get(0).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 40) }), lightCyclesFromGrid.get(1).coordinates);
    }

    @Test
    void initStartingPositions_shouldInitStartingPositionsForThreePlayers() {
        // arrange
        ArrayList<LightCycle> lightCycles = new ArrayList<>(Arrays.asList(lc1, lc2, lc3));
        grid.setLightCycles(lightCycles);

        // act
        grid.initStartingPositions();

        // assert
        ArrayList<LightCycle> lightCyclesFromGrid = (ArrayList<LightCycle>) grid.getLightCycles();
        assertIterableEquals(lightCycles, lightCyclesFromGrid);
        assertIterableEquals(lightCycles, grid.getAlive());
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 26) }), lightCyclesFromGrid.get(0).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 26) }), lightCyclesFromGrid.get(1).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(50, 71) }), lightCyclesFromGrid.get(2).coordinates);
    }

    @Test
    void initStartingPositions_shouldInitStartingPositionsForFourPlayers() {
        // arrange
        ArrayList<LightCycle> lightCycles = new ArrayList<>(Arrays.asList(lc1, lc2, lc3, lc4));
        grid.setLightCycles(lightCycles);

        // act
        grid.initStartingPositions();

        // assert
        ArrayList<LightCycle> lightCyclesFromGrid = (ArrayList<LightCycle>) grid.getLightCycles();
        assertIterableEquals(lightCycles, lightCyclesFromGrid);
        assertIterableEquals(lightCycles, grid.getAlive());
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 16) }), lightCyclesFromGrid.get(0).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 16) }), lightCyclesFromGrid.get(1).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 63) }), lightCyclesFromGrid.get(2).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 63) }), lightCyclesFromGrid.get(3).coordinates);
    }

    @Test
    void initStartingPositions_shouldInitStartingPositionsForFivePlayers() {
        // arrange
        ArrayList<LightCycle> lightCycles = new ArrayList<>(Arrays.asList(lc1, lc2, lc3, lc4, lc5));
        grid.setLightCycles(lightCycles);

        // act
        grid.initStartingPositions();

        // assert
        ArrayList<LightCycle> lightCyclesFromGrid = (ArrayList<LightCycle>) grid.getLightCycles();
        assertIterableEquals(lightCycles, lightCyclesFromGrid);
        assertIterableEquals(lightCycles, grid.getAlive());
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 26) }), lightCyclesFromGrid.get(0).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(50, 8) }), lightCyclesFromGrid.get(1).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 26) }), lightCyclesFromGrid.get(2).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(90, 72) }), lightCyclesFromGrid.get(3).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 72) }), lightCyclesFromGrid.get(4).coordinates);
    }

    @Test
    void initStartingPositions_shouldInitStartingPositionsForSixPlayers() {
        // arrange
        ArrayList<LightCycle> lightCycles = new ArrayList<>(Arrays.asList(lc1, lc2, lc3, lc4, lc5, lc6));
        grid.setLightCycles(lightCycles);

        // act
        grid.initStartingPositions();

        // assert
        ArrayList<LightCycle> lightCyclesFromGrid = (ArrayList<LightCycle>) grid.getLightCycles();
        assertIterableEquals(lightCycles, lightCyclesFromGrid);
        assertIterableEquals(lightCycles, grid.getAlive());
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 8) }), lightCyclesFromGrid.get(0).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 8) }), lightCyclesFromGrid.get(1).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 40) }), lightCyclesFromGrid.get(2).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(89, 71) }), lightCyclesFromGrid.get(3).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 71) }), lightCyclesFromGrid.get(4).coordinates);
        assertEquals(List.of(new Coordinate[] { new Coordinate(10, 40) }), lightCyclesFromGrid.get(5).coordinates);
    }

    @Test
    void setLightCycleTurn_shouldSetTurnForLightCycleCorrectly() {
        // arrange
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2, lc3 }));

        // act
        grid.setLightCycleTurn(Turn.RIGHT, lc1.getId());
        grid.setLightCycleTurn(Turn.LEFT, lc2.getId());
        grid.setLightCycleTurn(Turn.STRAIGHT, lc3.getId());

        // assert
        grid.getLightCycles().forEach(lc -> {
            if (lc1.getId() == lc.getId()) {
                assertEquals(Turn.RIGHT, lc.turn);
            } else if (lc2.getId() == lc.getId()) {
                assertEquals(Turn.LEFT, lc.turn);
            } else if (lc3.getId() == lc.getId()) {
                assertEquals(Turn.STRAIGHT, lc.turn);
            }
        });
    }

    @Test
    void update_shouldUpdateGridCorrectly() {
        // arrange
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2, lc3 }));
        grid.initStartingPositions();
        grid.setLightCycleTurn(Turn.RIGHT, lc1.getId());
        grid.setLightCycleTurn(Turn.LEFT, lc2.getId());
        grid.setLightCycleTurn(Turn.STRAIGHT, lc3.getId());

        // act
        grid.update();

        // assert
        grid.getLightCycles().forEach(lc -> {
            if (lc1.getId() == lc.getId()) {
                assertEquals(List.of(new Coordinate[] { new Coordinate(10, 26), new Coordinate(10, 27) }),
                        lc.coordinates);
            } else if (lc2.getId() == lc.getId()) {
                assertEquals(List.of(new Coordinate[] { new Coordinate(89, 26), new Coordinate(89, 27) }),
                        lc.coordinates);
            } else if (lc3.getId() == lc.getId()) {
                assertEquals(List.of(new Coordinate[] { new Coordinate(50, 71), new Coordinate(50, 70) }),
                        lc.coordinates);
            }
        });

    }

    @Test
    void getCrashed_shouldReturnCoordinatesOfCrashesInThisFrame() {
        // arrange
        lc1.coordinates = List.of(new Coordinate[] { new Coordinate(0, 50), new Coordinate(-1, 50) });
        lc2.coordinates = List.of(new Coordinate[] { new Coordinate(50, 9), new Coordinate(50, 10) });
        lc3.coordinates = List.of(new Coordinate[] { new Coordinate(50, 11), new Coordinate(50, 10) });
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2, lc3 }));

        // act
        Set<Coordinate> crashCoordinates = grid.getCrashed();

        // assert
        assertEquals(2, crashCoordinates.size());
        assertTrue(crashCoordinates.contains(new Coordinate(0, 50)));
        assertTrue(crashCoordinates.contains(new Coordinate(50, 10)));
    }

    @Test
    void getAlive_shouldReturnOnlyLightCyclesThatAreStillAlive() {
        // arrange
        lc1.isAlive = false;
        lc2.isAlive = true;
        lc3.isAlive = true;
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2, lc3 }));

        // act
        List<LightCycle> alive = grid.getAlive();

        // assert
        assertIterableEquals(List.of(new LightCycle[] { lc2, lc3 }), alive);
    }

    @Test
    void getResults_withTwoLosersAndOneWinner_shouldReturnRankedLightCycles() {
        // arrange
        // lc1 crashing west
        lc1.coordinates = List.of(new Coordinate[] { new Coordinate(0, 50), new Coordinate(-1, 50) });
        lc1.turn = Turn.STRAIGHT;
        lc1.direction = Direction.WEST;
        // lc2 not crashing
        lc2.coordinates = List.of(new Coordinate[] { new Coordinate(50, 9), new Coordinate(50, 10) });
        lc2.turn = Turn.STRAIGHT;
        lc2.direction = Direction.NORTH;
        // lc3 crashing north
        lc3.coordinates = List.of(new Coordinate[] { new Coordinate(50, 0), new Coordinate(50, -1) });
        lc3.turn = Turn.STRAIGHT;
        lc3.direction = Direction.NORTH;
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2, lc3 }));

        // act
        Set<Coordinate> crashed = grid.getCrashed();
        List<LightCycle> result = grid.getResults();

        // assert every light cycle has the correct rank
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc1.getId() && lc.rank == 3));
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc2.getId() && lc.rank == 1));
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc3.getId() && lc.rank == 3));
    }

    @Test
    void getResults_withThreePlayers_shouldReturnRankedLightCycles() {
        // arrange
        // lc1 crashing west
        lc1.coordinates = new ArrayList<>(List.of(new Coordinate[] { new Coordinate(0, 50), new Coordinate(-1, 50) }));
        lc1.turn = Turn.STRAIGHT;
        lc1.direction = Direction.WEST;
        // lc2 crashing after next move
        lc2.coordinates = new ArrayList<>(List.of(new Coordinate[] { new Coordinate(50, 1), new Coordinate(50, 0) }));
        lc2.turn = Turn.STRAIGHT;
        lc2.direction = Direction.NORTH;
        // lc3 crashing in two moves
        lc3.coordinates = new ArrayList<>(List.of(new Coordinate[] { new Coordinate(40, 2), new Coordinate(40, 1) }));
        lc3.turn = Turn.STRAIGHT;
        lc3.direction = Direction.NORTH;
        // lc4 would have crashed in three moves
        lc4.coordinates = new ArrayList<>(List.of(new Coordinate[] { new Coordinate(30, 3), new Coordinate(30, 2) }));
        lc4.turn = Turn.STRAIGHT;
        lc4.direction = Direction.NORTH;
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2, lc3, lc4 }));

        // act
        Set<Coordinate> crashedFirst = grid.getCrashed(); // lc1 should be crashed already
        grid.update(); // let lc2 crash
        Set<Coordinate> crashedSecond = grid.getCrashed();
        grid.update(); // let lc3 crash
        Set<Coordinate> crashedThird = grid.getCrashed();
        List<LightCycle> result = grid.getResults();

        // assert every light cycle has the correct rank
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc1.getId() && lc.rank == 4));
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc2.getId() && lc.rank == 3));
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc3.getId() && lc.rank == 2));
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc4.getId() && lc.rank == 1));
    }

    @Test
    void getResults_withTwoLosersAndNoWinner_shouldReturnRankedLightCycles() {
        // arrange
        // lc1 crashing west
        lc1.coordinates = List.of(new Coordinate[] { new Coordinate(0, 50), new Coordinate(-1, 50) });
        lc1.turn = Turn.STRAIGHT;
        lc1.direction = Direction.WEST;
        // lc2 crashing north
        lc2.coordinates = List.of(new Coordinate[] { new Coordinate(50, 0), new Coordinate(50, -1) });
        lc2.turn = Turn.STRAIGHT;
        lc2.direction = Direction.NORTH;
        grid.setLightCycles(List.of(new LightCycle[] { lc1, lc2 }));

        // act
        Set<Coordinate> crashed = grid.getCrashed();
        List<LightCycle> result = grid.getResults();

        // assert every light cycle has the correct rank
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc1.getId() && lc.rank == 2));
        assertTrue(result.stream().anyMatch(lc -> lc.getId() == lc2.getId() && lc.rank == 2));
    }
}