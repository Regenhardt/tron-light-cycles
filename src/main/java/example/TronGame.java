package example;

import java.io.FileInputStream;
import java.util.Properties;

import javafx.application.Application;
import javafx.stage.Stage;
import tron.controller.TronController;
import tron.model.GameMode;
import tron.model.ITronModel;
import tron.view.TronViewWrapper;

public class TronGame extends Application {

    public final static String VIEW_CONFIG_FILE = "view.properties";
    public final static String KEYMAP_CONFIG_FILE = "keymap.properties";
    private TronController controller;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("TRON - Light Cycles");
        Properties viewProps = new Properties();
        viewProps.load(new FileInputStream(VIEW_CONFIG_FILE));
        var args = this.getParameters().getUnnamed();
        GameMode mode;
        var modeArg = args.isEmpty() ? null : args.get(0).toLowerCase();
        if (args.isEmpty() || modeArg.equals("local")) {
            mode = GameMode.LOCAL;
        } else if (modeArg.equals("remote")) {
            mode = GameMode.REMOTE;
        } else if (modeArg.equals("rest")) {
            mode = GameMode.REST;
        } else {
            throw new Exception("Invalid Game Mode");
        }

        ITronModel model = ITronModel.buildModel(
                mode,
                Integer.parseInt(viewProps.getProperty("rows")),
                Integer.parseInt(viewProps.getProperty("columns")),
                Integer.parseInt(viewProps.getProperty("tickrate")));
        TronViewWrapper wrapper = new TronViewWrapper(VIEW_CONFIG_FILE, model, stage);
        Properties keymapProps = new Properties();
        keymapProps.load(new FileInputStream(KEYMAP_CONFIG_FILE));
        TronController controller = new TronController(keymapProps, model, wrapper);

        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
