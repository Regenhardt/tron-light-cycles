package tron.view;

import java.util.ArrayList;
import java.util.Map;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Pair;

public class CountdownScreen extends StackPane {
    public final static String NAME = "countdown";
    private final Label message;
    private final Label countdownLabel;

    private final HBox hboxControls;
    private final Map<Integer, Color> idToColor;
    private ArrayList<Pair<Integer, ArrayList<KeyCode>>> controlSettings;

    public CountdownScreen(String stylesheet, Map<Integer, Color> idToColor) {
        this.getStylesheets().add(stylesheet);
        this.setAlignment(Pos.BOTTOM_CENTER);
        this.idToColor = idToColor;

        this.message = new Label("Game begins in:");
        this.message.setStyle("-fx-font-size: 20px;");
        this.countdownLabel = new Label("0");
        this.countdownLabel.setStyle("-fx-font-size: 80px;");

        var a = new VBox();
        a.setAlignment(Pos.CENTER);

        hboxControls = new HBox(23.0);
        hboxControls.setAlignment(Pos.CENTER);
        hboxControls.getChildren().add(countdownLabel);
        a.getChildren().add(message);
        a.getChildren().add(countdownLabel);
        BorderPane b = new BorderPane();
        b.setBottom(hboxControls);

        this.getChildren().add(a);
        this.getChildren().add(b);
    }

    public void setCountdownNumber(int countdown) {
        Platform.runLater(() -> this.countdownLabel.setText(Integer.toString(countdown)));
    }

    public void showOwnColor(Integer id) {
        Color color = idToColor.get(id);

        // control buttons
        Label leftArrow = new Label("←");
        Label rightArrow = new Label("→");
        Button leftButton = new Button(controlSettings.get(0).getValue().get(0).getName());
        Button rightButton = new Button(controlSettings.get(0).getValue().get(1).getName());
        leftButton.setDisable(true);
        rightButton.setDisable(true);

        // colorlanes
        HBox colorLaneBox = new HBox();
        colorLaneBox.setPadding(new Insets(10, 0, 0, 0));
        colorLaneBox.setAlignment(Pos.TOP_CENTER);
        Rectangle colorLane = new Rectangle(10, 200);

        // arrow + button boxes
        VBox leftControlBox = new VBox();
        VBox rightControlBox = new VBox();
        leftControlBox.setAlignment(Pos.TOP_CENTER);
        rightControlBox.setAlignment(Pos.TOP_CENTER);

        // colorlane box
        HBox playerControl = new HBox(8.0);
        playerControl.setMaxHeight(50);
        playerControl.setAlignment(Pos.TOP_CENTER);
        Platform.runLater(() -> {
            DropShadow borderGlow = new DropShadow();
            colorLane.setFill(color);
            borderGlow.setColor(color);
            colorLane.setEffect(borderGlow);
            hboxControls.getChildren().clear();

            playerControl.getChildren().addAll(leftControlBox, colorLaneBox, rightControlBox);
            leftControlBox.getChildren().addAll(leftArrow, leftButton);
            rightControlBox.getChildren().addAll(rightArrow, rightButton);
            colorLaneBox.getChildren().add(colorLane);
            hboxControls.getChildren().add(playerControl);
        });
    }

    public void setControlSettings(ArrayList<Pair<Integer, ArrayList<KeyCode>>> controlSettings) {
        this.controlSettings = controlSettings;
        showAllControls();
    }

    private void showAllControls() {
        for (Pair<Integer, ArrayList<KeyCode>> controls : controlSettings) {
            Color color = idToColor.get(controls.getKey());

            // control buttons
            Label leftArrow = new Label("←");
            Label rightArrow = new Label("→");
            Button leftButton = new Button(controls.getValue().get(0).getName());
            Button rightButton = new Button(controls.getValue().get(1).getName());
            leftButton.setDisable(true);
            rightButton.setDisable(true);

            // colorlanes
            HBox colorLaneBox = new HBox();
            colorLaneBox.setPadding(new Insets(10, 0, 0, 0));
            colorLaneBox.setAlignment(Pos.TOP_CENTER);
            Rectangle colorLane = new Rectangle(10, 200);
            DropShadow borderGlow = new DropShadow();
            colorLane.setFill(color);
            borderGlow.setColor(color);
            colorLane.setEffect(borderGlow);

            // arrow + button boxes
            VBox leftControlBox = new VBox();
            VBox rightControlBox = new VBox();
            leftControlBox.setAlignment(Pos.TOP_CENTER);
            rightControlBox.setAlignment(Pos.TOP_CENTER);

            // colorlane box
            HBox playerControl = new HBox(8.0);
            playerControl.setMaxHeight(50);
            playerControl.setAlignment(Pos.TOP_CENTER);

            playerControl.getChildren().addAll(leftControlBox, colorLaneBox, rightControlBox);
            leftControlBox.getChildren().addAll(leftArrow, leftButton);
            rightControlBox.getChildren().addAll(rightArrow, rightButton);
            colorLaneBox.getChildren().add(colorLane);
            hboxControls.getChildren().add(playerControl);
        }
    }
}
