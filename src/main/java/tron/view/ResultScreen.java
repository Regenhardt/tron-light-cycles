package tron.view;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import tron.model.ILightCycle;
import tron.util.Action;

public class ResultScreen extends VBox {
    public final static String NAME = "result";
    private final int MAX_PLAYERS = 6;
    private final Label labelResult;
    private final Button buttonReset;
    private final GridPane ranking;
    private final Map<Integer, Color> idToColor;

    public ResultScreen(String stylesheet, Map<Integer, Color> idToColor) {
        super(20.0);
        this.getStylesheets().add(stylesheet);
        this.setAlignment(Pos.CENTER);
        this.idToColor = idToColor;

        labelResult = new Label();
        this.getChildren().add(labelResult);

        // Add a row for each player to the grid
        ranking = new GridPane();
        ranking.setAlignment(Pos.CENTER);
        ranking.setHgap(20);
        ranking.setVgap(20);
        ranking.addColumn(0);
        ranking.addColumn(1);
        for (int i = 0; i < MAX_PLAYERS; i++) {
            ranking.addRow(i);
        }
        this.getChildren().add(ranking);

        buttonReset = new Button("Reset");
        this.getChildren().add(buttonReset);
    }

    public void setResults(Iterable<? extends ILightCycle> lightCycles, String message) {
        Platform.runLater(() -> this.labelResult.setText(message));

        List<Label> labels = new ArrayList<>();
        List<Rectangle> rectangles = new ArrayList<>();
        for (ILightCycle lightCycle : lightCycles) {
            int rank = lightCycle.getRank();
            Rectangle color = new Rectangle(70 - (10 * rank), 20);
            color.setFill(idToColor.get(lightCycle.getId()));
            Label label = new Label(String.valueOf(rank));
            labels.add(label);
            rectangles.add(color);
        }

        Platform.runLater(() -> {
            ranking.getChildren().clear();
            for (int i = 0; i < labels.size(); i++) {
                ranking.add(labels.get(i), 0, i);
                ranking.add(rectangles.get(i), 1, i);
            }
        });
    }

    public void setOnResetClicked(Action resetClickedAction) {
        buttonReset.setOnAction(event -> resetClickedAction.invoke());
    }
}
