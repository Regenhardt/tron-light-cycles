package tron.view;

import edu.cads.bai5.vsp.tron.view.ViewUtility;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import tron.util.Action1;

public class StartScreen extends VBox {
    private final static int MIN_PLAYERS = 2;
    private final static int MAX_PLAYERS = 6;
    public final static String NAME = "start";

    private final Label labelReady;
    private final HBox hboxPlayerCount;
    private final Button decreaseCountButton;
    private final Label playerCountField;
    private final Button increaseCountButton;
    private final Button btnStart;

    private Integer playerCount = 2;

    private Action1<Integer> startClicked;

    public StartScreen(String stylesheet) {
        super(20.0);
        this.getStylesheets().add(stylesheet);
        this.setAlignment(Pos.CENTER);

        labelReady = new Label("How many players?");
        labelReady.setStyle(
                "-fx-text-fill: " + ViewUtility.getHexTriplet(Color.PAPAYAWHIP.brighter()) +
                        ";");
        this.getChildren().add(labelReady);

        hboxPlayerCount = new HBox(20.0);
        hboxPlayerCount.setAlignment(Pos.CENTER);

        playerCountField = new Label(playerCount.toString());
        playerCountField.setStyle("-fx-text-fill: " +
                ViewUtility.getHexTriplet(Color.PAPAYAWHIP.brighter()) + ";");
        playerCountField.setPrefWidth(30);
        playerCountField.setAlignment(Pos.CENTER);

        decreaseCountButton = new Button("-");
        decreaseCountButton.setStyle("-fx-font-weight: bold;");
        decreaseCountButton.setOnAction(event -> {
            if (playerCount > MIN_PLAYERS) {
                playerCount--;
                Platform.runLater(() -> {
                    playerCountField.setText(playerCount.toString());
                });
            }
        });

        increaseCountButton = new Button("+");
        increaseCountButton.setOnAction(event -> {
            if (playerCount < MAX_PLAYERS) {
                playerCount++;
                Platform.runLater(() -> {
                    playerCountField.setText(playerCount.toString());
                });
            }
        });

        hboxPlayerCount.getChildren().addAll(decreaseCountButton, playerCountField,
                increaseCountButton);
        this.getChildren().add(hboxPlayerCount);

        btnStart = new Button("START GAME");
        btnStart.setStyle("-fx-font: bold 25px \"Monospace\";");
        btnStart.setOnAction(event -> onStartButtonClicked());
        this.getChildren().add(btnStart);
    }

    private void onStartButtonClicked() {
        if (startClicked != null) {
            startClicked.invoke(playerCount);
        }
    }

    public void setOnStartClicked(Action1<Integer> startClickedAction) {
        this.startClicked = startClickedAction;
    }

}