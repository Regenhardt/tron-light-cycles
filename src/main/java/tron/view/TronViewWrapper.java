package tron.view;

import java.io.IOException;
import java.util.ArrayList;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import edu.cads.bai5.vsp.tron.view.ITronView;
import edu.cads.bai5.vsp.tron.view.TronView;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Pair;
import tron.model.ITronModel;
import tron.model.ILightCycle;
import tron.util.Action;
import tron.util.Action1;

public class TronViewWrapper implements ITronViewWrapper {
    private final ITronView view;
    private final StartScreen startScreen;
    private final WaitingScreen waitingScreen;
    private final ResultScreen resultScreen;
    private final CountdownScreen countdownScreen;
    private Map<Integer, Color> idToColor;

    public TronViewWrapper(String viewConfigFile, ITronModel model, Stage stage)
            throws NumberFormatException, IOException {
        this.view = new TronView(viewConfigFile);
        initializeIdToColorMap();

        startScreen = new StartScreen("menu.css");
        view.registerOverlay(StartScreen.NAME, startScreen);

        waitingScreen = new WaitingScreen("menu.css");
        view.registerOverlay(WaitingScreen.NAME, waitingScreen);
        model.setOnWaiting(this::showWaitingScreen);
        model.setOnPeerCountUpdate(this::showPeerCount);

        countdownScreen = new CountdownScreen("menu.css", idToColor);
        view.registerOverlay(CountdownScreen.NAME, countdownScreen);
        model.setOnCountdown(this::showCountdownScreen);
        model.setOnIdAssigned(this::showOwnColor);

        model.setOnGameStarted(this::onGameStarted);
        model.setOnUpdate(this::onUpdated);
        model.setOnDeath(this::onDeath);

        resultScreen = new ResultScreen("menu.css", idToColor);
        view.registerOverlay(ResultScreen.NAME, resultScreen);
        model.setOnGameOver(this::showResultScreen);

        model.setOnGameReset(this::showStartScreen);

        // init view and stage
        view.init();
        view.showOverlay(StartScreen.NAME);
        stage.setScene(view.getScene());
    }

    private void initializeIdToColorMap() {
        idToColor = new HashMap<>();
        idToColor.put(0, Color.rgb(117, 170, 248));
        idToColor.put(1, Color.rgb(239, 135, 82));
        idToColor.put(2, Color.rgb(246, 255, 127));
        idToColor.put(3, Color.rgb(131, 240, 113));
        idToColor.put(4, Color.rgb(233, 92, 247));
        idToColor.put(5, Color.rgb(193, 63, 55));
    }

    @Override
    public void setOnGameStarted(Action1<Integer> gameStartedAction) {
        startScreen.setOnStartClicked(gameStartedAction);
    }

    @Override
    public void setOnKeyPressed(Action1<KeyCode> keyPressedAction) {
        this.view.getScene().setOnKeyPressed(event -> keyPressedAction.invoke(event.getCode()));
    }

    @Override
    public void setOnResetClicked(Action resetClickedAction) {
        resultScreen.setOnResetClicked(resetClickedAction);
    }

    // Handler for events from the model
    private void showWaitingScreen(Integer playerCount) {
        // TODO show wanted player count
        view.hideOverlays();
        view.showOverlay(WaitingScreen.NAME);
    }

    private void showCountdownScreen(int countdownNumber) {
        countdownScreen.setCountdownNumber(countdownNumber);
        view.hideOverlays();
        view.showOverlay(CountdownScreen.NAME);
    }

    private void showOwnColor(int id) {
        countdownScreen.showOwnColor(id);
    }

    private void onGameStarted() {
        view.hideOverlays();
    }

    private void onUpdated(Iterable<? extends ILightCycle> lightCycles) {
        view.clear();
        lightCycles.forEach(lc -> {
            Platform.runLater(() -> {
                view.draw(lc.getCoordinates(), idToColor.get(lc.getId()));
            });
        });
    }

    private void onDeath(Coordinate pos) {
        view.highlightCell(pos);
    }

    private void showResultScreen(Iterable<? extends ILightCycle> lightcycles, String message) {
        resultScreen.setResults(lightcycles, message);
        view.showOverlay(ResultScreen.NAME);
    }

    private void showStartScreen() {
        view.hideOverlays();
        view.clear();
        view.showOverlay(StartScreen.NAME);
    }

    @Override
    public void setControlSettings(ArrayList<Pair<Integer, ArrayList<KeyCode>>> controlSettings) {
        countdownScreen.setControlSettings(controlSettings);
    }

    private void showPeerCount(Short count) {
        waitingScreen.setLightCycleCount(count);
    }
}
