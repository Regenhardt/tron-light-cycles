package tron.view;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class WaitingScreen extends VBox {
    public final static String NAME = "waiting";

    private final Label labelWaiting;
    private final Button buttonCancel;

    private final Label lightCycleCount;

    public WaitingScreen(String stylesheet) {
        super(20.0);
        this.getStylesheets().add(stylesheet);
        this.setAlignment(Pos.CENTER);

        labelWaiting = new Label("Waiting for other players...");
        this.getChildren().add(labelWaiting);

        this.lightCycleCount = new Label("1");
        this.lightCycleCount.setStyle("-fx-font-size: 100px;");
        this.getChildren().add(lightCycleCount);

        buttonCancel = new Button("Cancel");
        this.getChildren().add(buttonCancel);
    }

    public void setLightCycleCount(Short count) {
        Platform.runLater(() -> this.lightCycleCount.setText(Integer.toString(count)));
    }
}
