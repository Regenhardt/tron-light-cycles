package tron.view;

import java.util.ArrayList;
import javafx.scene.input.KeyCode;
import javafx.util.Pair;
import tron.util.Action;
import tron.util.Action1;

public interface ITronViewWrapper {
    /**
     * Registers a handler to invoke when the user chooses to start a game.
     * 
     * @param gameStartedAction Action to be invoked.
     */
    public void setOnGameStarted(Action1<Integer> gameStartedAction);

    /**
     * Registers a handler to invoke when a key is pressed on the keyboard.
     * 
     * @param keyPressedAction Action to be invoked.
     */
    public void setOnKeyPressed(Action1<KeyCode> keyPressedAction);

    /**
     * Registers a handler to invoke when the user chooses to reset the game.
     * 
     * @param resetAction Action to be invoked.
     */
    public void setOnResetClicked(Action resetClickedAction);

    /**
     * Sets the control settings for every player to show in the start screen
     * 
     * @param controlSettings
     */
    public void setControlSettings(ArrayList<Pair<Integer, ArrayList<KeyCode>>> controlSettings);
}
