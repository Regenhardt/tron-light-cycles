package tron.model;

import java.util.List;

import edu.cads.bai5.vsp.tron.view.Coordinate;

public interface ILightCycle {
    public int getId();

    public List<Coordinate> getCoordinates();

    public int getRank();
}
