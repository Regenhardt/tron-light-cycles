package tron.model.states;

import java.util.stream.StreamSupport;

import tron.model.TronCommand;
import tron.model.TronModel;

public class WaitingState extends State {
    private Thread loadingThread;

    public WaitingState(TronModel model) {
        super(model);
        createThread();
    }

    private void createThread() {
        loadingThread = new Thread(() -> {
            this.model.onWaiting();
            if (this.model.getGrid().getLightCycles() == null
                    || StreamSupport.stream(this.model.getGrid().getLightCycles().spliterator(), false).count() == 0)
                // no one set the lightcycles for us
                this.model.initLightCycles();
            this.model.changeState(new CountdownState(this.model));
        });
    }

    @Override
    public void activate() {
        loadingThread.start();
    }

    @Override
    public void execute(TronCommand cmd, Object param) {
        // Nothing to do in this state
    }

}
