package tron.model.states;

import tron.model.TronCommand;
import tron.model.TronModel;

public abstract class State {
    protected final TronModel model;
    public State(TronModel model){
        this.model = model;
    }

    public abstract void activate();

    public abstract void execute(TronCommand cmd, Object param);
}
