package tron.model.states;

import tron.model.TronCommand;
import tron.model.TronModel;

public class GameOverState extends State {

    public GameOverState(TronModel model) {
        super(model);
    }

    @Override
    public void activate() {
        model.onGameOver();
    }

    @Override
    public void execute(TronCommand cmd, Object param) {
        if (cmd == TronCommand.RESET && param == null) {
            model.changeState(new StartState(model));
        }
    }
}
