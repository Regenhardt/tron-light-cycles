package tron.model.states;

import tron.model.TronCommand;
import tron.model.TronModel;

public class CountdownState extends State {
    private Thread countdownThread;

    public CountdownState(TronModel model) {
        super(model);
        createThread();
    }

    private void createThread() {
        countdownThread = new Thread(() -> {
            for (int i = 3; i > 0; i--) {
                this.model.onCountdown(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.model.changeState(new GameState(this.model));
        });
    }

    @Override
    public void activate() {
        countdownThread.start();
    }

    @Override
    public void execute(TronCommand cmd, Object param) {
        // Nothing to do in this state
    }

}
