package tron.model.states;

import javafx.scene.paint.Color;
import tron.model.TronCommand;
import tron.model.TronModel;
import tron.model.Turn;

public class GameState extends State {
    private Thread gameThread;

    public GameState(TronModel model) {
        super(model);
        createThread();
    }

    private void createThread() {
        gameThread = new Thread(() -> {
            long timeToTick, startTime = System.currentTimeMillis();
            do {
                timeToTick = System.currentTimeMillis() - startTime;
                try {
                    var timeToSleep = model.tickPeriod - timeToTick;
                    if(timeToSleep > 0)
                        Thread.sleep(timeToSleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startTime = System.currentTimeMillis();
            } while (model.update() > 1);
            this.model.changeState(new GameOverState(model));
        });
    }

    @Override
    public void activate() {
        this.model.onGameStarted();
        gameThread.start();
    }

    @Override
    public void execute(TronCommand cmd, Object id) {
        switch (cmd) {
            case LEFT -> this.model.getGrid().setLightCycleTurn(Turn.LEFT, (int) id);
            case RIGHT -> this.model.getGrid().setLightCycleTurn(Turn.RIGHT, (int) id);
            default -> {
            }
        }
    }

}
