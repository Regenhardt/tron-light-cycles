package tron.model.states;

import tron.model.TronCommand;
import tron.model.TronModel;

public class StartState extends State {
    public StartState(TronModel model) {
        super(model);
    }

    @Override
    public void activate() {
        this.model.onReset();
    }

    @Override
    public void execute(TronCommand cmd, Object param) {
        if (cmd == TronCommand.START && param instanceof Integer) {
            int playerCount = (Integer) param;
            if (playerCount >= 2 && playerCount <= 6) {
                this.model.setPlayerCount(playerCount);
                this.model.changeState(new WaitingState(this.model));
            }
        }
    }
}
