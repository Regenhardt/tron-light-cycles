package tron.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import javafx.util.Pair;
import middleware.IMiddleware;
import middleware.PeerId;
import tron.util.Action;
import tron.util.Action1;
import tron.util.Action2;

public class RemoteModel extends TronModelDecorator {
    private IMiddleware middleware;
    private PeerId peerId;

    int rows;
    int columns;
    double tickrate;
    private IMiddleware hostingMiddleware;
    private PeerId hostId = new PeerId((short) 0);

    private Action1<Integer> onWaitingListener;
    private Action1<Integer> onCountdownListener;
    private Action1<Integer> onAssignedIdListener;
    private Action onGameStartedListener;
    private Action1<Iterable<? extends ILightCycle>> onUpdateListener;
    private Action1<Coordinate> onDeathListener;
    private Action2<Iterable<? extends ILightCycle>, String> onGameOverListener;
    private Action onResetListener;
    private Consumer<Short> onPeerCountListener;
    private int lightCycleId;

    private int playerCount;
    private List<LightCycle> lightCycles;

    public RemoteModel(ITronModel model) {
        super(model);
        super.setOnWaiting(i -> {
            this.onWaitingListener.invoke(i);
        });
        try {
            middleware = IMiddleware.create();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void startGame(int playerCount) {
        this.playerCount = playerCount;
        if (onWaitingListener != null) {
            onWaitingListener.invoke(playerCount);
        }
        try {
            middleware.setOnPeerCount(onPeerCountListener);
            middleware.initPeerNetwork(playerCount, this::configureMiddleware, this::createGame, this::startRemoteGame);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createGame(IMiddleware middleware) { // only as host
        hostingMiddleware = middleware;
        hostingMiddleware.setOnCall(this::callHost);
        super.setOnCountdown((i) -> {
            hostingMiddleware.publishReliable(Protocol.COUNTDOWN, i);
            if (latestCoordinates != null && latestCoordinates.length > 0)
                hostingMiddleware.publishQuick(Protocol.UPDATE, latestCoordinates);
        });
        super.setOnGameStarted(() -> hostingMiddleware.publishReliable(Protocol.GAME_STARTED, new Object[0]));
        super.setOnUpdate(this::sendLastCoordinates);
        super.setOnDeath((coordinate) -> hostingMiddleware.publishQuick(Protocol.DEATH, coordinate.x, coordinate.y));
        super.setOnGameOver(this::sendGameOverInfo);
        super.setOnIdAssigned(this::onIdAssigned);

    }

    private void startRemoteGame() {
        super.startGame(playerCount);
    }

    private void configureMiddleware(IMiddleware middleware, PeerId id) { // all peers
        this.middleware = middleware;
        this.middleware.setOnCall(this::callPeer);
        peerId = id;
        lightCycleId = id.ID;
        onAssignedIdListener.invoke(peerId.ID);
    }

    @Override
    public void setLightCycleTurn(Turn turn, int _id) {
        middleware.invokeQuick(hostId, Protocol.SET_LIGHTCYCLE_TURN, turn.ordinal(), lightCycleId);
    }

    @Override
    public void resetGame() {
        try {
            // TODO wird noch referenziert?
            middleware = IMiddleware.create();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.resetGame();
        hostingMiddleware = null;
        lightCycles = null;
        if (onResetListener != null)
            onResetListener.invoke();
    }

    @Override
    public void setOnWaiting(Action1<Integer> listener) {
        this.onWaitingListener = listener;
    }

    @Override
    public void setOnCountdown(Action1<Integer> listener) {
        onCountdownListener = listener;
    }

    @Override
    public void setOnGameStarted(Action listener) {
        onGameStartedListener = listener;
    }

    @Override
    public void setOnUpdate(Action1<Iterable<? extends ILightCycle>> listener) {
        onUpdateListener = listener;
    }

    @Override
    public void setOnDeath(Action1<Coordinate> listener) {
        onDeathListener = listener;
    }

    @Override
    public void setOnGameOver(Action2<Iterable<? extends ILightCycle>, String> listener) {
        onGameOverListener = listener;
    }

    @Override
    public void setOnGameReset(Action listener) {
        onResetListener = listener;
    }

    private void callPeer(PeerId sender, Pair<Byte, Object[]> data) {
        switch ((byte) data.getKey()) {
            case Protocol.COUNTDOWN:
                if (onCountdownListener != null)
                    onCountdownListener.invoke((int) data.getValue()[0]);
                break;
            case Protocol.GAME_STARTED:
                if (onGameStartedListener != null)
                    onGameStartedListener.invoke();
                break;
            case Protocol.UPDATE:
                addCoordinatesToLightCycles((int[]) data.getValue()[0]);
                if (onUpdateListener != null)
                    onUpdateListener.invoke(lightCycles.stream().filter(lc -> lc.isAlive).toList());
                break;
            case Protocol.DEATH:
                if (onDeathListener != null)
                    onDeathListener.invoke(new Coordinate((int) data.getValue()[0], (int) data.getValue()[1]));
                break;
            case Protocol.GAME_OVER:
                updateLightCyclesOnGameOver(Arrays.copyOfRange(data.getValue(), 0, data.getValue().length - 1));
                if (onGameOverListener != null)
                    onGameOverListener.invoke(
                            lightCycles.stream().sorted(Comparator.comparing(LightCycle::getRank)).toList(),
                            (String) data.getValue()[data.getValue().length - 1]);
                break;
            case Protocol.ID_OFFSET:
                var offset = (int) data.getValue()[0];
                lightCycleId = peerId.ID + offset;
                onAssignedIdListener.invoke(lightCycleId);
                break;
        }
    }

    private void callHost(PeerId sender, Pair<Byte, Object[]> data) {
        switch (data.getKey()) {
            case Protocol.SET_LIGHTCYCLE_TURN:
                super.setLightCycleTurn(Turn.values()[(int) data.getValue()[0]], (int) data.getValue()[1]);
                break;
        }
    }

    int[] latestCoordinates; // cached

    private void sendLastCoordinates(Iterable<? extends ILightCycle> lightCycles) {
        // opt TODO: send last 3 coordinates
        var lcList = (List<ILightCycle>) lightCycles;
        var numberOfLightCycles = lcList.size();
        latestCoordinates = new int[numberOfLightCycles * 3];
        for (int i = 0; i < numberOfLightCycles; i++) {
            var lc = lcList.get(i);
            int id = lc.getId();
            var lastIndex = lc.getCoordinates().size() - 1;
            latestCoordinates[i * 3] = id;
            latestCoordinates[i * 3 + 1] = (lc.getCoordinates()).get(lastIndex).x;
            latestCoordinates[i * 3 + 2] = (lc.getCoordinates()).get(lastIndex).y;
        }
        hostingMiddleware.publishQuick(Protocol.UPDATE, latestCoordinates);
    }

    private void addCoordinatesToLightCycles(int[] idAndCoordinates) {
        // if not set get playerCount with first update
        if (this.lightCycles == null) {
            playerCount = idAndCoordinates.length / 3;
            this.lightCycles = new ArrayList<>(playerCount);
            for (int i = 0; i < playerCount; i++) {
                lightCycles.add(new LightCycle(i));
            }
            this.lightCycles.sort(Comparator.comparingInt(ILightCycle::getId));
        }

        // opt TODO: check if received coordinates continue existing ones
        // do something if not
        boolean[] alive = new boolean[playerCount];
        for (var i = 0; i < idAndCoordinates.length; i += 3) {
            lightCycles.get(idAndCoordinates[i]).coordinates
                    .add(new Coordinate((int) idAndCoordinates[i + 1], (int) idAndCoordinates[i + 2]));
            alive[idAndCoordinates[i]] = true;
        }
        // kill died players
        for (int i = 0; i < playerCount; i++) {
            if (!alive[i]) {
                lightCycles.get(i).isAlive = false;
                System.out.println(i + " crashed");
            }
        }
    }

    private void updateLightCyclesOnDeath(Object[] args) {
        HashSet<Integer> aliveIds = new HashSet<>();
        for (int i = 0; i < args.length; i++) {
            aliveIds.add((int) args[i]);
        }
        for (LightCycle lightCycle : lightCycles) {
            if (!aliveIds.contains(lightCycle.getId()))
                lightCycle.isAlive = false;
        }
    }

    private void sendGameOverInfo(Iterable<? extends ILightCycle> lightCycles, String message) {
        var args = new Object[playerCount + 1];
        args[args.length - 1] = message;
        for (ILightCycle lightcycle : lightCycles) {
            args[lightcycle.getId()] = lightcycle.getRank();
        }
        hostingMiddleware.publishReliable(Protocol.GAME_OVER, args);
    }

    private void updateLightCyclesOnGameOver(Object[] args) {
        for (int i = 0; i < args.length; i++) {
            lightCycles.get(i).rank = (int) args[i];
        }
    }

    @Override
    public void setOnPeerCountUpdate(Consumer<Short> listener) {
        onPeerCountListener = listener;
    }

    @Override
    public void setOnIdAssigned(Action1<Integer> listener) {
        // if no one under me does it i do it
        onAssignedIdListener = listener;
        // otherwise they can also do it
        super.setOnIdAssigned(listener);
    }

    private void onIdAssigned(Integer idOffset) {
        // if method got called, have to send the possible offset to all peers
        if (idOffset > 0) {
            hostingMiddleware.publishReliable(Protocol.ID_OFFSET, idOffset);
        }
    }

}
