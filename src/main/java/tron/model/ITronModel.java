package tron.model;

import java.io.IOException;
import java.util.function.Consumer;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import tron.util.Action;
import tron.util.Action1;
import tron.util.Action2;

public interface ITronModel {

    public static ITronModel buildModel(GameMode mode, int rows, int columns, double tickrate) throws IOException {
        ITronModel tronModel;
        switch (mode) {
            case LOCAL:
                tronModel = new TronModel(new Grid(rows, columns), tickrate);
                break;
            case REMOTE:
                tronModel = new RemoteModel(
                        new TronModel(new Grid(rows, columns), tickrate));
                break;
            case REST:
                tronModel = new RemoteModel(
                        new RestModel(
                                new TronModel(new Grid(rows, columns), tickrate)));
                break;
            default:
                throw new IllegalArgumentException("Unknown game mode: " + mode);
        }
        return tronModel;
    }

    public IGrid getGrid();

    /**
     * Sets the given turn for the lightcycle with the given id.
     * 
     * @param turn Turn to set.
     * @param id   id of lightclycle to set turn for.
     */
    public void setLightCycleTurn(Turn turn, int id);

    /**
     * Starts the game with the given number of players.
     * 
     * @param playerCount Number of players.
     */
    public void startGame(int playerCount);

    /**
     * Resets the game, removes all players.
     */
    public void resetGame();

    /**
     * Registers a listener for when the game is loading;
     */
    public void setOnWaiting(Action1<Integer> listener);

    /**
     * Registers a listener for the countdown before the game starts.
     * 
     * @param listener
     */
    public void setOnCountdown(Action1<Integer> listener);

    /**
     * Registers a listener for when the game starts.
     * 
     * @param listener
     */
    public void setOnGameStarted(Action listener);

    /**
     * Registers a listener for when the game updates current light cycle positions.
     * 
     * @param listener
     */
    public void setOnUpdate(Action1<Iterable<? extends ILightCycle>> listener);

    /**
     * Registers a listener for when a light cycle crashed.
     * 
     * @param onDeathAction
     */
    public void setOnDeath(Action1<Coordinate> onDeathAction);

    /**
     * Registers a listener for when the game over condition is met
     * 
     * @param onGameOverAction all bikes and message to show at end of game
     */
    public void setOnGameOver(Action2<Iterable<? extends ILightCycle>, String> onGameOverAction);

    /**
     * Registers a listener for when the game resets.
     * 
     * @param listener
     */
    public void setOnGameReset(Action listener);

    public void setOnPeerCountUpdate(Consumer<Short> listener);

    public void setOnIdAssigned(Action1<Integer> listener);
}