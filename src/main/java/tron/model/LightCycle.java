package tron.model;

import java.util.ArrayList;
import java.util.List;

import edu.cads.bai5.vsp.tron.view.Coordinate;

class LightCycle implements ILightCycle {
    final int id;
    List<Coordinate> coordinates = new ArrayList<Coordinate>(256);
    boolean isAlive = true;
    int rank = 10;

    Direction direction = Direction.EAST;
    Turn turn = Turn.STRAIGHT;

    /**
     * Initializes LightCycle without coordinates.
     * 
     * @param id
     */
    public LightCycle(int id) {
        this.id = id;
    }

    public LightCycle(LightCycle that) {
        this.id = that.id;
        this.isAlive = that.isAlive;
        this.rank = that.rank;
    }

    public int getId() {
        return id;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public int getRank() {
        return rank;
    }

    /**
     * Updates direction if a turn is set and moves the lightcycle
     */
    protected void move() {
        if (turn != Turn.STRAIGHT) {
            direction = direction.turn(turn);
            turn = Turn.STRAIGHT;
        }
        coordinates.add(direction.applyTo(coordinates.get(coordinates.size() - 1)));
    }
}
