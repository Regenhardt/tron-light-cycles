package tron.model;

import java.util.List;
import java.util.Set;

import edu.cads.bai5.vsp.tron.view.Coordinate;

public interface IGrid {

    /**
     * Sets the indicated cycle's turn to the given turn value.
     * 
     * @param turn Direction the light cycle should turn.
     * @param id
     */
    public void setLightCycleTurn(Turn turn, int id);

    /**
     * Sets the light cycles in the grid.
     * 
     * @param lightCycles
     */
    public void setLightCycles(List<LightCycle> lightCycles);

    /**
     * Returns the colors of the newly crashed ligth cycles.
     * 
     * @return
     */
    public Set<Coordinate> getCrashed();

    /**
     * Returns a copy of all the light cycles in the grid.
     * 
     * @return
     */
    public List<LightCycle> getResults();

    /**
     * Sets the existing light cycles to initial positions depending on how many
     * there are and how big the grid is.
     */
    public void initStartingPositions();

    public Iterable<LightCycle> getLightCycles();

    /**
     * Berechnet und setzt die neuen Koordinaten der LightCycles anhand gesetzter
     * Turn-Werte und Directions.
     */
    public void update();

    /**
     * Get the light cycles still alive
     * 
     * @return
     */
    public List<? extends ILightCycle> getAlive();

    /**
     * resets the LightCycles and their rankings
     * 
     * @return
     */
    public void resetLightCycles();
}
