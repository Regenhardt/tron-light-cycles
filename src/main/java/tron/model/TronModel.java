package tron.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import tron.model.states.StartState;
import tron.model.states.State;
import tron.util.Action;
import tron.util.Action1;
import tron.util.Action2;

public class TronModel implements ITronModel {
    private int playerCount = 2;
    private State state;
    private final IGrid grid;
    private Action1<Integer> onWaitingListener;
    private Action1<Integer> onCountdownListener;
    private Action onGameStartedListener;
    private Action1<Integer> onAssignedIdListener;
    private Action onResetListener;
    private Action1<Iterable<? extends ILightCycle>> onUpdateListener;
    private Action1<Coordinate> onDeathListener;
    private Action2<Iterable<? extends ILightCycle>, String> onGameOverListner;
    private Consumer<Short> onPeerCountListener;
    public final double tickrate;
    public final long tickPeriod;

    public TronModel(IGrid grid, double tickrate) {
        this.grid = grid;
        this.tickrate = tickrate;
        this.tickPeriod = (long) (1000 / tickrate);
        changeState(new StartState(this));
    }

    @Override
    public IGrid getGrid() {
        return grid;
    }

    @Override
    public void setLightCycleTurn(Turn turn, int id) {
        this.grid.setLightCycleTurn(turn, id);
    }

    @Override
    public void startGame(int playerCount) {
        this.state.execute(TronCommand.START, playerCount);
    }

    @Override
    public void resetGame() {
        this.resetPlayers();
        this.state.execute(TronCommand.RESET, null);
    }

    @Override
    public void setOnWaiting(Action1<Integer> listener) {
        this.onWaitingListener = listener;
    }

    @Override
    public void setOnCountdown(Action1<Integer> listener) {
        this.onCountdownListener = listener;
    }

    @Override
    public void setOnGameStarted(Action listener) {
        this.onGameStartedListener = listener;
    }

    @Override
    public void setOnUpdate(Action1<Iterable<? extends ILightCycle>> onUpdateAction) {
        this.onUpdateListener = onUpdateAction;
    }

    @Override
    public void setOnDeath(Action1<Coordinate> onDeathAction) {
        this.onDeathListener = onDeathAction;
    }

    @Override
    public void setOnGameOver(Action2<Iterable<? extends ILightCycle>, String> onGameOverAction) {
        this.onGameOverListner = onGameOverAction;
    }

    @Override
    public void setOnGameReset(Action listener) {
        this.onResetListener = listener;
    }

    /**
     * Triggers the onWaiting event
     */
    public void onWaiting() {
        if (this.onWaitingListener != null) {
            this.onWaitingListener.invoke(playerCount);
        }
    }

    public void onAssignedId(Integer id) {
        if (this.onAssignedIdListener != null) {
            this.onAssignedIdListener.invoke(id);
        }
    }

    /**
     * Triggers the countdown event.
     * 
     * @param countdownNumber
     */
    public void onCountdown(Integer countdownNumber) {
        if (this.onCountdownListener != null) {
            this.onCountdownListener.invoke(countdownNumber);
        }
    }

    /**
     * Triggers the gameStarted event
     */
    public void onGameStarted() {
        if (this.onGameStartedListener != null) {
            this.onGameStartedListener.invoke();
        }
    }

    /**
     * Triggers the update event
     */
    private void onUpdate(List<? extends ILightCycle> lightCycles) {
        if (this.onUpdateListener != null) {
            this.onUpdateListener.invoke(lightCycles);
        }
    }

    /**
     * Triggers the death event
     */
    private void onDeath(Coordinate c) {
        if (this.onDeathListener != null) {
            this.onDeathListener.invoke(c);
        }
    }

    /**
     * Triggers the GameOver event
     */
    public void onGameOver() {
        if (this.onGameOverListner != null) {
            this.onGameOverListner.invoke(grid.getResults(), this.getGameOverMessage());
        }
    }

    /**
     * Triggers the reset event
     */
    public void onReset() {
        if (this.onResetListener != null) {
            this.onResetListener.invoke();
        }
    }

    public void changeState(State state) {
        this.state = state;
        this.state.activate();
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public void initLightCycles() {
        // Create the light cycles
        List<LightCycle> cycles = new ArrayList<>();
        for (int i = 0; i < playerCount; i++) {
            cycles.add(new LightCycle(i));
        }
        // Place players on the grid
        this.grid.setLightCycles(cycles);
        this.grid.initStartingPositions();
        this.onUpdate(cycles);
    }

    /**
     * Moves light cycles and checks for collisions.
     */
    public int update() {
        this.grid.update();
        Set<Coordinate> crashed = this.grid.getCrashed();
        List<? extends ILightCycle> alive = this.grid.getAlive();
        this.onUpdate(alive);
        for (Coordinate c : crashed) {
            this.onDeath(c);
        }
        return alive.size();
    }

    private String getGameOverMessage() {
        var results = grid.getResults();
        if (results.stream().noneMatch(lc -> lc.getRank() == 1))
            return "No one wins...";

        var winner = results.stream()
                .filter(lc -> lc.getRank() == 1)
                .findFirst().orElse(null);

        return (winner == null)
                ? "No one wins...also there's something wrong with the code."
                : "Player " + winner.getId() + " wins!";
    }

    private void resetPlayers() {
        playerCount = 2;
        grid.resetLightCycles();
    }

    @Override
    public void setOnPeerCountUpdate(Consumer<Short> listener) {
        onPeerCountListener = listener;
    }

    @Override
    public void setOnIdAssigned(Action1<Integer> listener) {
        onAssignedIdListener = listener;
    }

}
