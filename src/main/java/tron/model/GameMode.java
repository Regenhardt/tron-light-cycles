package tron.model;

public enum GameMode {
    LOCAL,
    REMOTE,
    REST
}
