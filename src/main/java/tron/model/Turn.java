package tron.model;

public enum Turn {
    LEFT, RIGHT, STRAIGHT
}