package tron.model.http;

import java.io.IOException;
import java.net.InetAddress;
import java.util.function.Function;

import org.glassfish.grizzly.http.Method;
import org.glassfish.grizzly.http.server.*;
import org.glassfish.grizzly.http.util.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.util.Pair;

public class RestServer {
    private HttpServer httpServer;
    private ObjectMapper mapper = new ObjectMapper();
    private int port;

    public RestServer() {
        port = NetworkListener.DEFAULT_NETWORK_PORT;
        httpServer = HttpServer.createSimpleServer();
    }

    public <T> void on(Method method, String route, Class<T> type, Function<Pair<InetAddress, T>, HttpStatus> handler) {
        if (!route.startsWith("/"))
            route = "/" + route;

        this.httpServer.getServerConfiguration().addHttpHandler(new HttpHandler() {
            @Override
            public void service(Request request, Response response) throws Exception {
                var sender = request.getRemoteAddr();
                var requestMethod = request.getMethod();
                System.out.println(requestMethod + " " + request.getRequestURI() + " from " + sender);
                if (method.equals(requestMethod)) {
                    var payload = readPayload(request);
                    var res = mapper.readValue(payload, type);
                    var responseCode = handler.apply(new Pair<>(InetAddress.getByName(sender), res));
                    response.setStatus(responseCode);
                } else {
                    response.setStatus(HttpStatus.METHOD_NOT_ALLOWED_405);
                }
            }
        }, route);
    }

    /**
     * Starts the server if it is not already running.
     * @throws IOException
     */
    public void startServer() throws IOException {
        if(!this.httpServer.isStarted())
            this.httpServer.start();
    }

    private String readPayload(Request req) throws IOException {
        return req.getPostBody(Integer.parseInt(req.getHeader("Content-length"))).toStringContent();
    }

    public int getPort(){
        return this.port;
    }
}
