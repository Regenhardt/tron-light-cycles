package tron.model;

import java.util.Map;

import edu.cads.bai5.vsp.tron.view.Coordinate;

public enum Direction {
    NORTH, SOUTH, WEST, EAST;

    private static final Map<Direction, Map<Turn, Direction>> TURNS = Map.of(
            NORTH, Map.of(Turn.LEFT, WEST, Turn.RIGHT, EAST),
            SOUTH, Map.of(Turn.LEFT, EAST, Turn.RIGHT, WEST),
            WEST, Map.of(Turn.LEFT, SOUTH, Turn.RIGHT, NORTH),
            EAST, Map.of(Turn.LEFT, NORTH, Turn.RIGHT, SOUTH)
    );

    /**
     * Returns the new direction after turning
     */
    public Direction turn(Turn turn) {
        return TURNS.get(this).get(turn);
    }

    /**
     * Calculates the new coordinate depending on the direction
     * @param coordinate The place to move from
     * @return A coordinate object indicating the new position
     */
    public Coordinate applyTo(Coordinate coordinate) {
        return switch (this) {
            case NORTH -> new Coordinate(coordinate.x, coordinate.y - 1);
            case SOUTH -> new Coordinate(coordinate.x, coordinate.y + 1);
            case WEST -> new Coordinate(coordinate.x - 1, coordinate.y);
            case EAST -> new Coordinate(coordinate.x + 1, coordinate.y);
        };
    }
}