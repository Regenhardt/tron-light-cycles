package tron.model;

import edu.cads.bai5.vsp.tron.view.Coordinate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class Grid implements IGrid {
    private final int rows;
    private final int columns;
    private List<LightCycle> lightCycles = new ArrayList<LightCycle>();
    private Queue<Set<LightCycle>> ranking = new LinkedList<>();

    public Grid(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    @Override
    public void setLightCycleTurn(Turn turn, int id) {
        LightCycle c = lightCycles.stream().filter(lc -> lc.id == id).findFirst().get();
        c.turn = turn;
    }

    @Override
    public void update() {
        lightCycles.stream().filter(lc -> lc.isAlive).forEach(LightCycle::move);
    }

    @Override
    public Set<Coordinate> getCrashed() {
        return checkCollisions();
    }

    private Set<Coordinate> checkCollisions() {
        Set<Coordinate> crashed = new HashSet<>();
        Set<LightCycle> crashedCycles = new HashSet<>();
        List<LightCycle> aliveLightCycles = lightCycles.stream().filter(lc -> lc.isAlive).toList();

        for (int i = 0; i < aliveLightCycles.size(); i++) {
            LightCycle current = aliveLightCycles.get(i);
            Coordinate latestPos = current.coordinates.get(current.coordinates.size() - 1);
            if (current.isAlive) {
                // Check if current ran into its own ribbon
                for (int j = 0; j < current.coordinates.size() - 1; j++) {
                    if (current.coordinates.get(j).equals(latestPos)) {
                        crashed.add(latestPos);
                        crashedCycles.add(new LightCycle(current));
                        current.isAlive = false;
                        continue;
                    }
                }

                // Check if current ran into the wall
                if (!isInsideGrid(latestPos)) {
                    // take last seen position
                    crashed.add(current.coordinates.get(current.coordinates.size() - 2));
                    crashedCycles.add(new LightCycle(current));
                    current.isAlive = false;
                    continue;
                }

                // Check if current crashed into other alive lightcycles
                for (int j = 0; j < aliveLightCycles.size(); j++) {
                    if (j == i)
                        continue;
                    LightCycle other = aliveLightCycles.get(j);
                    if (other.coordinates.contains(latestPos)) {
                        crashed.add(latestPos);
                        crashedCycles.add(new LightCycle(current));
                        current.isAlive = false;
                    }
                }
            }
        }
        if (crashedCycles.size() > 0) {
            ranking.add(crashedCycles);
        }
        return crashed;
    }

    private Boolean isInsideGrid(Coordinate c) {
        return c.x >= 0 && c.x < columns && c.y >= 0 && c.y < rows;
    }

    @Override
    public Iterable<LightCycle> getLightCycles() {
        return lightCycles;
    }

    @Override
    public void setLightCycles(List<LightCycle> lightCycles) {
        this.lightCycles = lightCycles;
        this.ranking = new LinkedList<>();
    }

    @Override
    public List<LightCycle> getAlive() {
        return lightCycles.stream().filter(lc -> lc.isAlive).collect(Collectors.toList());
    }

    @Override
    public List<LightCycle> getResults() {
        List<LightCycle> lightCyclesClone = new ArrayList<LightCycle>();
        int currentRank = this.lightCycles.size();
        for (Set<LightCycle> currentRanked : ranking) {
            for (LightCycle currentCycle : currentRanked) {
                currentCycle.rank = currentRank;
                lightCyclesClone.add(new LightCycle(currentCycle));
            }
            currentRank -= currentRanked.size();
        }
        for (LightCycle winner : getAlive()) {
            winner.rank = 1;
            lightCyclesClone.add(new LightCycle(winner));
        }
        lightCyclesClone.sort(Comparator.comparing(LightCycle::getRank));
        return lightCyclesClone;
    }

    @Override
    public void initStartingPositions() {
        switch (this.lightCycles.size()) {
            case 2 -> initToTwoPositions();
            case 3 -> initToThreePositions();
            case 4 -> initToFourPositions();
            case 5 -> initToFivePositions();
            case 6 -> initToSixPositions();
            default -> throw new UnsupportedOperationException(
                    this.lightCycles.size() + " cycles in one game not implemented");
        }
    }

    private void initToTwoPositions() {
        if (this.lightCycles.size() != 2) {
            throw new IllegalArgumentException("There must be exactly two light cycles.");
        }

        int distanceFromSide = this.columns / 10;
        int distanceFromTop = this.rows / 2;

        this.lightCycles.get(0).coordinates.clear();
        this.lightCycles.get(0).coordinates.add(new Coordinate(distanceFromSide, distanceFromTop));
        this.lightCycles.get(0).direction = Direction.EAST;

        this.lightCycles.get(1).coordinates.clear();
        this.lightCycles.get(1).coordinates.add(new Coordinate(this.columns - 1 - distanceFromSide, distanceFromTop));
        this.lightCycles.get(1).direction = Direction.WEST;
    }

    private void initToThreePositions() {
        if (this.lightCycles.size() != 3) {
            throw new IllegalArgumentException("There must be exactly three light cycles.");
        }

        int distanceFromSide = this.columns / 10;
        int distanceFromBottom = this.rows / 10;
        int distanceFromTop = this.rows / 3;
        int halfX = this.columns / 2;

        this.lightCycles.get(0).coordinates.clear();
        this.lightCycles.get(0).coordinates.add(new Coordinate(distanceFromSide, distanceFromTop));
        this.lightCycles.get(0).direction = Direction.EAST;

        this.lightCycles.get(1).coordinates.clear();
        this.lightCycles.get(1).coordinates.add(new Coordinate(this.columns - 1 - distanceFromSide, distanceFromTop));
        this.lightCycles.get(1).direction = Direction.WEST;

        this.lightCycles.get(2).coordinates.clear();
        this.lightCycles.get(2).coordinates.add(new Coordinate(halfX, this.rows - 1 - distanceFromBottom));
        this.lightCycles.get(2).direction = Direction.NORTH;
    }

    private void initToFourPositions() {
        if (this.lightCycles.size() != 4) {
            throw new IllegalArgumentException("There must be exactly four light cycles.");
        }
        int distanceFromX = this.columns / 10;
        int distanceFromY = this.rows / 5;

        this.lightCycles.get(0).coordinates.clear();
        this.lightCycles.get(0).coordinates.add(new Coordinate(distanceFromX, distanceFromY));
        this.lightCycles.get(0).direction = Direction.EAST;

        this.lightCycles.get(1).coordinates.clear();
        this.lightCycles.get(1).coordinates.add(new Coordinate(this.columns - 1 - distanceFromX, distanceFromY));
        this.lightCycles.get(1).direction = Direction.WEST;

        // dont crash if not turned!
        this.lightCycles.get(2).coordinates.clear();
        this.lightCycles.get(2).coordinates
                .add(new Coordinate(this.columns - 1 - distanceFromX, this.rows - 1 - distanceFromY));
        this.lightCycles.get(2).direction = Direction.WEST;

        this.lightCycles.get(3).coordinates.clear();
        this.lightCycles.get(3).coordinates
                .add(new Coordinate(distanceFromX, this.rows - 1 - distanceFromY));
        this.lightCycles.get(3).direction = Direction.EAST;
    }

    private void initToFivePositions() {
        if (this.lightCycles.size() != 5) {
            throw new IllegalArgumentException("There must be exactly five light cycles.");
        }

        int smallDistanceFromSide = this.columns / 10;
        int smallDistanceFromTop = this.rows / 10;
        int thirdY = this.rows / 3;
        int halfX = this.columns / 2;

        this.lightCycles.get(0).coordinates.clear();
        this.lightCycles.get(0).coordinates.add(new Coordinate(smallDistanceFromSide, thirdY));
        this.lightCycles.get(0).direction = Direction.EAST;

        this.lightCycles.get(1).coordinates.clear();
        this.lightCycles.get(1).coordinates.add(new Coordinate(halfX, smallDistanceFromTop));
        this.lightCycles.get(1).direction = Direction.SOUTH;

        this.lightCycles.get(2).coordinates.clear();
        this.lightCycles.get(2).coordinates.add(new Coordinate(this.columns - 1 - smallDistanceFromSide, thirdY));
        this.lightCycles.get(2).direction = Direction.WEST;

        this.lightCycles.get(3).coordinates.clear();
        this.lightCycles.get(3).coordinates
                .add(new Coordinate(this.columns - smallDistanceFromSide, this.rows - smallDistanceFromTop));
        this.lightCycles.get(3).direction = Direction.WEST;

        this.lightCycles.get(4).coordinates.clear();
        this.lightCycles.get(4).coordinates
                .add(new Coordinate(smallDistanceFromSide, this.rows - smallDistanceFromTop));
        this.lightCycles.get(4).direction = Direction.EAST;
    }

    private void initToSixPositions() {
        if (this.lightCycles.size() != 6) {
            throw new IllegalArgumentException("There must be exactly six light cycles.");
        }

        int smallDistanceFromSide = this.columns / 10;
        int smallDistanceFromTop = this.rows / 10;
        int halfY = this.rows / 2;

        this.lightCycles.get(0).coordinates.clear();
        this.lightCycles.get(0).coordinates.add(new Coordinate(smallDistanceFromSide, smallDistanceFromTop));
        this.lightCycles.get(0).direction = Direction.EAST;

        this.lightCycles.get(1).coordinates.clear();
        this.lightCycles.get(1).coordinates
                .add(new Coordinate(this.columns - 1 - smallDistanceFromSide, smallDistanceFromTop));
        this.lightCycles.get(1).direction = Direction.WEST;

        this.lightCycles.get(2).coordinates.clear();
        this.lightCycles.get(2).coordinates.add(new Coordinate(this.columns - 1 - smallDistanceFromSide, halfY));
        this.lightCycles.get(2).direction = Direction.WEST;

        this.lightCycles.get(3).coordinates.clear();
        this.lightCycles.get(3).coordinates
                .add(new Coordinate(this.columns - 1 - smallDistanceFromSide, this.rows - 1 - smallDistanceFromTop));
        this.lightCycles.get(3).direction = Direction.WEST;

        this.lightCycles.get(4).coordinates.clear();
        this.lightCycles.get(4).coordinates
                .add(new Coordinate(smallDistanceFromSide, this.rows - 1 - smallDistanceFromTop));
        this.lightCycles.get(4).direction = Direction.EAST;

        this.lightCycles.get(5).coordinates.clear();
        this.lightCycles.get(5).coordinates
                .add(new Coordinate(smallDistanceFromSide, halfY));
        this.lightCycles.get(5).direction = Direction.EAST;
    }

    @Override
    public void resetLightCycles() {
        lightCycles = new ArrayList<LightCycle>();
        ranking = new LinkedList<>();
    }
}
