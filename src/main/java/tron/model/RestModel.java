package tron.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.http.client.utils.URIBuilder;
import org.glassfish.grizzly.http.Method;
import org.glassfish.grizzly.http.util.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.cads.bai5.vsp.tron.view.Coordinate;
import javafx.util.Pair;
import tron.model.http.RestServer;
import tron.util.Action1;
import tron.util.Action2;

public class RestModel extends TronModelDecorator {
    private static final String PROPS_FILE = "rest.properties";

    // Setup
    private final String nameServiceUri;
    private int localPlayerCount;
    private String coordinatorAddress;
    private InetAddress ownIpAddress;
    private URI ownUri;
    private RestServer httpServer = new RestServer();
    private HashMap<InetSocketAddress, Integer> playersPerSupernode = new HashMap<>();
    private HashMap<InetSocketAddress, InitialLightCycle[]> initialLightCycles = new HashMap<>();
    private boolean gameRunning;
    private Action1<Integer> onWaitingListener;
    private Action1<Integer> onAssignedIdListener;

    private static final Map<String, Direction> stringToDirection = Map.of("UP", Direction.NORTH, "RIGHT",
            Direction.EAST, "DOWN", Direction.SOUTH, "LEFT", Direction.WEST);
    private static final Map<Direction, String> directionToString = Map.of(Direction.NORTH, "UP", Direction.EAST,
            "RIGHT", Direction.SOUTH, "DOWN", Direction.WEST, "LEFT");
    private static final Map<String, Turn> stringToTurn = Map.of("LEFT", Turn.LEFT, "RIGHT", Turn.RIGHT);
    private static final int MAX_PLAYERS = 6;
    private HttpRequest.Builder preparedRequest = HttpRequest.newBuilder().header("Content-Type", "application/json")
            .timeout(Duration.ofSeconds(1));
    private Timer timer;

    // Playing
    private ObjectMapper mapper = new ObjectMapper();
    private URI[] superpeers;
    private int numberOfSuperPeersForEfficiency;
    private HttpClient steeringSender = HttpClient.newHttpClient();

    public RestModel(ITronModel model) throws FileNotFoundException, IOException {
        super(model);
        super.setOnWaiting(i -> {
            this.onWaitingListener.invoke(i);
        });
        Properties props = new Properties();
        props.load(new FileInputStream(PROPS_FILE));
        nameServiceUri = props.getProperty("nameserver");
        var ipPrefix = props.getProperty("ip-prefix");
        try {
            ownIpAddress = getOwnAddress(ipPrefix);
            ownUri = new URIBuilder().setScheme("http").setHost(ownIpAddress.getHostAddress().split("%")[0])
                    .setPort(httpServer.getPort()).build();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private InetAddress getOwnAddress(String ipPrefix) throws Exception {
        var interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            var networkInterface = interfaces.nextElement();
            var addresses = networkInterface.getInetAddresses();
            while (addresses.hasMoreElements()) {
                var inetAddress = addresses.nextElement();
                if (inetAddress.toString().replace("/", "").startsWith(ipPrefix))
                    return inetAddress;
            }
        }
        throw new Exception("Couldn't find own network address");
    }

    @Override
    public void startGame(int playerCount) {
        if (this.onWaitingListener != null)
            this.onWaitingListener.invoke(playerCount);
        this.localPlayerCount = playerCount;
        try {
            this.register();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnWaiting(Action1<Integer> listener) {
        this.onWaitingListener = listener;
    }

    private void register() throws Exception {
        this.lookupCoordinator();
        if (this.coordinatorAddress == null) { // no one is coordinator
            registerAsCoordinator();
            return;
        }
        System.out.println("Coordinator found: " + this.coordinatorAddress);
        this.httpServer.startServer();
        if (ownUri.toString().equals(this.coordinatorAddress)) { // we are coordinator and start listening
            startCoordinator();
            var status = this.handleRegistrationRequest( // register ourself
                    new Pair<>(ownIpAddress, new Registration(localPlayerCount,
                            ownUri)));
            if (status != HttpStatus.OK_200) {
                System.err.println("Could not register as coordinator: " + status.getReasonPhrase());
                System.exit(1);
            }
        } else { // we register at other found coordinator
            registerAtCoordinator();
        }
    }

    /**
     * After this, {@code coordinatorAddress} is set to the address of the
     * coordinator
     * 
     * @throws Exception
     */
    private void lookupCoordinator() throws Exception {
        var httpClient = HttpClient.newHttpClient();
        var request = HttpRequest
                .newBuilder(new URI(this.nameServiceUri + "/entry/tron.coordinator"))
                .GET()
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        ObjectMapper mapper = new ObjectMapper();
        var entry = mapper.readValue(response.body(), Entry.class);
        this.coordinatorAddress = entry != null ? entry.address : null;
    }

    private void startCoordinator() throws IOException {
        this.gameRunning = false;
        timer = new Timer();

        timer.schedule(new TimerTask() { // start game with players found after 30sek
            @Override
            public void run() {
                RestModel.this.startRestGame(playersPerSupernode.values().stream().reduce(0,
                        Integer::sum));
            }
        }, 30000);

        System.out.println("Opening registrations");
        this.httpServer.on(Method.PUT, "registration", Registration.class, this::handleRegistrationRequest);
    }

    private synchronized HttpStatus handleRegistrationRequest(Pair<InetAddress, Registration> data) {
        System.out.println("Registration request from " + data.getValue().uri);
        if (!areRegistrationsAccepted())
            return HttpStatus.GONE_410; // we already play

        if (playersPerSupernode.size() + data.getValue().playerCount > 6)
            return HttpStatus.NOT_ACCEPTABLE_406; // no space for requestet player count
        var inetSocketAdr = data.getValue().uri.split("//")[1];
        var startOfPort = inetSocketAdr.lastIndexOf(":") + 1;
        playersPerSupernode.put(new InetSocketAddress(
                inetSocketAdr.substring(0, startOfPort - 1),
                Integer.parseInt(inetSocketAdr.substring(startOfPort))),
                data.getValue().playerCount);
        int numberOfPlayers = playersPerSupernode.values().stream().reduce(0, Integer::sum);
        if (numberOfPlayers == MAX_PLAYERS)
            startRestGame(MAX_PLAYERS);

        return HttpStatus.OK_200; // register supernode
    }

    private void registerAtCoordinator() throws Exception {
        var httpClient = HttpClient.newHttpClient();
        var request = preparedRequest
                .uri(new URI(this.coordinatorAddress + "/registration"))
                .timeout(Duration.ofSeconds(1))
                .PUT(BodyPublishers.ofString(new ObjectMapper()
                        .writeValueAsString(new Registration(localPlayerCount,
                                ownUri))))
                .build();
        HttpResponse<String> response = null;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            switch (response.statusCode()) {
                case 200:
                    System.out.println("Registered at other coordinator");
                    this.httpServer.on(Method.PUT, "game", Supernode[].class, this::handleGameRequest);
                    return;
                case 406:
                    System.err.println("No space for requested player count");
                    System.exit(1);
                    break;
                case 410:
                    System.out.println("Coordinator already playing... We are coordinator!");
                    registerAsCoordinator();
                    break;
            }
        } catch (IOException e) {
            System.out.println("Coordinator didn't answer... We are coordinator!");
            registerAsCoordinator();
        }
    }

    private void registerAsCoordinator() throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        var req = HttpRequest
                .newBuilder(new URI(this.nameServiceUri + "/entry"))
                .header("Content-Type", "application/json")
                .PUT(BodyPublishers.ofString(
                        new ObjectMapper()
                                .writeValueAsString(new Entry(
                                        "tron.coordinator",
                                        ownUri.toString()))))
                .build();
        HttpResponse<String> response = client.send(req, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() != 200) {
            System.err.println("Could not register as coordinator: " + response.statusCode());
            System.exit(1);
        }
        register();
    }

    private HttpStatus handleGameRequest(Pair<InetAddress, Supernode[]> data) {
        System.out.println("Game request received from " + data.getKey() + ": " + Arrays.toString(data.getValue()));
        try {
            startLocalGame(data.getValue());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return HttpStatus.OK_200;
    }

    private HttpStatus handleSteeringRequest(Pair<InetAddress, Steering> data) {
        super.setLightCycleTurn(stringToTurn.get(data.getValue().turn), data.getValue().id);
        return HttpStatus.OK_200;
    }

    @Override
    public void setLightCycleTurn(Turn turn, int id) {
        super.setLightCycleTurn(turn, id);
        try {
            sendSteering(turn, id);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void sendSteering(Turn turn, int id) throws JsonProcessingException, URISyntaxException {
        for (int i = 0; i < numberOfSuperPeersForEfficiency; i++) {
            var request = preparedRequest
                    .uri(new URI(superpeers[i].toString() + "/steering"))
                    .PUT(BodyPublishers.ofString(mapper
                            .writeValueAsString(new Steering(id, turn))))
                    .build();
            System.out.println("Sending steering to " + superpeers[i]);
            steeringSender.sendAsync(request, HttpResponse.BodyHandlers.ofString());
        }
    }

    private boolean areRegistrationsAccepted() {
        return !gameRunning;
    }

    private void startRestGame(int numberOfPlayers) {
        timer.cancel();
        Thread initThread = new Thread(() -> {
            gameRunning = true;

            var grid = super.getGrid();
            var lightCycles = new LinkedList<LightCycle>();
            for (int i = 0; i < numberOfPlayers; i++)
                lightCycles.add(new LightCycle(i));
            grid.setLightCycles(lightCycles);
            grid.initStartingPositions();

            // build InitialLightCycles with starting position and id
            playersPerSupernode.forEach((address, playerCount) -> {
                this.initialLightCycles.put(address,
                        IntStream.range(this.initialLightCycles.values().stream().mapToInt(l -> l.length).sum(),
                                this.initialLightCycles.values().stream().mapToInt(k -> k.length).sum()
                                        + playerCount)
                                .mapToObj(idx -> new InitialLightCycle(idx)
                                        .withPosition(
                                                lightCycles.get(idx).coordinates.get(0).x,
                                                lightCycles.get(idx).coordinates.get(0).y)
                                        .withDirection(
                                                directionToString.get(lightCycles.get(idx).direction)))
                                .collect(Collectors.toList()).toArray(new InitialLightCycle[0]));
            });

            Supernode[] config = this.initialLightCycles.entrySet().stream()
                    .map(pair -> new Supernode(pair.getKey(), pair.getValue())).collect(Collectors.toList())
                    .toArray(new Supernode[0]);
            String configAsJson = "";
            try {
                configAsJson = new ObjectMapper().writeValueAsString(config);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            var httpClient = HttpClient.newHttpClient();
            var requestBuilder = HttpRequest.newBuilder()
                    .PUT(BodyPublishers.ofString(configAsJson));

            // send game config to all players except for us
            playersPerSupernode.keySet().stream().filter(adr -> !adr.getAddress().equals(ownIpAddress)).forEach(adr -> {
                try {
                    var uri = new URIBuilder()
                            .setScheme("http")
                            .setHost(adr.getAddress().getHostAddress())
                            .setPath("game")
                            .setPort(adr.getPort())
                            .build();
                    var request = requestBuilder
                            .uri(uri)
                            .build();
                    httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            try {
                startLocalGame(config);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
            }
        });

        initThread.start();
    }

    private void startLocalGame(Supernode[] config) throws URISyntaxException {
        this.numberOfSuperPeersForEfficiency = config.length - 1;

        var lightCycles = new ArrayList<LightCycle>();
        superpeers = new URI[numberOfSuperPeersForEfficiency];
        var i = 0;
        for (Supernode sn : config) {
            if (sn.address.equals(ownUri.toString())) {
                // invoke with possible offset
                if (onAssignedIdListener != null)
                    onAssignedIdListener.invoke(sn.lightCycles[0].id);
            } else {
                // save addresses of other superpeers
                superpeers[i] = new URI(sn.address);
                i++;

            }
            // set lightcycles with received stats
            for (InitialLightCycle lc : sn.lightCycles) {
                var lightCycle = new LightCycle(lc.id);
                lightCycle.coordinates.add(new Coordinate(lc.position.x, lc.position.y));
                lightCycle.direction = stringToDirection.get(lc.direction);
                lightCycles.add(lightCycle);
            }
        }

        lightCycles.sort(Comparator.comparingInt(lc -> lc.getId()));

        super.getGrid().setLightCycles(lightCycles);
        super.startGame(lightCycles.size());

        System.out.println("Game started with " + lightCycles.size() + " players");

        this.httpServer.on(Method.PUT, "steering", Steering.class, this::handleSteeringRequest);
    }

    @Override
    public void setOnGameOver(Action2<Iterable<? extends ILightCycle>, String> onGameOver) {
        super.setOnGameOver((lcs, msg) -> {
            gameRunning = false;
            onGameOver.invoke(lcs, msg);
        });
    }

    @Override
    public void setOnIdAssigned(Action1<Integer> listener) {
        onAssignedIdListener = listener;
        super.setOnIdAssigned(listener);
    }
}

class Entry {
    public String name;
    public String address;
    public Date updated;

    public Entry() {
    }

    public Entry(String name, String address) {
        this.name = name;
        this.address = address;
    }
}

class Registration {
    public int playerCount;
    public String uri;

    public Registration() {
    }

    public Registration(int playerCount, URI uri) {
        this.playerCount = playerCount;
        this.uri = uri.toString();
    }
}

class Steering {
    public int id;
    public String turn;

    public Steering() {
    }

    public Steering(int id, Turn turn) {
        this.id = id;
        this.turn = turn.name();
    }
}

class Supernode {
    public String address; // URI
    public InitialLightCycle[] lightCycles;

    public Supernode() {
    }

    public Supernode(InetSocketAddress address, InitialLightCycle[] lightcycles) {
        this.address = new URIBuilder().setScheme("http").setHost(address.getAddress().getHostAddress())
                .setPort(address.getPort()).toString();
        this.lightCycles = lightcycles;
    }
}

class InitialLightCycle {
    public int id;
    public Position position;
    public String direction;

    public InitialLightCycle() {
    }

    public InitialLightCycle(int id) {
        this.id = id;
        this.position = new Position();
    }

    public InitialLightCycle withPosition(int x, int y) {
        this.position.x = x;
        this.position.y = y;
        return this;
    }

    public InitialLightCycle withDirection(String direction) {
        this.direction = direction;
        return this;
    }
}

class Position {
    public int x;
    public int y;
}
