package tron.model;

public final class Protocol {
    public static final byte COUNTDOWN = 1;
    public static final byte GAME_STARTED = 2;
    public static final byte UPDATE = 3;
    public static final byte DEATH = 4;
    public static final byte GAME_OVER = 5;
    public static final byte SET_LIGHTCYCLE_TURN = 6;
    public static final byte ID_OFFSET = 7;
}
