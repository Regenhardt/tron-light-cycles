package tron.model;

import java.util.function.Consumer;

import edu.cads.bai5.vsp.tron.view.Coordinate;
import tron.util.Action;
import tron.util.Action1;
import tron.util.Action2;

abstract class TronModelDecorator implements ITronModel {
    protected ITronModel model;

    public TronModelDecorator(ITronModel model) {
        this.model = model;
    }

    @Override
    public void startGame(int playerCount) {
        model.startGame(playerCount);
    }

    @Override
    public void setLightCycleTurn(Turn turn, int id) {
        model.setLightCycleTurn(turn, id);
    }

    @Override
    public void setOnPeerCountUpdate(Consumer<Short> onPeerCountUpdate) {
        model.setOnPeerCountUpdate(onPeerCountUpdate);
    }

    @Override
    public void resetGame() {
        model.resetGame();
    }

    @Override
    public void setOnWaiting(Action1<Integer> onWaiting) {
        model.setOnWaiting(onWaiting);
    }

    @Override
    public void setOnGameReset(Action onGameReset) {
        model.setOnGameReset(onGameReset);
    }

    @Override
    public void setOnGameStarted(Action onGameStarted) {
        model.setOnGameStarted(onGameStarted);
    }

    @Override
    public void setOnGameOver(Action2<Iterable<? extends ILightCycle>, String> onGameOver) {
        model.setOnGameOver(onGameOver);
    }

    @Override
    public void setOnDeath(Action1<Coordinate> onDeath) {
        model.setOnDeath(onDeath);
    }

    @Override
    public void setOnUpdate(Action1<Iterable<? extends ILightCycle>> onUpdate) {
        model.setOnUpdate(onUpdate);
    }

    @Override
    public void setOnCountdown(Action1<Integer> onCountdown) {
        model.setOnCountdown(onCountdown);
    }

    @Override
    public IGrid getGrid() {
        return model.getGrid();
    }

    @Override
    public void setOnIdAssigned(Action1<Integer> listener) {
        model.setOnIdAssigned(listener);
    }
}