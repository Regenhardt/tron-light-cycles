package tron.model;

public enum TronCommand {
    START,
    ALLREADY,
    LEFT,
    RIGHT,
    GAMEOVER,
    RESET
}
