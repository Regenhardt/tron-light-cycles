package tron.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javafx.scene.input.KeyCode;
import tron.model.ITronModel;
import tron.util.Action1;
import tron.view.ITronViewWrapper;

public class TronController {
    private final ITronModel model;
    private final ITronViewWrapper view;
    private final Keymap keymap;

    public TronController(Properties props, ITronModel model, ITronViewWrapper view) {
        if (model == null || view == null) {
            throw new IllegalArgumentException("Model and view must not be null!");
        }

        this.model = model;
        this.view = view;
        this.keymap = new Keymap(props);
        this.initInputs();
    }

    private void initInputs() {
        this.view.setControlSettings(keymap.getControlSettings());
        this.view.setOnGameStarted(this::startGame);
        this.view.setOnKeyPressed(this::onKeyPressed);
        this.view.setOnResetClicked(this.model::resetGame);
    }

    private void onKeyPressed(KeyCode key) {
        Action1<ITronModel> command = keymap.getCommand(key);
        if (command != null) {
            command.invoke(this.model);
        }
    }

    private void startGame(int playerCount) {
        this.model.startGame(playerCount);
    }
}
