package tron.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import tron.model.ITronModel;
import tron.model.Turn;
import tron.util.Action1;

public class Keymap {
    static final int P1_ID = 0;
    static final String P1_LEFT = "P1_LEFT";
    static final String P1_RIGHT = "P1_RIGHT";
    static final int P2_ID = 1;
    static final String P2_LEFT = "P2_LEFT";
    static final String P2_RIGHT = "P2_RIGHT";
    static final int P3_ID = 2;
    static final String P3_LEFT = "P3_LEFT";
    static final String P3_RIGHT = "P3_RIGHT";
    static final int P4_ID = 3;
    static final String P4_LEFT = "P4_LEFT";
    static final String P4_RIGHT = "P4_RIGHT";
    static final int P5_ID = 4;
    static final String P5_LEFT = "P5_LEFT";
    static final String P5_RIGHT = "P5_RIGHT";
    static final int P6_ID = 5;
    static final String P6_LEFT = "P6_LEFT";
    static final String P6_RIGHT = "P6_RIGHT";

    final HashMap<KeyCode, Action1<ITronModel>> keyMap = new HashMap<KeyCode, Action1<ITronModel>>();
    final ArrayList<Pair<Integer, ArrayList<KeyCode>>> controlSettings = new ArrayList<>();

    public Keymap(Properties props) {
        keyMap.put(KeyCode.valueOf(props.getProperty(P1_LEFT)), (model) -> model.setLightCycleTurn(Turn.LEFT, P1_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P1_RIGHT)), (model) -> model.setLightCycleTurn(Turn.RIGHT, P1_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P2_LEFT)), (model) -> model.setLightCycleTurn(Turn.LEFT, P2_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P2_RIGHT)), (model) -> model.setLightCycleTurn(Turn.RIGHT, P2_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P3_LEFT)), (model) -> model.setLightCycleTurn(Turn.LEFT, P3_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P3_RIGHT)), (model) -> model.setLightCycleTurn(Turn.RIGHT, P3_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P4_LEFT)), (model) -> model.setLightCycleTurn(Turn.LEFT, P4_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P4_RIGHT)), (model) -> model.setLightCycleTurn(Turn.RIGHT, P4_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P5_LEFT)), (model) -> model.setLightCycleTurn(Turn.LEFT, P5_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P5_RIGHT)), (model) -> model.setLightCycleTurn(Turn.RIGHT, P5_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P6_LEFT)), (model) -> model.setLightCycleTurn(Turn.LEFT, P6_ID));
        keyMap.put(KeyCode.valueOf(props.getProperty(P6_RIGHT)), (model) -> model.setLightCycleTurn(Turn.RIGHT, P6_ID));

        buildControlSettings(props);

    }

    private void buildControlSettings(Properties props) {
        controlSettings.add(new Pair<Integer, ArrayList<KeyCode>>(P1_ID, new ArrayList<KeyCode>() {
            {
                add(KeyCode.valueOf(props.getProperty(P1_LEFT)));
                add(KeyCode.valueOf(props.getProperty(P1_RIGHT)));
            }
        }));
        controlSettings.add(new Pair<Integer, ArrayList<KeyCode>>(P2_ID, new ArrayList<KeyCode>() {
            {
                add(KeyCode.valueOf(props.getProperty(P2_LEFT)));
                add(KeyCode.valueOf(props.getProperty(P2_RIGHT)));
            }
        }));
        controlSettings.add(new Pair<Integer, ArrayList<KeyCode>>(P3_ID, new ArrayList<KeyCode>() {
            {
                add(KeyCode.valueOf(props.getProperty(P3_LEFT)));
                add(KeyCode.valueOf(props.getProperty(P3_RIGHT)));
            }
        }));
        controlSettings.add(new Pair<Integer, ArrayList<KeyCode>>(P4_ID, new ArrayList<KeyCode>() {
            {
                add(KeyCode.valueOf(props.getProperty(P4_LEFT)));
                add(KeyCode.valueOf(props.getProperty(P4_RIGHT)));
            }
        }));
        controlSettings.add(new Pair<Integer, ArrayList<KeyCode>>(P5_ID, new ArrayList<KeyCode>() {
            {
                add(KeyCode.valueOf(props.getProperty(P5_LEFT)));
                add(KeyCode.valueOf(props.getProperty(P5_RIGHT)));
            }
        }));
        controlSettings.add(new Pair<Integer, ArrayList<KeyCode>>(P6_ID, new ArrayList<KeyCode>() {
            {
                add(KeyCode.valueOf(props.getProperty(P6_LEFT)));
                add(KeyCode.valueOf(props.getProperty(P6_RIGHT)));
            }
        }));
    }

    public Action1<ITronModel> getCommand(KeyCode key) {
        return keyMap.getOrDefault(key, null);
    }

    public ArrayList<Pair<Integer, ArrayList<KeyCode>>> getControlSettings() {
        return controlSettings;
    }
}