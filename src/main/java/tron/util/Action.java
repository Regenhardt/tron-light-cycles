package tron.util;

public interface Action{
    public void invoke();
}
