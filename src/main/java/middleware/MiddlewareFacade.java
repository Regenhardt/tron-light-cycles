package middleware;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javafx.util.Pair;

public class MiddlewareFacade implements IMiddleware {
    private int port;
    private PeerFinder peerFinder;
    private BiConsumer<IMiddleware, PeerId> configureMiddleware;
    private Consumer<IMiddleware> registrationFactory;
    private Consumer<Short> onPeerCountListener;
    private Runnable allReady;
    private ClientStub client;
    private ServerStub server;
    private Properties props;
    private Thread hostingThread;
    private HashMap<PeerId, InetAddress> peerIds;
    private HashMap<InetAddress, PeerId> peerIdsReverse;

    public MiddlewareFacade(Properties props) {
        this.props = props;
        port = Integer.parseInt(props.getProperty("port"));
    }

    public MiddlewareFacade(ClientStub client, ServerStub server, Properties props) {
        this(props);
        this.client = client;
        this.server = server;
    }

    @Override
    public void initPeerNetwork(
            int count,
            BiConsumer<IMiddleware, PeerId> configureMiddleware,
            Consumer<IMiddleware> registrationFactory,
            Runnable allReady) throws IOException {
        this.configureMiddleware = configureMiddleware;
        this.registrationFactory = registrationFactory;
        this.allReady = allReady;
        peerFinder = new PeerFinder(this, props);
        peerFinder.setOnPeerCountUpdate(onPeerCountListener);
        peerFinder.findPeers(count);
    }

    public void peersFound(PeerNetwork network) throws IOException, InterruptedException {
        if (network.hostObjectLocally) {
            this.createPeerIds(network.peers);
            // Signal other clients that the network is ready so they start listening
            System.out.println("Sending network ready to " + network.peers.size() + " peers");
            for (PeerId id = new PeerId(1); id.ID <= network.peers.size(); id = new PeerId(id.ID + 1)) {
                network.clientStub.sendTcp(
                        peerIds.get(id),
                        MiddlewareProtocol.NETWORK_FINISHED,
                        id,
                        network.multicastGroup);
            }
            // Host object and connect to own created stubs
            ServerSocket serverSocket = new ServerSocket(port);
            hostObject(network, serverSocket);
            Socket socket = new Socket(serverSocket.getInetAddress(),
                    serverSocket.getLocalPort());
            this.peerIds.put(new PeerId(0), socket.getLocalAddress()); // Add local client to peers
            this.peerIdsReverse.put(socket.getLocalAddress(), new PeerId(0));
            this.client = new ClientStub(socket, network.multicastGroup, port);
            this.server = new ServerStub(socket, network.multicastGroup, port);
            System.out.println("Created new server stub from peersFound on host: " + this.server);
            this.configureMiddleware.accept(this, network.peerId); // To application

            hostingThread.join();
            // Signal application that network is finished and ready for use
            this.allReady.run();
        } else {
            this.peerIds = new HashMap<>();
            this.peerIdsReverse = new HashMap<>();
            this.peerIds.put(new PeerId(0), network.peers.get(0).getInetAddress());
            this.peerIdsReverse.put(network.peers.get(0).getInetAddress(), new PeerId(0));
            this.client = network.clientStub;
            this.server = network.serverStub;
            this.configureMiddleware.accept(this, network.peerId); // To application
        }
        this.peerFinder = null;
    }

    private void createPeerIds(List<Socket> sockets) {
        this.peerIds = new HashMap<>();
        this.peerIdsReverse = new HashMap<>();
        int i = 1;
        for (Socket s : sockets) {
            peerIds.put(new PeerId(i), s.getInetAddress());
            peerIdsReverse.put(s.getInetAddress(), new PeerId(i++));
        }
    }

    public void hostObject(PeerNetwork network, ServerSocket serverSocket) throws IOException {
        hostingThread = new Thread(() -> {
            Socket socketToSelf;
            try {
                socketToSelf = serverSocket.accept();
                serverSocket.close();
                System.out.println("Local connection accepted");
                network.clientStub.addSocket(socketToSelf);
                network.serverStub.addSocket(socketToSelf);
                var hostingMiddleware = new MiddlewareFacade(network.clientStub, network.serverStub, props);
                var superpeerPeers = new ArrayList<>(network.peers);
                superpeerPeers.add(socketToSelf);
                hostingMiddleware.createPeerIds(superpeerPeers);
                this.registrationFactory.accept(hostingMiddleware); // To
                                                                    // application
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        });
        hostingThread.start();
    }

    @Override
    public void invokeReliable(PeerId target, byte id, Object... args) {
        try {
            client.sendTcp(this.peerIds.get(target), id, args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void invokeQuick(PeerId target, byte id, Object... args) {
        try {
            client.sendUdpUnicast(this.peerIds.get(target), id, args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publishReliable(byte id, Object... args) {
        try {
            client.sendTcp(id, args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publishQuick(byte id, Object... args) {
        try {
            client.sendUdpMulticast(id, args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnCall(BiConsumer<PeerId, Pair<Byte, Object[]>> listener) {
        this.server.register((adr, data) -> listener.accept(this.peerIdsReverse.get(adr), data));
    }

    @Override
    public void setOnPeerCount(Consumer<Short> listener) {
        onPeerCountListener = listener;
    }
}
