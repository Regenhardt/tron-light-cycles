package middleware;

public class MiddlewareProtocol {
    public static final byte NOTIFY = 124;
    public static final byte NETWORK_FINISHED = 125;
    public static final byte HANDOVER = 126;
    public static final byte LOOKUP = 127;
}
