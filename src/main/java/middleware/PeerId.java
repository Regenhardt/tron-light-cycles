package middleware;

public class PeerId implements java.io.Serializable {
    public final int ID;

    public PeerId(int id) {
        this.ID = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PeerId other = (PeerId) obj;
        return this.ID == other.ID;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(ID);
    }
}