package middleware;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javafx.util.Pair;

public interface IMiddleware {

    public final static String NETWORK_CONFIG_FILE = "network.properties";

    public static IMiddleware create() throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(NETWORK_CONFIG_FILE));
        return new MiddlewareFacade(props);
    }

    /**
     * Initializes the middleware network.
     * 
     * @param count               The number of peers in the network.
     * @param configureMiddleware A callback that is invoked when the network is
     *                            finished. Parameters are the middleware instance
     *                            and the ID for this peer.
     * @param regisrationFactory  A callback that is invoked on one of the peers to
     *                            host the remote object. Parameter is the
     *                            middleware intance to use for communication with
     *                            the connected peers.
     */
    public void initPeerNetwork(
            int count,
            BiConsumer<IMiddleware, PeerId> configureMiddleware,
            Consumer<IMiddleware> registrationFactory,
            Runnable allReady) throws IOException;

    /**
     * Uses a reliable but slow communication channel to invoke a method on a remote
     * object.
     */
    public void invokeReliable(PeerId target, byte id, Object... args);

    public void invokeQuick(PeerId target, byte id, Object... args);

    public void publishReliable(byte id, Object... args);

    public void publishQuick(byte id, Object... args);

    public void setOnCall(BiConsumer<PeerId, Pair<Byte, Object[]>> listener);

    public void setOnPeerCount(Consumer<Short> onPeerCountListener);
}