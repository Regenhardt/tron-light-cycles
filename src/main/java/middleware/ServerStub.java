package middleware;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import javafx.util.Pair;

class ServerStub {
    private List<Socket> tcpSockets;
    private InetAddress multicastGroup;
    private MulticastSocket udpSocket; // used for UDP Unicast and Multicast
    private int port;

    private List<Thread> tcpListener;
    private Thread multicastListener;

    private BiConsumer<InetAddress, Pair<Byte, Object[]>> onCall;

    public ServerStub(InetAddress multicastGroup, int port) throws SocketException, IOException {
        System.out.println("creating ServerStub with port: " + port);
        this.multicastGroup = multicastGroup;
        this.initializeSocket(port);
        this.setMulticastGroup(multicastGroup);
        this.tcpListener = new ArrayList<>();
        this.tcpSockets = new ArrayList<>();
    }

    public ServerStub(Socket socket, InetAddress multicastGroup, int port) throws IOException {
        this(List.of(socket), multicastGroup, port);
    }

    public ServerStub(List<Socket> sockets, InetAddress multicastGroup, int port) throws IOException {
        this(multicastGroup, port);
        this.tcpSockets = sockets;
        this.listenTCP();
    }

    public void setMulticastGroup(InetAddress multicastGroup) throws IOException {
        if (this.multicastGroup != null)
            try {
                this.udpSocket.leaveGroup(this.multicastGroup);
            } catch (SocketException e) {
                System.out.println("Not a member of group");
            }
        this.multicastGroup = multicastGroup;
        this.udpSocket.joinGroup(multicastGroup);
    }

    public void initializeSocket(int port) throws IOException {
        System.out.println("setting ServerStub port: " + port);
        this.port = port;
        this.udpSocket = new MulticastSocket(port);
        this.udpSocket.joinGroup(multicastGroup);
        listenUdp();
    }

    public void addSocket(Socket socket) {
        tcpSockets.add(socket);
        var listener = getTcpListener(socket);
        listener.start();
        tcpListener.add(listener);
    }

    private void listenUdp() {
        multicastListener = new Thread(() -> {
            while (true) {
                byte[] buffer = new byte[1024];
                DatagramPacket message = new DatagramPacket(buffer, buffer.length);

                try {
                    udpSocket.receive(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (ByteArrayInputStream bais = new ByteArrayInputStream(message.getData())) {
                    var methodId = bais.read();
                    System.out.println("<--- Received UDP: " + methodId + " (" + this + ")");
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    var paramCount = ois.readInt();
                    System.out.println("Param count: " + paramCount);
                    var inputs = new Object[paramCount];
                    for (int i = 0; i < paramCount; i++) {
                        inputs[i] = ois.readObject();
                        System.out.println("Param " + i + ": " + inputs[i]);
                    }
                    if (onCall != null)
                        onCall.accept(message.getAddress(), new Pair<Byte, Object[]>((byte) methodId, inputs));
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        multicastListener.start();
        Thread.yield();
    }

    private void listenTCP() {
        tcpListener = tcpSockets.stream().map(this::getTcpListener).toList();

        for (Thread thread : tcpListener) {
            thread.start();
        }
    }

    private Thread getTcpListener(Socket s) {
        return new Thread(() -> {
            try (InputStream is = s.getInputStream();
                    ObjectInputStream in = new ObjectInputStream(s.getInputStream())) {
                while (true) {
                    var methodId = is.read();
                    System.out.println("<--- Received TCP: " + methodId + " (" + this + ")");
                    var paramCount = in.readInt();
                    System.out.println("Param count: " + paramCount);
                    var inputs = new Object[paramCount];
                    for (int i = 0; i < paramCount; i++) {
                        inputs[i] = in.readObject();
                        System.out.println("Param " + i + ": " + inputs[i]);
                    }
                    if (onCall != null)
                        onCall.accept(s.getInetAddress(), new Pair<Byte, Object[]>((byte) methodId, inputs));
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                try {
                    s.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public void register(BiConsumer<InetAddress, Pair<Byte, Object[]>> callListener) {
        onCall = callListener;
        System.out.println("Register on " + " (" + this + ") for " + this.tcpListener.size()
                + " tcp sockets and the multicast group "
                + this.multicastGroup);
    }

}