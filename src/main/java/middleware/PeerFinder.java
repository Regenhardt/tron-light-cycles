package middleware;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;
import java.util.function.Consumer;

public class PeerFinder {

    private MiddlewareFacade middleware;

    private int targetCount;

    private int basePort;

    /**
     * Start of multicast range
     */
    private InetAddress multicastStart;

    /**
     * End of multicast range
     */
    private InetAddress multicastEnd;

    /**
     * Listens for new connections
     */
    private ServerSocket superPeerSocket;

    private ServerStub serverStub;
    private ClientStub clientStub;

    /**
     * Accepts new connections to the {@code superPeerSocket} and delegates them
     */
    private Thread conciergeThread;

    private long timestamp;

    private PeerNetwork peerNetwork = new PeerNetwork();

    private Consumer<Short> peerCountListener;

    public PeerFinder(MiddlewareFacade middleware, Properties props) throws SocketException, IOException {
        this.middleware = middleware;
        setConfig(props);
    }

    public void setOnPeerCountUpdate(Consumer<Short> listener) {
        peerCountListener = listener;
    }

    /**
     * Acccepts connection to the prior set port and delegates new connection to
     * runNewPeerSocket.
     */
    private void acceptConnections() {
        try {
            this.superPeerSocket = new ServerSocket(basePort);
            conciergeThread = new Thread(() -> {
                try {
                    while (true) {
                        Socket peerSocket = this.superPeerSocket.accept(); // superpeer connects to us
                        System.out
                                .println("Superpeer " + peerSocket.getInetAddress().toString() + " connected to me!");
                        runNewPeerSocket(peerSocket);
                    }
                } catch (IOException e) {
                    // e.printStackTrace();
                    System.out.println("Socket closed.");
                }
            });
            conciergeThread.start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Listening to TCP...");
    }

    /**
     * Sets the socket as the peerSocket and starts a new thread to listen to it.
     * 
     * @param peerSocket The socket to run
     * @throws IOException
     */
    private void runNewPeerSocket(Socket peerSocket) throws IOException {
        System.out.println("im peer and have a socket with " + peerSocket.getInetAddress() + "!");
        System.out.println("Close all peer-sockets and connect to superpeer"
                + peerSocket.getInetAddress().toString() + "!");
        this.peerNetwork.hostObjectLocally = false;
        for (Socket peer : this.peerNetwork.peers) {
            peer.close();
        }
        this.peerNetwork.peers.clear();
        this.peerNetwork.peers.add(peerSocket);
        this.serverStub = new ServerStub(this.multicastStart, this.basePort + this.targetCount);
        System.out.println("Created server stub from runNewClientSocket: " + this.serverStub);
        this.serverStub.addSocket(peerSocket);
        this.clientStub = new ClientStub(this.multicastStart, this.basePort + this.targetCount);
        this.clientStub.addSocket(peerSocket);
        startListener();
    }

    private void startListener() {
        this.serverStub.register((adr, pair) -> {
            var method = pair.getKey();
            var args = pair.getValue();
            System.out.println("von: " + adr.toString() + " - Nachricht: " + method);
            try {
                switch (method) {
                    case MiddlewareProtocol.HANDOVER: // we are superpeer and get peers
                        for (int i = 0; i < args.length; i++) {
                            InetAddress senderAddress = (InetAddress) args[i];
                            this.connectToPeer(senderAddress);
                        }
                        this.connectToPeer(adr);
                        break;
                    case MiddlewareProtocol.NETWORK_FINISHED: // network finished, we are peer and get a peerId and
                                                              // a new in-game-multicast-address
                        System.out.println("Bekam NETWORK_FINISHED und rufe finishPeerNetwork auf.");
                        peerNetwork.peerId = (PeerId) args[0];
                        System.out.println("Meine peerId ist " + peerNetwork.peerId + "!");
                        peerNetwork.multicastGroup = (InetAddress) args[1];
                        serverStub.setMulticastGroup((InetAddress) args[1]);
                        clientStub.setMulticastGroup((InetAddress) args[1]);
                        finishPeerNetwork();
                        break;
                    case MiddlewareProtocol.NOTIFY: // notification about count of found peers
                        short peerCount = (short) args[0];
                        if (peerCountListener != null) {
                            System.out.println(
                                    "Bekam NOTIFY und rufe auf: peerCountListener.accept(" + peerCount + ")");
                            peerCountListener.accept(peerCount);
                        }
                        break;
                    case MiddlewareProtocol.LOOKUP:
                        if (!peerNetwork.hostObjectLocally)
                            break;
                        if (isOwnAddress(adr))
                            break;
                        System.out.println("Bekam LOOKUP und rufe lookup auf.");
                        long timestamp = (long) args[0];
                        if (this.timestamp < timestamp) { // we are superpeer and connect to peer
                            connectToPeer(adr);
                            if (targetCountReached())
                                finishPeerNetwork();
                        } else { // we wrongly thought we are superpeer and handover peers
                            handOverSuperpeer(adr);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Sets used addresses (multicast + tcp port) based on the given properties
     * 
     * @param props
     */
    public void setConfig(Properties props) {
        basePort = Integer.parseInt(props.getProperty("port"));
        try {
            multicastStart = InetAddress.getByName(props.getProperty("multicast-start"));
            multicastEnd = InetAddress.getByName(props.getProperty("multicast-end"));
        } catch (Exception e1) {
            try {
                multicastStart = InetAddress.getByName("FF05::500");
                multicastEnd = InetAddress.getByName("FF05::3485");
            } catch (Exception e2) {
                // won't happen because of hardcoded fallback addresses
            }
        }
    }

    /**
     * Starts the 🐋-algorithm to build a network of {@code count} peers
     * 
     * @param count
     * @throws IOException
     */
    public void findPeers(int count) throws IOException {
        this.timestamp = System.currentTimeMillis();
        targetCount = count;
        serverStub = new ServerStub(multicastStart, basePort + count);
        System.out.println("Created server stub from findPeers: " + this.serverStub);
        this.startListener();
        clientStub = new ClientStub(multicastStart, basePort + count);
        this.acceptConnections();
        // Send current timestamp to muticast
        this.clientStub.sendUdpMulticast(MiddlewareProtocol.LOOKUP, timestamp);
    }

    private boolean isOwnAddress(InetAddress address) throws SocketException {
        var interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            var networkInterface = interfaces.nextElement();
            var addresses = networkInterface.getInetAddresses();
            while (addresses.hasMoreElements()) {
                var inetAddress = addresses.nextElement();
                if (inetAddress.equals(address)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Opens a TCP-Connection to the {@code senderAddress} on port {@code port} and
     * saves the socket in a list
     * 
     * @param senderAddress
     * @throws IOException
     */
    private void connectToPeer(InetAddress senderAddress) throws IOException {
        System.out.println("Verbinde mich jetzt per TCP mit Peer " + senderAddress.toString() + "!");
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(senderAddress, basePort));
        this.serverStub.addSocket(socket);
        this.clientStub.addSocket(socket);
        peerNetwork.peers.add(socket);
        peerCountListener.accept((short) (peerNetwork.peers.size() + 1));
        this.clientStub.sendTcp(MiddlewareProtocol.NOTIFY, (short) (peerNetwork.peers.size() + 1));
    }

    /**
     * Checks if the requested number of peers is found
     * 
     * @return
     * @throws Exception
     */
    private boolean targetCountReached() {
        return peerNetwork.peers.size() == targetCount - 1;
    }

    /**
     * Sends encoded peer address list (including self) to new {@code superPeer}
     * over TCP
     * 
     * @param superPeer
     * @throws IOException
     */
    private void handOverSuperpeer(InetAddress superPeer) throws IOException {
        System.out.println("Handover zum neuen Superpeer" + superPeer.toString());
        peerNetwork.hostObjectLocally = false;
        var tempClientStub = new ClientStub(multicastStart, basePort);
        Socket socket = new Socket(superPeer, basePort);
        tempClientStub.addSocket(socket);
        tempClientStub.sendTcp(MiddlewareProtocol.HANDOVER,
                peerNetwork.peers.stream().map(Socket::getInetAddress).toArray(Object[]::new));
        socket.close();
    }

    /**
     * Selects a random multicast address in the configured range thats not the
     * inital configured address (dynamic allocation)
     * 
     * @throws Exception
     */
    private void finishPeerNetwork() throws Exception {
        System.out.println("Ich finishe jetzt das PeerNetwork!");
        superPeerSocket.close();
        conciergeThread.interrupt();
        if (peerNetwork.hostObjectLocally) {
            InetAddress gameMulticast;
            do {
                gameMulticast = generateMulticastAddress();
            } while (gameMulticast.equals(multicastStart));
            peerNetwork.multicastGroup = gameMulticast;
            clientStub.setMulticastGroup(gameMulticast);
            serverStub.setMulticastGroup(gameMulticast);
            peerNetwork.peerId = new PeerId(0);
        }
        peerNetwork.clientStub = clientStub;
        peerNetwork.serverStub = serverStub;
        peerNetwork.clientStub.initializeSocket(basePort);
        peerNetwork.serverStub.initializeSocket(basePort);

        middleware.peersFound(peerNetwork);
    }

    /**
     * 
     * @return random generated ipv6 multicast address in configured range
     * @throws Exception
     */
    public InetAddress generateMulticastAddress() throws Exception {
        var start = multicastStart.getAddress();
        var end = multicastEnd.getAddress();
        var result = ByteBuffer.allocate(16);
        int lowerBound = 0;
        int upperBound = 0;
        boolean upperBoundStillRelevant = true;
        boolean lowerBoundStillRelevant = true;
        boolean divergenceFound = false;
        Random r = new Random();

        for (int i = 0; i < start.length; i++) {

            if (lowerBoundStillRelevant || upperBoundStillRelevant) {

                lowerBound = lowerBoundStillRelevant ? start[i] & 0xFF : 0; // unsigned byte to int
                upperBound = upperBoundStillRelevant ? end[i] & 0xFF : 255; // unsigned byte to int

                if (!divergenceFound) {
                    if (lowerBound == upperBound) { // no divergence
                        result.put((byte) lowerBound);
                        continue;
                    }

                    if (start[i] > end[i]) {
                        throw new Exception("Start address bigger than end address");
                    }
                    divergenceFound = true;
                }
            }

            var rolledValue = r.nextInt(lowerBound, upperBound + 1);
            result.put((byte) rolledValue);

            if (upperBoundStillRelevant || lowerBoundStillRelevant) {
                // rolled upper bound, bound of end address still relevant in next round
                if (upperBoundStillRelevant && rolledValue == upperBound) {
                    lowerBoundStillRelevant = false;
                    // rolled lower bound, bound of start address still relevant in next round
                } else if (lowerBoundStillRelevant && rolledValue == lowerBound) {
                    upperBoundStillRelevant = false;
                } else {
                    upperBoundStillRelevant = lowerBoundStillRelevant = false;
                }
            }
        }
        System.out.println("Ich generierte eine Multicast-Adresse " + Arrays.toString(result.array()) + "!");
        return InetAddress.getByAddress(result.array());
    }
}