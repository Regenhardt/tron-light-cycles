package middleware;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class ClientStub {
    private Map<InetAddress, Object> tcpSockets;
    private Map<InetAddress, OutputStream> tcpStreams = new HashMap<>();
    private Map<InetAddress, ObjectOutputStream> objectOutputStreams;
    private InetAddress multicastGroup;
    private int port;
    private DatagramSocket udpSocket;

    public ClientStub(InetAddress multicastGroup, int port) throws SocketException {
        this.port = port;
        this.multicastGroup = multicastGroup;
        this.udpSocket = new DatagramSocket();
        tcpSockets = new HashMap<>();
        objectOutputStreams = new HashMap<>();
    }

    public ClientStub(Socket socket, InetAddress multicastGroup, int port) throws SocketException {
        this(List.of(socket), multicastGroup, port);
    }

    public ClientStub(List<Socket> sockets, InetAddress multicastGroup, int port)
            throws SocketException {
        this.port = port;
        this.tcpSockets = sockets.stream().collect(Collectors.toMap(Socket::getInetAddress, socket -> socket));
        // Marshaller
        this.objectOutputStreams = sockets.stream()
                .collect(Collectors.toMap(socket -> socket.getInetAddress(), socket -> {
                    try {
                        var stream = socket.getOutputStream();
                        tcpStreams.put(socket.getInetAddress(), stream);
                        return new ObjectOutputStream(stream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }));
        this.multicastGroup = multicastGroup;
        this.udpSocket = new DatagramSocket();
    }

    public void setMulticastGroup(InetAddress multicastGroup) {
        this.multicastGroup = multicastGroup;
    }

    public void initializeSocket(int port) throws IOException {
        this.port = port;
    }

    public void addSocket(Socket socket) {
        tcpSockets.put(socket.getInetAddress(), socket);
        try {
            var stream = socket.getOutputStream();
            tcpStreams.put(socket.getInetAddress(), stream);
            objectOutputStreams.put(socket.getInetAddress(), new ObjectOutputStream(stream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a message to all clients.
     */
    public void sendTcp(byte id, Object... args) throws IOException {
        System.out.println("---> Sending TCP: " + id);
        for (OutputStream stream : tcpStreams.values()) { // Marshalling + Sending
            stream.write(id);
        }
        for (ObjectOutputStream stream : objectOutputStreams.values()) {
            stream.writeInt(args.length);
            for (Object arg : args) {
                stream.writeObject(arg);
            }
            stream.flush();
        }
    }

    public void sendTcp(InetAddress receiver, byte id, Object... args) throws IOException {
        System.out.println("---> Sending TCP: " + id);
        tcpStreams.get(receiver).write(id);
        objectOutputStreams.get(receiver).writeInt(args.length);
        for (var arg : args) {
            objectOutputStreams.get(receiver).writeObject(arg);
        }
        objectOutputStreams.get(receiver).flush();
    }

    // (e.g. update)
    public void sendUdpMulticast(byte id, Object... args) throws IOException {
        var data = marshall(id, args);
        sendUdpMulticast(data);
    }

    // (e.g. setLightCycleTurn)
    public void sendUdpUnicast(InetAddress target, byte id, Object... args) throws IOException {
        var data = marshall(id, args);
        sendUdpUnicast(target, data);
    }

    // Marshaller
    private byte[] marshall(byte id, Object... args) throws IOException {
        var os = new ByteArrayOutputStream(1024);
        os.write((byte) id);
        var oos = new ObjectOutputStream(os);
        oos.writeInt(args.length);
        for (Object object : args) {
            oos.writeObject(object);
        }
        return os.toByteArray();
    }

    // Sender
    private void sendUdpMulticast(byte[] data) {
        try {
            udpSocket.send(new DatagramPacket(data, data.length, multicastGroup, port));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Sender
    private void sendUdpUnicast(InetAddress target, byte[] data) {
        try {
            udpSocket.send(new DatagramPacket(data, data.length, target, port));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}