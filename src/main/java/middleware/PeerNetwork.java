package middleware;

import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class PeerNetwork {
    public boolean hostObjectLocally = true;
    public List<Socket> peers = new ArrayList<Socket>();
    public ClientStub clientStub;
    public ServerStub serverStub;
    public InetAddress multicastGroup;
    public PeerId peerId;
}
